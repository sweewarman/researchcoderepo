import numpy as np
import os,sys

from Indexing import *
from graphviz import Digraph

class TransitionSystem():

    def __init__(self,nFeatures,nFeatSizes,label,nActions):
        """
        Constructor to initialize transition system
        """

        self.nFeature     = nFeatures
        self.nFeatSize    = nFeatSizes
        self.sizeTS       = np.prod(nFeatSizes)
        self.FeatureLabel = label
        self.nActions     = nActions

        self.TransMat     = np.zeros((self.nActions,self.sizeTS,self.sizeTS))
        self.Inputmat     = 0
        self.Output       = []
        self.Propositions = []

        self.TS           = True
        self.FSM          = False
        self.MarkovChain  = False

        self.FeatVals     = range(nFeatures)
        self.FeatValLabel = range(nFeatures)

        
        self.delim = ','

        self.node_color = ['none' for i in range(self.sizeTS)]

        
        
    def SetFeatureVals(self,ind,vals,label):
        """
        function to Set labels for individual feature values
        """
        self.FeatVals[ind]     = vals
        self.FeatValLabel[ind] = label
        
        return

    def InitTrans(self,TM,index =-1):
        """
        Function to initialize the transition matrix
        """

        if(index == -1):
            self.TransMat  = TM
        else:
            self.TransMat[index,:,:] = TM

        return

    def GetIndex(self,subscripts):
        """
        Get index corresponding to given subscripts
        """
        
        index = sub2ind(subscripts,self.nFeatSize)

        return index

    def GetSubscripts(self,index):
        """
        Get subscripts corresponding to given index
        """

        subscripts = ind2sub(index,self.nFeatSize)

        return subscripts
        
    def SynProduct(self,FSM):
        """
        Synchronous product between transition system and
        finite state machine
        """

        return

    def PrintGraph(self,ind,filename,init = -1):
        """
        Print graph for visualization
        """

        
        Graph = Digraph()

        nStates    = self.sizeTS
        Tensor     = self.TransMat[ind,:,:]
        Feat_Label = self.FeatValLabel
        
        V     = [-1 for i in range(nStates)]
        
        ToVisit = []
        Visited = []

        if(init != -1):
            ToVisit.append(init)
        else:
            ToVisit = range(nStates)

        
        while( len(ToVisit) != 0 ):
            
            i = ToVisit.pop(0)

            if(i in Visited):
                continue

            Visited.append(i)
      
            trans = [j for j in range(nStates) if (Tensor[i,j] != 0.0)]

            if not trans:
                continue
            else:
                
                # Add parent node if its not in the list already
                if( V[i] == -1 ):
                    sub1  = self.GetSubscripts(i)
                    
                    V_label = ''
                    for fi,fj in enumerate(sub1):
                        if( fi != len(sub1)-1 ):
                            V_label += Feat_Label[int(fi)][int(fj)]+self.delim
                        else:
                            V_label += Feat_Label[int(fi)][int(fj)]

                    Graph.node(str(i),label=V_label,\
                                           fillcolor=self.node_color[i])
                
                
                # Add children
                for v in trans:
                    if(V[v] == -1):
                        
                        sub2  = self.GetSubscripts(v)
                        
                        V_label = ''
                        for fi,fj in enumerate(sub2):
                            if( fi != len(sub2)-1 ):
                                V_label += Feat_Label[int(fi)][int(fj)]+self.delim
                            else:
                                V_label += Feat_Label[int(fi)][int(fj)]
                        
                        Graph.node(str(v),label=V_label,\
                                        fillcolor=self.node_color[v])
                        
                        
                        estr = "%1.3f"%Tensor[i,v]
                        
                        
                        if(init != -1):
                            ToVisit.append(v)
                            Graph.edge(str(i),str(v),estr)
                        else:
                            Graph.edge(str(i),str(v),estr)
                        
        
        Graph.node_attr = {'style':'filled'}
        filename = filename + '.gv'
           
        Graph.render(filename)

        return
