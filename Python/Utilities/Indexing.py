"""
Utility function for indexing
"""
import numpy as np

def sub2ind(feat_ind,feat_size):
    
    """
    Get the single index corresponding to the feature indices given
    """

    total_feat = len(feat_ind)

    index = 0

    for i in range(total_feat):
        if(i < (total_feat - 1)):
            index += feat_ind[i] * np.prod(feat_size[i+1:])
        else:
            index += feat_ind[i]


    return int(index)
    

def ind2sub(index,feat_size):
    
    """
    Get the feature indices corresponding to the given index
    """

    last = len(feat_size) - 1;

    subscripts = np.zeros(len(feat_size),'int')

    while(last>=0): 
        subscripts[last]   = int(index % int(feat_size[last]))
        index              = index / int(feat_size[last]);
        last               = last - 1;

    return subscripts
