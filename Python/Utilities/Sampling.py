"""
Utility functions for sampling
"""
import numpy as np

def Truncate(X,Xmin,Xmax):
    """
    Function to truncate signal X between specified min and max values
    """
    if(X <= Xmin):
        X = Xmin
    elif(X >= Xmax):
        X = Xmax

    return X

def SampleArb(dist):
    """
    Function to sample from an arbitrary distribution
    """

    # Cumulative distribution
    cum_dist = []

    for i,j in enumerate(dist):
        cum = sum(dist[0:i+1])
        cum_dist.append(cum)

    u = np.random.uniform(0,1)

    if (u == 1):
        return len(cum_dist)-1

    for i,j in enumerate(cum_dist):
        if u <= cum_dist[i]:
            return i
