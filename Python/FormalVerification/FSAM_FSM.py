"""
Functions required to construct a composite transition system

# State Index
 0: S1
 1: S2
 2: S3
 3: S4
 4: S5
 5: S6
 6: S7
 7: S9
 8: S10
 9: S11
10: S12

# Alphabet Index
 0: V0
 1: Vmcg
 2: V1
 3: VR
 4: Vlof
 5: V2
 6: Vfp
 7: P_rot
 8: TS

"""

import os,sys

libpath = os.path.abspath('../Utilities')
sys.path.append(libpath);


import numpy as np
from TransitionSystem import TransitionSystem


nAlphabets = 9
nStates    = 11+1
TransMat   = np.zeros((nAlphabets,nStates,nStates))

TransMat[:,:,nStates-1] = -1
TransMat[0,0,1]  = 1
TransMat[0,1,1]  = 1
TransMat[1,1,2]  = 1
TransMat[1,2,2]  = 1
TransMat[2,2,3]  = 1
TransMat[2,3,3]  = 1
TransMat[3,3,4]  = 1
TransMat[7,3,7]  = 1
TransMat[3,4,4]  = 1
TransMat[4,4,5]  = 1
TransMat[8,4,8]  = 1
TransMat[4,5,5]  = 1
TransMat[5,5,6]  = 1
TransMat[5,6,6]  = 1
TransMat[7,7,7]  = 1
TransMat[3,7,4]  = 1
TransMat[8,8,8]  = 1
TransMat[4,8,5]  = 1

FSM = TransitionSystem(1,[nStates],['S'],nAlphabets)
FSM.FeatValLabel[0] = ['S1','S2','S3','S4','S5','S6',\
                       'S7','S8','S9','S10','S11','S12']

FSM.TransMat = TransMat

def SynProduct(TS,FSM):
        """
        Function to construct synchronous product of 
        Transition system TS and Finite State machine FSM
        """
        
        n1    = TS.sizeTS
        n2    = FSM.sizeTS
        
        TS_prod = np.zeros((n1*n2,n1*n2));
        
        syn_state = [(i,j) for i in range(n1) for j in range(n2)]
        
        syn_trans = [(i,j) for i in syn_state for j in syn_state]
        
        for S in syn_trans:
                
                qi = S[0][0]
                si = S[0][1]
                qj = S[1][0]
                sj = S[1][1]

               
                if(si in {0,1,2,3,4,5,6,11}):
                        TransMat = TS.TransMat[0,:,:]
                        mode = 1
                elif(si in {7,8,9,10}):
                        TransMat = TS.TransMat[1,:,:]
                        mode = 2
                else:
                       
                        print "Error evaluating mode in Syn product"

                 
                if(TransMat[qi,qj] != 0.0):
                     
                        fsm_in = GetAlphabetIndex(TS,qj)

                        if(FSM.TransMat[fsm_in,si,sj] == 1.0):
                                ti = qi*n2 + si
                                tj = qj*n2 + sj
                                TS_prod[ti,tj] = mode
             
                                
        return TS_prod


def GetAlphabetIndex(TS,qj):
        """
        Function to get the alphabet index corresponding
        to given transition system state
        """

        surface_type = qj%3

        

        Label = TS.SurfaceLabels[qj]

        VLabel = Label[0]
        PLabel = Label[1]
        HLabel = Label[2]
                
        if( VLabel == 'V0' or VLabel == ('V0','Vmcg') ):
                output = 0

        elif( VLabel == 'Vmcg' or VLabel == ('Vmcg','V1') ):
                output = 1

        elif( VLabel == 'V1' or VLabel == ('V1','VRmin')):
                output = 2
                if( PLabel == 'P2' or PLabel == ('P2','P3') ):
                        output = 7

        elif( VLabel == 'VRmin' or VLabel == ('VRmin','VR') ):
                output = 2
                if( PLabel == 'P2' or PLabel == ('P2','P3') ):
                        output = 7

        elif( VLabel == 'VR' or VLabel ==  ('VR','VRmax') ):
                output = 3
                if( PLabel == 'P3' or PLabel == ('P3','P4') ):
                        if( HLabel == 'H0' or HLabel == ('H0','H1') ):
                                output = 8

        elif(VLabel == 'VRmax' or VLabel == ('VRmax','Vlof') ):
                output = 3
                if(PLabel == 'P3' or PLabel == ('P3','P4') ):
                        if(HLabel == 'H0' or HLabel == ('H0','H1') ):
                                output = 8

        elif(VLabel == 'Vlof' or VLabel == ('Vlof','V2') ):
                output = 4

        elif(VLabel == 'V2' or VLabel == ('V2','Vfp') ):
                output = 5
        
       
        return output


