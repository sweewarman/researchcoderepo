"""
Main script for Takeoff FSAM model checking
* See Init.py for Initialization variables
"""

from Init import *
from Utils import *


# Construct FSAM finite state machine
from FSAM_FSM import *


# Construct pilot transition system

SIM.ICcount      = 0
SIM.ICpilotcount = 0
SIM.mode         = 0
SIM.debug        = False
SIM.count        = 0

# Append all states for a brute force simulation
# Append only initial state for reachable states only
SIM.ToVisit      = [nodep for nodep in range(TS2.sizeTS)]
SIM.Visited      = []
#SIM.ToVisit.append(0)


while( len(SIM.ToVisit) ):
    
    SIM.count += 1
   
    # Initialize output array to zero
    SIM.yout[:,:] = 0 
    SIM.stop      = False
    
    # Get initial conditions
    GetInitialConditions2(SIM)

    SIM.RunSim()

print "Finished Propagation step for pilot"


# Construct EA transition system

SIM.ICcount      = 0
SIM.ICpilotcount = 0
SIM.mode         = 1
SIM.count        = 0


# Append all states for a brute force simulation
# Append only initial state for reachable states only
SIM.ToVisit      = [nodep for nodep in range(TS2.sizeTS)]
SIM.Visited      = []
#SIM.ToVisit.append(0)


while( len(SIM.ToVisit) ):
    
    SIM.count += 1
  
    
    # Initialize output array to zero
    SIM.yout[:,:] = 0 
    SIM.stop      = False
    
    # Get initial conditions
    GetInitialConditions2(SIM)

    SIM.RunSim()

print "Finished Propagation step for EA"



SIM.TS2.PrintGraph(0,'TS_pilot_surf',init=0)
SIM.TS2.PrintGraph(1,'TS_EA_surf',init=0)


# Compose transition system with FSAM finite state machine

FSM_states  = FSM.sizeTS
TS2_states  = TS2.sizeTS

TS3 = TransitionSystem(1,[FSM_states*TS2_states],['CS'],1)

# Create labels to visualize the composite transition system
CSlabel = []
for i in range(TS3.sizeTS):
    
    sub = ind2sub(i,[TS2_states,FSM_states])

    FSMlabel = FSM.FeatValLabel[0][sub[1]]
    TSlabel  = TS2.FeatValLabel[0][sub[0]]

    CSlabel.append(FSMlabel+'\\n'+TSlabel)

    if(sub[1] in {7,8,9,10}):
       TS3.node_color[i] = 'yellow'

    if(TS2.node_color[sub[0]] != 'none'):
        TS3.node_color[i] = TS2.node_color[sub[0]]

TS3.FeatValLabel[0]= CSlabel

    
TransMat = SynProduct(TS2,FSM)

TS3.TransMat[0,:,:] = TransMat

    
TS3.PrintGraph(0,'CompositeTS',init=1)

