import os,sys

libpath2 = os.path.abspath('../Utilities/')
sys.path.append(libpath2)

from Indexing import *
from Sampling import Truncate

r2d = 180/np.pi
d2r = np.pi/180

from numpy import fabs

def GetInitialConditions(sim):
    """
    Function to get initial conditions for various simulations
    """

    if(sim.ICcount < 8):
        cube_index = sim.ToVisit[0]
    else:
        node = sim.ToVisit.pop(0)
        sim.Visited.append(node)

        # If ToVisit list is empty stop simulation
        if(len(sim.ToVisit)):
            cube_index = sim.ToVisit[0]
            sim.ICcount = 0
            sim.ICpilotcount = 0
        else:
            sim.stop = True
            return

    
    sub = sim.TS1.GetSubscripts(cube_index)
    
    Vinit = sim.TS1.FeatVals[0][sub[0]]
    Pinit = sim.TS1.FeatVals[1][sub[1]]
    Hinit = sim.TS1.FeatVals[2][sub[2]]

    
    ic_sub = ind2sub(sim.ICcount,[2,2,2])

    sim.yout[7,0]  = Pinit[ic_sub[1]]*d2r 
    sim.yout[12,0] = Vinit[ic_sub[0]]
    sim.yout[11,0] = Hinit[ic_sub[2]]

    if(ic_sub[0] == 1):
        sim.yout[12,0] -= 0.01
    if(ic_sub[1] == 1):
        sim.yout[7,0] -= 0.01
    if(ic_sub[2] == 1):
        sim.yout[11,0] -= 0.01
    

    if(sim.mode == 0):

        #TODO:  Adjust this if necessary
        vr_min = sim.TS1.FeatVals[0][3][0]
        v2     = sim.TS1.FeatVals[0][7][0]


        # Get pilot parameters
        if(sim.yout[12,0] >= vr_min and sim.yout[12,0] <= v2 ):
            ic_subp = ind2sub(sim.ICpilotcount,[2,2,2,2,2])
            
            sim.PilotParams['Vr']    = sim.vr_range[ic_subp[0]]
            sim.PilotParams['Kp']    = sim.kp_range[ic_subp[1]]
            sim.PilotParams['n']     = sim.n_range[ic_subp[2]]
            sim.PilotParams['Pref']  = sim.pref_range[ic_subp[3]]
            sim.PilotParams['qinit'] = sim.qinit_range[ic_subp[4]]

            sim.yout[4,0] = sim.PilotParams['qinit']*d2r
            
            if(sim.ICpilotcount == 31):
                sim.ICpilotcount = 0
                sim.ICcount += 1

        else:
            ic_subp = ind2sub(sim.ICpilotcount,[2,2,2,2])
            
            sim.PilotParams['Vr']    = sim.vr_range[ic_subp[0]]
            sim.PilotParams['Kp']    = sim.kp_range[ic_subp[1]]
            sim.PilotParams['n']     = sim.n_range[ic_subp[2]]
            sim.PilotParams['Pref']  = sim.pref_range[ic_subp[3]]
            sim.PilotParams['qinit'] = 0

            sim.yout[4,0] = 0

            if(sim.ICpilotcount == 15):
                sim.ICpilotcount = 0
                sim.ICcount += 1

        sim.ICpilotcount += 1

    else:

        #TODO:  Adjust this if necessary
        vr_min = sim.TS1.FeatVals[0][4][0]
        v2     = sim.TS1.FeatVals[0][7][0]

        if(sim.yout[12,0] >= vr_min and sim.yout[12,0] <= v2 ):
            sim.yout[4,0] = sim.qinit_range[sim.ICpilotcount]*d2r

            if(sim.ICpilotcount == 1):
                sim.ICpilotcount = 0
                sim.ICcount += 1

            sim.ICpilotcount += 1

        else:
            sim.ICpilotcount = 0
            sim.ICcount += 1

    
    sim.isim = 0
    sim.tsim = 0

    return
    
def GetInitialConditions2(sim):
    """
    Get Initial conditions for transition system with surfaces
    """

    if(sim.ICcount < 4):
        surface_index = sim.ToVisit[0]
    else:
        node = sim.ToVisit.pop(0)
        sim.Visited.append(node)

        # If ToVisit list is empty stop simulation
        if(len(sim.ToVisit)):
            surface_index = sim.ToVisit[0]
            sim.ICcount = 0
            sim.ICpilotcount = 0
        else:
            sim.stop = True
            return

    
    cube_index   = surface_index/3
    surface_type = surface_index%3


    sim.cube_index = cube_index
    sim.surface_index = surface_index

    sub   = sim.TS1.GetSubscripts(cube_index)
    
    Vinit = sim.TS1.FeatVals[0][sub[0]]
    Pinit = sim.TS1.FeatVals[1][sub[1]]
    Hinit = sim.TS1.FeatVals[2][sub[2]]

    
    ic_sub = ind2sub(sim.ICcount,[2,2])


    if(surface_type == 0):
        sim.yout[7,0]  = Pinit[ic_sub[0]]*d2r 
        sim.yout[12,0] = Vinit[0]
        sim.yout[11,0] = Hinit[ic_sub[1]]

        if(ic_sub[0] == 1):
            sim.yout[7,0] -= 0.01
        if(ic_sub[1] == 1):
            sim.yout[11,0] -= 0.01

    elif(surface_type == 1):
        sim.yout[7,0]  = Pinit[0]*d2r 
        sim.yout[12,0] = Vinit[ic_sub[0]]
        sim.yout[11,0] = Hinit[ic_sub[1]]
        
        if(ic_sub[0] == 1):
            sim.yout[12,0] -= 0.01
        if(ic_sub[1] == 1):
            sim.yout[11,0] -= 0.01

    else:
        sim.yout[7,0]  = Pinit[ic_sub[1]]*d2r 
        sim.yout[12,0] = Vinit[ic_sub[0]]
        sim.yout[11,0] = Hinit[0]
    
        if(ic_sub[0] == 1):
            sim.yout[12,0] -= 0.01
        if(ic_sub[1] == 1):
            sim.yout[7,0] -= 0.01

    if(sim.mode == 0):

        #TODO:  Adjust this if necessary
        vr_min = sim.TS1.FeatVals[0][3][0]
        v2     = sim.TS1.FeatVals[0][7][0]


        # Get pilot parameters
        if(sim.yout[12,0] >= vr_min and sim.yout[12,0] <= v2 ):
            ic_subp = ind2sub(sim.ICpilotcount,[2,2,2,2,2])
            
            sim.PilotParams['Vr']    = sim.vr_range[ic_subp[0]]
            sim.PilotParams['Kp']    = sim.kp_range[ic_subp[1]]
            sim.PilotParams['n']     = sim.n_range[ic_subp[2]]
            sim.PilotParams['Pref']  = sim.pref_range[ic_subp[3]]
            sim.PilotParams['qinit'] = sim.qinit_range[ic_subp[4]]

            sim.yout[4,0] = sim.PilotParams['qinit']*d2r
            
            if(sim.ICpilotcount == 31):
                sim.ICpilotcount = 0
                sim.ICcount += 1

            
        else:
            ic_subp = ind2sub(sim.ICpilotcount,[2,2,2,2])
            
            sim.PilotParams['Vr']    = sim.vr_range[ic_subp[0]]
            sim.PilotParams['Kp']    = sim.kp_range[ic_subp[1]]
            sim.PilotParams['n']     = sim.n_range[ic_subp[2]]
            sim.PilotParams['Pref']  = sim.pref_range[ic_subp[3]]
            sim.PilotParams['qinit'] = 0

            sim.yout[4,0] = 0

            if(sim.ICpilotcount == 15):
                sim.ICpilotcount = 0
                sim.ICcount += 1


            

        sim.ICpilotcount += 1

    else:

        #TODO:  Adjust this if necessary
        vr_min = sim.TS1.FeatVals[0][4][0]
        v2     = sim.TS1.FeatVals[0][7][0]

        if(sim.yout[12,0] >= vr_min and sim.yout[12,0] <= v2 ):
            sim.yout[4,0] = sim.qinit_range[sim.ICpilotcount]*d2r

            if(sim.ICpilotcount == 1):
                sim.ICpilotcount = 0
                sim.ICcount += 1

            sim.ICpilotcount += 1

        else:
            sim.ICpilotcount = 0
            sim.ICcount += 1

    
    sim.isim = 0
    sim.tsim = 0

    return
    

def Update(sim,t,x0):
    """
    Function used by GenSim to generate inputs for the takeoff 
    simulation
    """

    sim.x0     = sim.Aircraft.ChkTailStrike(sim.x0)

    if(sim.isim > 0):
        getAbstractState(sim,x0)

    # Compute control input for current mode
    if(sim.mode == 0 ):
        U          = getPilotInput(sim)
    else:
        U          = getEAInput(sim)

    if( sim.x0[12] >= sim.Vmax):
        sim.stop = True

    return U

def Update2(sim,t,x0):
    """
    Function used by GenSim to generate inputs for the takeoff 
    simulation
    """

    sim.x0     = sim.Aircraft.ChkTailStrike(sim.x0)


    if(sim.isim > 0):
        getAbstractState2(sim,x0)

    # Compute control input for current mode
    if(sim.mode == 0 ):
        U          = getPilotInput(sim)
    else:
        U          = getEAInput(sim)

    if( sim.x0[12] >= sim.Vmax):
        sim.stop = True

    return U


def getAbstractState(sim,x0):
    """
    Get discrete state corresponding to current and previous state
    and add transitions to te transition system
    """

    CSi = sim.yout[:,sim.isim-1]
    CSj = sim.yout[:,sim.isim]

    Vi  = Truncate(CSi[12],sim.Vmin,sim.Vmax)
    Vj  = Truncate(CSj[12],sim.Vmin,sim.Vmax)

    Pi  = Truncate(CSi[7]*r2d,sim.Pmin,sim.Pmax)
    Pj  = Truncate(CSj[7]*r2d,sim.Pmin,sim.Pmax)

    Hi  = Truncate(CSi[11],sim.Hmin,sim.Hmax)
    Hj  = Truncate(CSj[11],sim.Hmin,sim.Hmax)

    Vind_i = GetPropIndex(sim,0,Vi)
    Vind_j = GetPropIndex(sim,0,Vj)
    Pind_i = GetPropIndex(sim,1,Pi)
    Pind_j = GetPropIndex(sim,1,Pj)
    Hind_i = GetPropIndex(sim,2,Hi)
    Hind_j = GetPropIndex(sim,2,Hj)


    DSi = sim.TS1.GetIndex([Vind_i,Pind_i,Hind_i])
    DSj = sim.TS1.GetIndex([Vind_j,Pind_j,Hind_j])

    if(DSi != DSj ):
        
        sim.TS1.TransMat[sim.mode,DSi,DSj] += 1

        if( (DSj not in sim.Visited) and (DSj not in sim.ToVisit) ):
            sim.ToVisit.append(DSj)
            

        sim.stop = True


def getAbstractState2(sim,x0):
    """
    Get discrete state corresponding to current and previous state
    and add transitions to the surfaces based transition system
    """

    CSi = sim.yout[:,sim.isim-1]
    CSj = sim.yout[:,sim.isim]

    Vi  = Truncate(CSi[12],sim.Vmin,sim.Vmax)
    Vj  = Truncate(CSj[12],sim.Vmin,sim.Vmax)

    Pi  = Truncate(CSi[7]*r2d,sim.Pmin,sim.Pmax)
    Pj  = Truncate(CSj[7]*r2d,sim.Pmin,sim.Pmax)

    Hi  = Truncate(CSi[11],sim.Hmin,sim.Hmax)
    Hj  = Truncate(CSj[11],sim.Hmin,sim.Hmax)

    Vind_i = GetPropIndex(sim,0,Vi)
    Vind_j = GetPropIndex(sim,0,Vj)
    Pind_i = GetPropIndex(sim,1,Pi)
    Pind_j = GetPropIndex(sim,1,Pj)
    Hind_i = GetPropIndex(sim,2,Hi)
    Hind_j = GetPropIndex(sim,2,Hj)


    DSi = sim.TS1.GetIndex([Vind_i,Pind_i,Hind_i])
    DSj = sim.TS1.GetIndex([Vind_j,Pind_j,Hind_j])

    if(DSi != DSj ):
        
        Si,Sj = GetSurfaceTransition(sim,DSi,DSj)

        if( (Sj not in sim.Visited) and (Sj not in sim.ToVisit) ):
            sim.ToVisit.append(Sj)
            

        sim.stop = True


def GetPropIndex(sim,ind,X):
    """
    Get proposition index corresponding to given
    continuous state and state index
    """

    for pind,prop in enumerate(sim.TS1.FeatVals[ind]):
        if( prop[0] <= X < prop[1]):
            return pind


def GetSurfaceTransition(sim,DSi,DSj):

    x0 = sim.yout[:,0]
    xn = sim.yout[:,sim.isim]

    j = GetNearestSurface(sim,xn)
    
    Si = sim.surface_index
    Sj = DSj*3 + j

    sim.TS2.TransMat[sim.mode,Si,Sj] += 1

    return Si,Sj

def GetNearestSurface(sim,x0):
    """
    Function that returns the nearest cube surface
    - ***Use this only for the end node
    """

    PI_V  = sim.TS1.FeatVals[0]
    PI_Th = sim.TS1.FeatVals[1]
    PI_H  = sim.TS1.FeatVals[2]

    v   = x0[12]
    p   = x0[7]*r2d
    h   = x0[11]

    Vi  = Truncate(v,sim.Vmin,sim.Vmax)
    Pi  = Truncate(p,sim.Pmin,sim.Pmax)
    Hi  = Truncate(h,sim.Hmin,sim.Hmax)


    v_ind = GetPropIndex(sim,0,Vi)
    p_ind = GetPropIndex(sim,1,Pi)
    h_ind = GetPropIndex(sim,2,Hi)
   

    veps1  = fabs(v - PI_V[v_ind][0])
    peps1  = fabs(p - PI_Th[p_ind][0])
    heps1  = fabs(h - PI_H[h_ind][0])
    
    
    F      = [veps1,peps1,heps1]
    
    surf = [i for i,j in enumerate(F) if j==min(F)]
    
    index = surf[0]
    
    return index
    
    
def getPilotInput(sim):
    """
    Pilot model
    """

    Vr        = sim.PilotParams['Vr']
    Kp        = sim.PilotParams['Kp']
    Kd        = sim.PilotParams['Kd']
    Pitch_ref = sim.PilotParams['Pref']*d2r
    lag       = sim.PilotParams['n']
    qmax      = sim.PilotParams['qinit']*d2r
    
    
    n         = sim.isim
    m         = n - lag

    Pitch     = sim.yout[7,n]
    q         = sim.yout[4,n]
    V         = sim.yout[12,n]
    
    if(V >= Vr):
        if(sim.isim == 0 ):
            Pitch_lag = Pitch - lag*qmax*sim.dt
            ue = Kp*( Pitch_ref - Pitch_lag ) + Kd*q
        else:
            Pitch_lag = sim.yout[7,m]
            ue = Kp*( Pitch_ref - Pitch ) + Kd*q
    else:
        Pitch_lag = sim.yout[7,m]
        ue = Kp*( 0 - Pitch ) + Kd*q


    U = [ue,0,0,0]

    return U

def getEAInput(sim):
    """
    Envelope Aware controller
    """
    
    Vr         = sim.EAParams['Vr']
    Kp         = sim.EAParams['Kp']
    Kd         = sim.EAParams['Kd']
    Pitch_ref1 = sim.EAParams['Pref1']*d2r
    Pitch_ref2 = sim.EAParams['Pref2']*d2r
    TS_pitch   = sim.EAParams['TS_pitch']*d2r
    TS_alt     = sim.EAParams['TS_alt']
    
    n         = sim.isim
    Pitch     = sim.yout[7,n]
    q         = sim.yout[4,n]
    Alt       = sim.yout[11,n]
    V         = sim.yout[12,n]
    
    if(V >= Vr):
        if( Pitch >= TS_pitch and Alt <= TS_alt):
            ue = Kp*( Pitch_ref2 - Pitch ) + Kd*q
        else:
            ue = Kp*( Pitch_ref1 - Pitch ) + Kd*q
    else:
        ue = Kp*( 0 - Pitch ) + Kd*q


    U = [ue,0,0,0]

    return U
    

    
    
