"""
This script is used to initialize the variables and objects required
to perform model checking for takeoff FSAM.

1. TS1 is a TransitionSystem object for a cube based transition system
2. TS2 is a TransitionSystem object for a surface based transition system
3. TS3 is the composite transition system
4. FSM is the finite state machine

"""

import os,sys

libpath1 = os.path.abspath('../TakeoffSim')
libpath2 = os.path.abspath('../GenSim')
libpath3 = os.path.abspath('../Utilities')

sys.path.append(libpath1);
sys.path.append(libpath2);
sys.path.append(libpath3);

from GenSim import *
from TK_Aircraft import *
from TransitionSystem import TransitionSystem
from Utils import *

# Setup the path variables to import aircraft parameters
path1    = '../AircraftModels/'
aircraft = path1 + 'GTM.dat'
perf     = path1 + 'GTM_performance.dat'

# Instantiate a takeoff class and compute takeoff performance
GTM      = TK_Aircraft(aircraft,perf)
GTM.Compute_Performance(GTM.Params)

# Simulation time span and timestep
tspan  = 30
dt     = 0.1

# u,v,w,p,q,r,roll,pitch,yaw,Xe,Ye,h,U,alpha,beta
x0     = [0.1,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0]

# Instantiate a simulation class
SIM          = GenSim(dt,tspan,x0)

# Instantiate required classes within the simulation framework
SIM.Aircraft = GTM

# Set simulation update and output function
SIM.SetUpdateFunc(Update2)
SIM.SetOutputFunc(GTM.SimAppDyn)

# Vspeeds
# Vspeed definitions (in m/s)
V0,Vmcg,V1      =  0.0, 10.0, 47.0
VRmin,VR,VRmax  = 49.0, 55.0, 62.0
Vlof,V2, Vfp    = 66.0, 67.5, 80.0
Vspeeds         = [Vmcg,V1,VR,Vlof,V2,Vfp]

# Pitch thresholds
P0,P1,P2        = -2,3,5
P3,P4,P5        =  9,10,15

# Altitude thresholds
H0,H1,H2,H3     = -5,1,2.5,15

TS1 = TransitionSystem(3,[8,5,3],['V','P','H'],2)
TS2 = TransitionSystem(1,[360],['S'],2)

#Propositions for hyper-cubes
PI_V      = [(V0,Vmcg),(Vmcg,V1),(V1,VRmin),(VRmin,VR),\
             (VR,VRmax),(VRmax,Vlof),(Vlof,V2),(V2,Vfp)]
PI_Th     = [(P0,P1),(P1,P2),(P2,P3),(P3,P4),(P4,P5)]
PI_H      = [(H0,H1),(H1,H2),(H2,H3)]

#Proposition strings for graph visualization
PI_str    = lambda x: '['+str(x[0])+','+str(x[1])+']'

PI_V_str  = [ PI_str(val) for val in PI_V  ]
PI_Th_str = [ PI_str(val) for val in PI_Th ]
PI_H_str  = [ PI_str(val) for val in PI_H  ]

#Proposition strings used for composition
PI_V_str2 = [('V0','Vmcg'),('Vmcg','V1'),('V1','VRmin'),('VRmin','VR'), \
             ('VR','VRmax'),('VRmax','Vlof'),('Vlof','V2'),('V2','Vfp')]

PI_Th_str2 = [ ('P0','P1'),('P1','P2'),('P2','P3'),('P3','P4'),('P4','P5')]
PI_H_str2  = [ ('H0','H1'),('H1','H2'),('H2','H3')]


TS1.SetFeatureVals(0,PI_V,PI_V_str)
TS1.SetFeatureVals(1,PI_Th,PI_Th_str)
TS1.SetFeatureVals(2,PI_H,PI_H_str)

# Delimiter for graph visualization
TS1.delim = '\\n'


# Create labels for transition system 2
SurfaceVal    = []
SurfaceValStr = []
TS2.SurfaceLabels = []

for i in range(360):
    
    cube    = i/3
    surface = i%3

    sub = TS1.GetSubscripts(cube)

    vind = sub[0]
    pind = sub[1]
    hind = sub[2]

    if(surface == 0):
        value  = (PI_V[vind][0],PI_Th[pind],PI_H[hind])
        string = str(PI_V[vind][0])+'\\n'   \
                    + PI_Th_str[pind]+'\\n' \
                    + PI_H_str[hind]

        if( value[1][0] >= P4 and value[2][1] <= H3 ):
            TS2.node_color[i] = 'red'

        if( value[1][0] >= P2 and value[0] <= VR  ):
            TS2.node_color[i] = 'blue'

        FsmLabel = (PI_V_str2[vind][0],PI_Th_str2[pind],PI_H_str2[hind])

    elif(surface == 1):
        value  = (PI_V[vind],PI_Th[pind][0],PI_H[hind])
        string = PI_V_str[vind]+'\\n'   \
                + str(PI_Th[pind][0])+'\\n' \
                    + PI_H_str[hind]

        if( value[1] > P4 and value[2][1] <= H3 ):
            TS2.node_color[i] = 'red'

        if( value[1] >= P2 and value[0][1] <= VR  ):
            TS2.node_color[i] = 'blue'

        FsmLabel = (PI_V_str2[vind],PI_Th_str2[pind][0],PI_H_str2[hind])

    elif(surface == 2):
        value  = (PI_V[vind],PI_Th[pind],PI_H[hind][0])
        string = PI_V_str[vind]+'\\n'   \
                + PI_Th_str[pind]+'\\n' \
                    + str(PI_H[hind][0])

        if( value[1][0] >= P4 and value[2] <= H3 ):
            TS2.node_color[i] = 'red'

        if( value[1][0] >= P2 and value[0][1] <= VR  ):
            TS2.node_color[i] = 'blue'

        FsmLabel = (PI_V_str2[vind],PI_Th_str2[pind],PI_H_str2[hind][0])

    
    SurfaceVal.append(value)
    SurfaceValStr.append(string)
    TS2.SurfaceLabels.append(FsmLabel)

TS2.SetFeatureVals(0,SurfaceVal,SurfaceValStr)


SIM.TS1 = TS1
SIM.TS2 = TS2

# Bounds on state variables to stop simulation if exceeded
SIM.Vmin  = 0
SIM.Vmax  = 79.9
SIM.Pmin  = -2
SIM.Pmax  = 14.9
SIM.Hmin  = -5
SIM.Hmax  = 14.9

# Control variables - mostly used by pilot model
SIM.vr_range      = [VR-10,VR+10]
SIM.kp_range      = [-1.0,-6.0]
SIM.n_range       = [0,1]
SIM.pref_range    = [7,13]
SIM.qinit_range   = [0,6]

# Dictionary datastructure to store pilot variables
SIM.PilotParams   = {'Vr'   :0,   \
                     'Kp'   :0,   \
                     'Kd'   :1.0, \
                     'n'    :0,   \
                     'Pref' :0,   \
                     'qinit':0}

# Dictionary datastructure to store EA controller variables
SIM.EAParams      = {'Vr'      :VR,   \
                     'Kp'      :-3,   \
                     'Kd'      :1.0,  \
                     'Pref1'   :8,    \
                     'Pref2'   :5,    \
                     'TS_pitch':9,    \
                     'TS_alt'  :2.5  }


