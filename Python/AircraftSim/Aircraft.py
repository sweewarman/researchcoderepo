## @package Aircraft Class
#  Generic Aircraft Class module for aircraft simulation
#
#  @detail Contains a generic aircraft class with attributes and objects. 
#          Aircraft class is based on NASA simulation document
#  @author Sweewarman Balachandran
#  @date   12-22-2014
#
#  @TODO 
#  Add lateral - directional dynamics
#
from scipy.integrate import odeint
import numpy as np
from numpy import sin,cos,fabs
from LandingGear import *

class Aircraft():
    
    ## Class constructor
    # @param self The object pointer
    # @brief Initializes the attributes of the aircraft
    def __init__(self,filename):

        """
        Class constructor 
        Initializes aircraft parameters
        """
        
        fp        = open(filename,'r')

        fp.readline()

        # Aircraft physical parameters
        self.M    = float(fp.readline().split()[1])   # Mass  -  Kg 
        self.c    = float(fp.readline().split()[1])   # chord -  m 
        self.b    = float(fp.readline().split()[1])   # planform length - m 
        self.Sref = float(fp.readline().split()[1])   # planform area   - m^2 
        self.Ixx  = float(fp.readline().split()[1])   # Moment of inertia - kgm^2 
        self.Iyy  = float(fp.readline().split()[1]) 
        self.Izz  = float(fp.readline().split()[1]) 
        self.Ixz  = float(fp.readline().split()[1]) 

        self.lxL  = float(fp.readline().split()[1])   # Gear x-y-z location from cg
        self.lyL  = float(fp.readline().split()[1]) 
        self.lzL  = float(fp.readline().split()[1]) 

        self.lxR  = self.lxL
        self.lyR  = -self.lyL
        self.lzR  = self.lzL

        self.lxN  = float(fp.readline().split()[1]) 
        self.lyN  = float(fp.readline().split()[1]) 
        self.lzN  = float(fp.readline().split()[1]) 

        self.k_L  = float(fp.readline().split()[1])   # Spring stiffness N/m to lfb/ft
        self.k_R  = self.k_L
        self.k_N  = float(fp.readline().split()[1]) 

        self.c_L  = float(fp.readline().split()[1])   # Damping coefficient Ns/m to lbfs/ft
        self.c_R  = self.c_L
        self.c_N  = float(fp.readline().split()[1]) 

        Ixx = self.Ixx
        Iyy = self.Iyy
        Izz = self.Izz
        Ixz = self.Ixz

        self.g = 9.8
        self.W = self.M * self.g
        self.mu = float(fp.readline().split()[1]) 


        # Moment of inertial based coefficients
        self.c1   = ((Iyy-Izz)*Izz-Ixz**2)/(Ixx*Izz-Ixz**2);
        self.c2   = (Ixx-Iyy+Izz)*Ixz/(Ixx*Izz-Ixz**2);
        self.c3   = Izz/(Ixx*Izz-Ixz**2);
        self.c4   = Ixz/(Ixx*Izz-Ixz**2);
        self.c5   = (Izz-Ixx)/Iyy;
        self.c6   = Ixz/Iyy;
        self.c7   = 1/Iyy;
        self.c8   = ((Ixx-Iyy)*Ixx-Ixz**2)/(Ixx*Izz-Ixz**2);
        self.c9   = Ixx/(Ixx*Izz-Ixz**2);

        # Parameters required to build full aerodynamic database
        # see Reference Jared A. Grauer and Eugene A. Morelli
        self.t    = np.zeros((1,45));

        # Comment line
        fp.readline()
        
        for i in np.arange(45):
            self.t[0,i] = float(fp.readline())

    
    ## Function to generate aerodynamic coefficients based on 
    #  the orthogonal basis function method
    def GenCoeff(self,alpha,beta,pt,qt,rt,del_e,del_a,del_r):
        
        """
        Function to generate aerodynamic coefficients
        - based off Jared and Morelli's paper
        returns:
        C = [C_D, C_E, C_L, C_l, C_m, C_n, C_X, C_Y, C_Z]
        """

        C_Dv = np.array([1, alpha,  alpha*qt,    alpha*del_e,        \
                         alpha**2, alpha**2*qt, alpha**2*del_e, \
                         alpha**3, alpha**3*qt, alpha**4])

        C_Yv = np.array([beta, pt, rt, del_a, del_r])

        C_Lv = np.array([1, alpha, qt, del_e, alpha*qt, alpha**2, alpha**3, alpha**4])

        C_lv = np.array([beta, pt, rt, del_a, del_r])

        C_mv = np.array([1, alpha, qt, del_e, alpha*qt, alpha**2*qt, alpha**2*del_e, alpha**3*qt, \
                alpha**3*del_e, alpha**4])

        C_nv = np.array([beta, pt, rt, del_a, del_r, beta**2, beta**3]);


        C_D = np.dot(C_Dv,self.t[0,0:10].T);
        C_Y = np.dot(C_Yv,self.t[0,10:15].T);
        C_L = np.dot(C_Lv,self.t[0,15:23].T);
        C_l = np.dot(C_lv,self.t[0,23:28].T);
        C_m = np.dot(C_mv,self.t[0,28:38].T);
        C_n = np.dot(C_nv,self.t[0,38:45].T);

        C_E = (C_Y + sin(beta)*C_D)/-cos(beta);
        C_X = -cos(alpha)*cos(beta)*C_D+cos(alpha)*sin(beta)*C_E+sin(alpha)*C_L;
        C_Z = -sin(alpha)*cos(beta)*C_D+sin(alpha)*sin(beta)*C_E-cos(alpha)*C_L;

        C = [C_D, C_E, C_L, C_l, C_m, C_n, C_X, C_Y, C_Z]

        return C


    ## Function that determines the gear force and moments
    #  @param x state vector
    #  @param u control input
    #  @param LG landing gear input
    def GearFM(self,x,u,LG):

        """
        Function to calculate gear force and moments
        """

        # If landing gear is down (1), calculate gear force and moments
        # Lateral gear loads is neglected for simplicity
        if(LG == 1):
        
            F,M = GearDynamics(self,x)
            Fx,Fy,Fz = F[0],F[1],F[2]
            Mx,My,Mz = M[0],M[1],M[2]
        
        else:        
            Fx,Fy,Fz = 0.0, 0.0, 0.0
            Mx,My,Mz = 0.0, 0.0, 0.0
        

        return [Fx,Fy,Fz,Mx,My,Mz]

    ## Function that returns the Body to Earth rotation matrix
    #  @param roll roll angle
    #  @param pitch pitch angle
    #  @param yaw yaw angle
    def DCM(self,roll,pitch,yaw):

        """
        Rotation matrix from body to earth frame
        """

        R  = np.zeros((3,3));
        
        R  = [[cos(pitch)*cos(yaw),
               sin(roll)*sin(pitch)*cos(yaw) - cos(roll)*sin(yaw),
               cos(roll)*sin(pitch)*cos(yaw) + sin(roll)*sin(yaw)],
              [cos(pitch)*sin(yaw),
               sin(roll)*sin(pitch)*sin(yaw) + cos(roll)*cos(yaw),
               cos(roll)*sin(pitch)*sin(yaw) - sin(roll)*cos(yaw)],
              [-sin(pitch),   sin(roll)*cos(pitch),   cos(roll)*cos(pitch) ]];
                      
        return R
    
    

    ## Function that returns the xdot vector for the aircraft
    #  @param x state vector 
    #  @param t current time 
    #  @param u control vector
    #  @param LG landing gear switch
    def Dynamics(self,x,t,u,LG):
        
        """
        Dynamics used for simulation
        """

        U     = x[0];   #  body velocity x component
        V     = x[1];   #  body velocity y component
        W     = x[2];   #  body velocity z component
        P     = x[3];   #  angular rate about x
        Q     = x[4];   #  angular rate about y
        R     = x[5];   #  angular rate about z
        
        phi   = x[6];   #  roll
        theta = x[7];   #  pitch
        psi   = x[8];   #  yaw
         
        Xe    = x[9];   #  x position
        Ye    = x[10];  #  y position
        h     = x[11];  #  Altitude
        
        g     = 9.8     #  Acceleration due to gravity in 
        
        del_e = u[0];   #  elevator
        del_a = u[1];   #  aileron
        del_r = u[2];   #  rudder
        del_T = u[3];   #  throttle

        WindX = self.Wind[0]
        WindY = self.Wind[1]
        WindZ = self.Wind[2]


        S     = self.Sref
        M     = self.M
        c     = self.c
        b     = self.b

        c1    = self.c1 
        c2    = self.c2 
        c3    = self.c3 
        c4    = self.c4 
        c5    = self.c5 
        c6    = self.c6 
        c7    = self.c7
        c8    = self.c8 
        c9    = self.c9 

        heng  = 0;

        Tstar = 1 - 0.703*10**(-5)*np.fabs(h) * 3.28084;
        rho   = 0.002377*Tstar**(4.14) * 515.4 ;
        
        # True airspeed
        U0    = np.sqrt(U**2 + V**2 + W**2)

        # Start from 1 if U = 0 (To prevent singularities)
        if(U == 0.0):
            U  = 1
            U0 = 1

        alpha,beta,q,pt,qt,rt = 0,0,0,0,0,0

        # Angle of attack
        alpha = np.arctan(W/U);

        # Side slip angle
        beta  = np.arcsin(V/U0);

        # Dynamic pressure
        q     = 0.5*rho*U0**2;

        pt    = b/(2*U0)*x[3];
        qt    = c/(2*U0)*x[4];
        rt    = b/(2*U0)*x[5];
        
        C         = self.GenCoeff(alpha,beta,pt,qt,rt,del_e,del_a,del_r)

        Gear      = self.GearFM(x,u,LG)
                
        udot      = R*V - Q*W - g*sin(theta)          + q*S*C[6]/M + del_T/M + Gear[0]/M;
        vdot      = P*W - R*U + g*cos(theta)*sin(phi) + q*S*C[7]/M + Gear[1]/M;
        wdot      = Q*U - P*V + g*cos(theta)*cos(phi) + q*S*C[8]/M + Gear[2]/M;
        
        pdot      = (c1*R + c2*P + c4*heng)*Q + q*S*b*(c3*C[3] + c4*C[5]) + c3*Gear[3] + c4*Gear[5];
        qdot      = (c5*P - c7*heng)*R        - c6*(P**2 - R**2) + q*S*c*c7*C[4] + c7*Gear[4];
        rdot      = (c8*P - c2*R + c9*heng)*Q + q*S*b*(c4*C[3] + c9*C[5]) + c4*Gear[3] + c9*Gear[5];

        phidot    = P + np.tan(theta)*(Q*sin(phi) + R*cos(phi));
        thetadot  = Q * cos(phi) - R*sin(phi);
        psidot    = (Q*sin(phi) + R*cos(phi))/cos(theta);

        xedot     = U*cos(psi)*cos(theta) + \
                    V*(cos(psi)*sin(theta)*sin(phi) - sin(psi)*cos(phi)) + \
                    W*(cos(psi)*sin(theta)*sin(phi) + sin(psi)*sin(phi)) - WindX

        yedot     = U*sin(psi)*cos(theta) + \
                    V*(sin(psi)*sin(theta)*sin(phi) + cos(psi)*cos(phi)) + \
                    W*(sin(psi)*sin(theta)*cos(phi) - cos(psi)*sin(phi)) - WindY

        hdot      = U*sin(theta) - V*cos(theta)*sin(phi) - W*cos(theta)*cos(phi) - WindZ

        U0dot     = (U*udot + V*vdot + W*wdot)/U0
    
        alphadot  = 0

        betadot   = 0

        # Derivative
        dx        = [udot,vdot,wdot,pdot,qdot,rdot,phidot,thetadot,
                     psidot,xedot,yedot,hdot,U0dot,alphadot,betadot]

        return dx

    def Dynamics2(self,x,t,SIM,INPUT):
        
        """
        Dynamics used for simulation
        """

        U     = x[0];   #  body velocity x component
        V     = x[1];   #  body velocity y component
        W     = x[2];   #  body velocity z component
        P     = x[3];   #  angular rate about x
        Q     = x[4];   #  angular rate about y
        R     = x[5];   #  angular rate about z
        
        phi   = x[6];   #  roll
        theta = x[7];   #  pitch
        psi   = x[8];   #  yaw
         
        Xe    = x[9];   #  x position
        Ye    = x[10];  #  y position
        h     = x[11];  #  Altitude
        
        g     = 9.8     #  Acceleration due to gravity in 
        
        u,LG  = INPUT(SIM,t,x)

        WindX = SIM.Wind[0]
        WindY = SIM.Wind[1]
        WindZ = SIM.Wind[2]

        del_e = u[0];   #  elevator
        del_a = u[1];   #  aileron
        del_r = u[2];   #  rudder
        del_T = u[3];   #  throttle

        S     = self.Sref
        M     = self.M
        c     = self.c
        b     = self.b

        c1    = self.c1 
        c2    = self.c2 
        c3    = self.c3 
        c4    = self.c4 
        c5    = self.c5 
        c6    = self.c6 
        c7    = self.c7
        c8    = self.c8 
        c9    = self.c9 

        heng  = 0;

        Tstar = 1 - 0.703*10**(-5)*np.fabs(h) * 3.28084;
        rho   = 0.002377*Tstar**(4.14) * 515.4 ;
        
        # True airspeed
        U0    = np.sqrt(U**2 + V**2 + W**2)

        # Start from 1 if U = 0 (To prevent singularities)
        if(U == 0.0):
            U  = 1
            U0 = 1

        alpha,beta,q,pt,qt,rt = 0,0,0,0,0,0

        # Angle of attack
        alpha = np.arctan(W/U);

        # Side slip angle
        beta  = np.arcsin(V/U0);

        # Dynamic pressure
        if(headwind):
            q     = 0.5*rho*(U0 + Vwind)**2;
        else:
            q     = 0.5*rho*(U0 - Vwind)**2;

        pt    = b/(2*U0)*x[3];
        qt    = c/(2*U0)*x[4];
        rt    = b/(2*U0)*x[5];
        
        C         = self.GenCoeff(alpha,beta,pt,qt,rt,del_e,del_a,del_r)

        Gear      = self.GearFM(x,u,LG)
                
        udot      = R*V - Q*W - g*sin(theta)          + q*S*C[6]/M + del_T/M + Gear[0]/M;
        vdot      = P*W - R*U + g*cos(theta)*sin(phi) + q*S*C[7]/M + Gear[1]/M;
        wdot      = Q*U - P*V + g*cos(theta)*cos(phi) + q*S*C[8]/M + Gear[2]/M;
        
        pdot      = (c1*R + c2*P + c4*heng)*Q + q*S*b*(c3*C[3] + c4*C[5]) + c3*Gear[3] + c4*Gear[5];
        qdot      = (c5*P - c7*heng)*R        - c6*(P**2 - R**2) + q*S*c*c7*C[4] + c7*Gear[4];
        rdot      = (c8*P - c2*R + c9*heng)*Q + q*S*b*(c4*C[3] + c9*C[5]) + c4*Gear[3] + c9*Gear[5];

        phidot    = P + np.tan(theta)*(Q*sin(phi) + R*cos(phi));
        thetadot  = Q * cos(phi) - R*sin(phi);
        psidot    = (Q*sin(phi) + R*cos(phi))/cos(theta);

        xedot     = U*cos(psi)*cos(theta) + \
                    V*(cos(psi)*sin(theta)*sin(phi) - sin(psi)*cos(phi)) + \
                    W*(cos(psi)*sin(theta)*sin(phi) + sin(psi)*sin(phi)) - WindX

        yedot     = U*sin(psi)*cos(theta) + \
                    V*(sin(psi)*sin(theta)*sin(phi) + cos(psi)*cos(phi)) + \
                    W*(sin(psi)*sin(theta)*cos(phi) - cos(psi)*sin(phi)) - WindY

        hdot      = U*sin(theta) - V*cos(theta)*sin(phi) - W*cos(theta)*cos(phi) - WindZ

        U0dot     = (U*udot + V*vdot + W*wdot)/U0
    
        alphadot  = (U*wdot - W*udot)/np.sqrt(U**2 + W**2)

        betadot   = (U0*vdot - V*U0dot)/(U0**2 * np.sqrt( 1 - (V/U0)**2 ) )

        # Derivative
        dx        = [udot,vdot,wdot,pdot,qdot,rdot,phidot,thetadot,
                     psidot,xedot,yedot,hdot,U0dot,alphadot,betadot]

        return dx


    def SimDyn(self,x,t,args):

        """
        wrapper for dynamics
        """

        U,LG  = args[0],args[1]
                
        y     =  self.Dynamics(x,t,U,LG)

        return y

    
