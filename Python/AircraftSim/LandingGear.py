"""
Function to compute landing gear forces and moments
- Simplified version of the simulink model
- See SciTech'14 paper for full model
- Neglecting wheel dynamics (rotational speed of wheels)
"""

import numpy as np
from numpy import sin,cos,arctan

def GearDynamics(self,x):

    
    U,V,W  = x[0],x[1],x[2]
    pb     = x[3]
    qb     = x[4]
    rb     = x[5]
    roll   = x[6]
    pitch  = x[7]
    yaw    = x[8]

    Ze     = -x[11]

    Ve     = np.dot( self.DCM(roll,pitch,yaw),np.array([[U],[V],[W]]))

    Vac    = Ve[2,0]

    steer  = 0

    lxL,lyL,lzL = self.lxL,self.lyL,self.lzL
    lxR,lyR,lzR = self.lxR,self.lyR,self.lzR
    lxN,lyN,lzN = self.lxN,self.lyN,self.lzN

    k_L,k_R,k_N = self.k_L,self.k_R,self.k_N
    c_L,c_R,c_N = self.c_L,self.c_R,self.c_N
    
    #Oleo locations in the body frame
    rposL  = np.array([ [lxL], [lyL], [0] ]);
    rposR  = np.array([ [lxR], [lyR], [0] ]);
    rposN  = np.array([ [lxN], [lyN], [0] ]);
    
    #Oleo locations in the inertial frame
    oleoL  = np.dot(self.DCM(roll,pitch,yaw),rposL)
    oleoR  = np.dot(self.DCM(roll,pitch,yaw),rposR)
    oleoN  = np.dot(self.DCM(roll,pitch,yaw),rposN)

    #Deflection rate of oleos in inertial frame due to attitude changes (using transport theorem)
    w_cross  = np.array([[0,-rb, qb],[rb,0,-pb],[-qb,pb,0]])
    hcat     = np.hstack((rposL,rposR,rposN))

    oleodotB = np.dot(w_cross,hcat)

    oleodot  = np.dot(self.DCM(roll,pitch,yaw),oleodotB)

    #Total deflection of oleos - z direction
    oleoLz   = oleoL[2] + Ze;
    oleoRz   = oleoR[2] + Ze;
    oleoNz   = oleoN[2] + Ze;

    #Total deflection rate of oleos 
    oleoLdot = oleodot[2,0]  + Vac;
    oleoRdot = oleodot[2,1]  + Vac;
    oleoNdot = oleodot[2,2]  + Vac;

    #Normal load on the gears
    Fsn = -k_N * oleoNz;
    Fdn = -c_N * oleoNdot;

    Fsl = -k_L * oleoLz;
    Fdl = -c_L * oleoLdot;

    Fsr = -k_R * oleoRz;
    Fdr = -c_R * oleoRdot;

    FzL = Fsl+Fdl; 
    FzR = Fsr+Fdr;
    FzN = Fsn+Fdn;

    if(FzL >= 0):
        FzL=0;
    
    if(FzR >= 0):
        FzR=0
    
    if(FzN >= 0):
        FzN=0

    #Coefficient of fricition related through the magic formula
    # Simplified by specifying a standard value for friction coeff
    muL   = self.mu
    muR   = self.mu
    muN   = self.mu

    #Longitudinal loads on the wheels
    FxL   = -muL*np.fabs(FzL); 
    FxR   = -muR*np.fabs(FzR);
    FxN   = -muN*np.fabs(FzN);

    #Lateral wheel slip
    if(U > 0):
        alpL = arctan(V/U)
        alpR = arctan(V/U)
        alpN = -steer + arctan(V/U)
    else:
        alpL = 0
        alpR = 0
        alpN = 0

    FymaxL  = -7.39e-7 * FzL**2 + 5.11e-1*FzL
    FymaxR  = -7.39e-7 * FzR**2 + 5.11e-1*FzR
    FymaxN  = -3.53e-6 * FzN**2 + 8.33e-1*FzN

    alpoptN = 3.52e-9 *FzN**2 + 2.80e-5*FzN + 13.8
    alpoptL = 1.34e-10*FzL**2 + 1.06e-5*FzL + 6.72
    alpoptR = 1.34e-10*FzR**2 + 1.06e-5*FzR + 6.72

    #Lateral loads on the wheels
    FyL   = -(-2*(FymaxL * alpoptL*alpL)/(alpoptL**2 + alpL**2))
    FyR   = -(-2*(FymaxR * alpoptR*alpR)/(alpoptR**2 + alpR**2))
    FyN   = -(-2*(FymaxN * alpoptN*alpN)/(alpoptN**2 + alpN**2))
    
  
    #Total reaction forces on the Aircraft exerted by ground in the Earth Frame
    FX    = FxL + FxR + FxN*cos(steer) + FyN*sin(steer);
    FY    = FyR + FyL + FyN*cos(steer) + FxN*sin(steer);
    FZ    = FzL + FzR + FzN;

    #Converting FZ from earth to body frame
    Fz_b  = np.dot(np.transpose(self.DCM(roll,pitch,yaw)),np.array([[0],[0],[FZ]]));

    #Adding the all the Force components in the Body Frame
    FX    = FX + Fz_b[0];
    FY    = FY + Fz_b[1];
    FZ    = Fz_b[2];

    epos = np.dot(self.DCM(roll,pitch,yaw),hcat)
                                                                          
    lxLe = epos[0,0];
    lxRe = epos[0,1];
    lxNe = epos[0,2];
          
    #Total Moment on the Aircraft body frame
    MX    = lyL*(FzL) + lyR*(FzR) + lzL*(-FyL) + lzL*(-FyR) + lzN*(-FyN)*cos(steer) + lzN*(-FxN)*sin(steer);
    MY    = lxNe*(-FzN) + lxRe*(-FzR) + lxLe*(-FzL) + lzN*(FxN)*cos(steer) + lzN*(-FyN)*sin(steer) + lzR*FxR + lzL*FxL;
    MZ    = lyR*(-FxR) + lyL*(-FxL) + lxRe*(FyR) + lxLe*(FyL) + lxNe*(FyN)*cos(steer) + lxNe*(FxN)*sin(steer);
            
    #Force and Moment on the aircraft due  to the undercarriage
    Fgear = [FX,FY,FZ];
    Mgear = [MX,MY,MZ];

    return Fgear,Mgear
