"""
Utility function to support Monte Carlo simulations
"""
import random as rd
from numpy import fabs
import numpy as np

import os,sys
libpath = os.path.abspath('../Utilities/')
sys.path.append(libpath)

from Sampling import *

def genSample(sim):
    """
    Function to generate random samples for Monte Carlo simulation
    """
    #Generate the guassian random variables
    vr           = rd.gauss(sim.vr_mu,sim.vr_sig)
    kpe          = rd.gauss(sim.kpe_mu,sim.kpe_sig)
    n            = int(fabs(rd.gauss(sim.n_mu,sim.n_sig)))
    pitch_ref    = rd.gauss(sim.pref_mu,sim.pref_sig)
    kpr          = rd.gauss(sim.kpr_mu,sim.kpr_sig)
    y0           = rd.gauss(sim.y0_mu,sim.y0_sig)
    Tmax         = rd.gauss(sim.T_mu,sim.T_sig)
    x0           = rd.gauss(sim.x0_mu,sim.x0_sig)
    u0           = rd.gauss(sim.u0_mu,sim.u0_sig)

    # Truncate gaussian distribution
    vr           = Truncate(vr, sim.vr_min, sim.vr_max)
    kpe          = Truncate(kpe, sim.kpe_min, sim.kpe_max)
    n            = Truncate(n, sim.n_min, sim.n_max)
    pitch_ref    = Truncate(pitch_ref, sim.pref_min, sim.pref_max)
    kpr          = Truncate(kpr, sim.kpr_min, sim.kpr_max)
    y0           = Truncate(y0, sim.y0_min, sim.y0_max)
    Tmax         = Truncate(Tmax, sim.T_min, sim.T_max)
    x0           = Truncate(x0, sim.x0_min, sim.x0_max)
    u0           = Truncate(u0, sim.u0_min, sim.u0_max)

    kde          = 1
    n            = 1
    kdr          = 0.1
    kyp          = -1

    sim.engStatus  = SampleArb(sim.Enghealth_dist)
    sim.failpoint  = np.random.uniform(0.1,1)*sim.Aircraft.Params[12]  

    sim.engfailure = 0

    sim.RTO        = SampleArb([sim.properRTO,1-sim.properRTO])

    sim.modeSwitchIndex = rd.uniform(2,sim.size_t)

    if(sim.exploringStarts == True):

        ind = rd.randint(1,4)

        if(ind == 1):
            x0           = rd.uniform(100,800)
            u0           = rd.uniform(2,20)
        elif(ind == 2):
            x0           = rd.uniform(100,800)
            u0           = rd.uniform(2,20)
        elif(ind == 3):
            x0           = rd.uniform(200,300)
            u0           = rd.uniform(20,50)
        elif(ind == 4):
            x0           = rd.uniform(300,800)
            u0           = rd.uniform(20,50)


    return [vr,kpe,kde,n,pitch_ref,Tmax,kpr,kdr,kyp],[x0,y0,u0]

         
def Update(sim,t,x0):
    """
    Function used by GenSim to generate inputs for the takeoff 
    simulation
    """

    i          = int(round(t/sim.dt))
    sim.x0     = sim.Aircraft.ChkTailStrike(sim.x0)
    
    # Initiate engine failure based on sampling
    if(sim.x0[9] > sim.failpoint):
        sim.engfailure = sim.engStatus

    # Perform mode switched if mode switches are enabled
    if(sim.modeSwitch == True and i >= sim.modeSwitchIndex):
        if( sim.Aux[0,i-1] == 0 ):
            sim.Aux[0,i] = 1
        else:
            sim.Aux[0,i] = 0
      
    # Compute control input for current mode
    if(sim.Aux[0,i] == 0 ):
        U          = sim.Pilot.getPilotInput(sim)
    else:
        U          = sim.EA.getEAInput(sim)

    # Landing gear is always down for takeoff
    LG         = 1

    # Store control inputs and aux variables
    sim.Control[:,i] = U
    sim.Aux[1,i]     = sim.engfailure;    # Engine failure

    # Stopping criteria
    if( int(sim.engfailure) != 0 and sim.x0[12] < 0.5 ):
        #print "Stopping simulation"
        sim.stop = True
    
    if( (sim.x0[9] > sim.Aircraft.Params[12] + 100) or (sim.x0[12] > 90) ):
        #print "Stopping simulation"
        sim.stop = True

    if( ( sim.x0[12] < 1 ) and ( U[3] < 1 ) ):
        #print "Stopping simulation"
        sim.stop = True


    return U,LG


    
    
