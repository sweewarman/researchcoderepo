"""
 Monte Carlo simulations to analyze takeoff dynamics
"""

import os,sys

libpath1 = os.path.abspath('../TakeoffSim/')
libpath2 = os.path.abspath('../GenSim/')
sys.path.append(libpath1)
sys.path.append(libpath2)

from GenSim import *
from TK_Aircraft import *
from PilotModel import *
from EAModel import *
from Utilities import *
import random as rd
import shelve


model =  '../AircraftModels/GTM.dat'
perf  =  '../AircraftModels/GTM_performance.dat'
#====================================================================

# Aircraft state
# u,v,w,p,q,r,roll,pitch,yaw,Xe,Ye,h,U,alpha,beta
x0     = [0.1,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0]

# Simulation time span and timestep
tspan  = 20
dt     = 0.1

GTM          = TK_Aircraft(model,perf)
SIM          = GenSim(dt,tspan,x0)
SIM.Aircraft = GTM
SIM.Pilot    = PilotModel()
SIM.EA       = EAModel()

# Set simulation update and output function
SIM.SetUpdateFunc(Update)
SIM.SetOutputFunc(GTM.SimDyn)

GTM.Params[12] = 800                    # Runway length
GTM.Params[13] = 0                      # Wind
GTM.hTS        = 10/3.2 - 1             # Tail strike height

# Compute takeoff performance for selected aircraft
GTM.Compute_Performance(GTM.Params)     


# Initialize data structure to record all Monte carlo trials
Trials  = 2
YOUT    = np.zeros((Trials,SIM.size_x,SIM.size_t))
UIN     = np.zeros((Trials,4,SIM.size_t))
AUX     = np.zeros((Trials,2,SIM.size_t))
STOP    = np.zeros((Trials,2))
Vr      = (GTM.Params[14] + (GTM.Params[1] - GTM.Params[13]) )/2
Thrust  = GTM.Params[19]

#Max and Min values for the Monte carlo variables
SIM.vr_min  ,SIM.vr_max      = Vr-10,Vr+10
SIM.kpe_min ,SIM.kpe_max     = -0.7*4,-0.05*4
SIM.n_min   ,SIM.n_max       = 0,2
SIM.pref_min,SIM.pref_max    = 7,13
SIM.kpr_min ,SIM.kpr_max     = -0.7,-0.05
SIM.y0_min  ,SIM.y0_max      = -1,1
SIM.T_min   ,SIM.T_max       = 150000,Thrust+10000
SIM.x0_min  ,SIM.x0_max      = 0,40
SIM.u0_min  ,SIM.u0_max      = 0.1,20
SIM.x0es_min,SIM.x0es_max    = 10,GTM.Params[12]
SIM.u0es_min,SIM.u0es_max    = 3,70

#Mean and standard deviations for control parameters
SIM.vr_mu  ,SIM.vr_sig       = Vr     ,5
SIM.kpe_mu ,SIM.kpe_sig      = -2.5   ,3
SIM.n_mu   ,SIM.n_sig        = 1      ,1
SIM.pref_mu,SIM.pref_sig     = 8      ,0
SIM.kpr_mu ,SIM.kpr_sig      = -0.25  ,0.25
SIM.y0_mu  ,SIM.y0_sig       = 0      ,0.5
SIM.T_mu   ,SIM.T_sig        = Thrust ,100000    
SIM.x0_mu  ,SIM.x0_sig       = 0      ,10
SIM.u0_mu  ,SIM.u0_sig       = 1      ,20

# Probability distribution of discrete events
SIM.Enghealth_dist = [1.0,0.0,0.0]   # [aeo,oei,faill]

# Probability of a proper pilot RTO
SIM.properRTO = 1.0 

# Array to record control inputs
SIM.Control  = np.zeros((4,SIM.size_t))

# Array to record auxiliary states 
# 1. Mode, 2. Eng Status
SIM.Aux      = np.zeros((2,SIM.size_t))


# Set to true if mode switches are also simulated
SIM.modeSwitch = False
SIM.exploringStarts = False

mode         = 0  # Set mode to pilot

# EA Parameters
EA_params    = [Vr,-3,1,0,8,Thrust,-0.25,0.1,-1,9,3]
SIM.EA.Init(EA_params)

for i in np.arange(Trials):

    """
    # Divide trials between pilot and EA controller
    if(i < Trials/2):
        mode = 0
        SIM.exploringStarts = False
        SIM.modeSwitch = False
    elif(i < 0.75*Trials/2 ):
        mode = 0
        SIM.exploringStarts = False
        SIM.modeSwitch = True
    else:
        mode = 1
        SIM.modeSwitch = False
        # Select exploring start options to diversify trials
        if(i < 3./4. * Trials):
            SIM.exploringStarts = False
        else:
            SIM.exploringStarts = True
            if(i >= 0.9*Trials):
                SIM.modeSwitch = True
    """

    # Reinitialize parameters that need to be reset for each trial
    SIM.stop = False
    SIM.stop_time = [SIM.Time[-1],SIM.size_t]
        
    SIM.Aux[0,:]     = mode  
    SIM.Aux[1,:]     = -1  
    SIM.yout[:,:]    = 0
    SIM.Control[:,:] = 0

    # Sample parameters from specified distribution
    params1,params2    = genSample(SIM)

    # Initialize pilot model with random parameters
    SIM.Pilot.Init(params1)

    EA_params[5] = params1[5]
    SIM.EA.Init(EA_params)
    
    x0[9]         = params2[0]
    x0[10]        = params2[1]
    x0[0]         = params2[2]
    x0[12]        = params2[2]

    SIM.yout[:,0] = x0

    # Run simulation
    SIM.RunSim()

    # Store inputs and output for analysis
    YOUT[i,:,:]  = SIM.yout
    UIN[i,:,:]   = SIM.Control
    AUX[i,:,:]   = SIM.Aux
    STOP[i,:]    = SIM.stop_time

#=====================================================================
# Save variables for analysis
size_t     = SIM.size_t
Time       = SIM.Time
GTM_params = GTM.Params

SaveData   = ['Trials','YOUT','size_t','Time','GTM_params','UIN','AUX','STOP']

shelf_id  = 'output.out'
my_shelf  = shelve.open(shelf_id,'n')
Workspace = globals()

for key in SaveData:
    
    try:
        my_shelf[key] = globals()[key]
    except TypeError:
        #
        # __builtins__, my_shelf, and imported modules can not be shelved.
        #
        print('ERROR shelving: {0}'.format(key))
    
    my_shelf[key] = Workspace[key]
    

my_shelf.close()


