"""
 Example for using the GenSim class to simulate pendulum dynamics
"""

import os,sys

libpath = os.path.abspath('../../C/ODE/')
sys.path.append(libpath)

from GSLode4Py import py_ode
from pylab import *
import numpy as np

r2d    = 180.0/np.pi
d2r    = 1.0/r2d

# Pendulum state
# theta,thetadot
x0     = [10*d2r,0]

solverparams = np.array([[2.0,10.,0.1]])
params       = np.array([[10.0]])
init         = np.array([[10.0*d2r,0.0]])

Yout   = py_ode(solverparams,params,init)

Time   = Yout[:,0]

# Plot simulation outputs
figure(1)
subplot(211)
plot(Time,Yout[:,1]*r2d);
ylabel(r'\theta')
grid(b=1)
subplot(212)
plot(Time,Yout[:,2]*r2d);
ylabel(r'\dot{\theta}')
grid(b=1)
