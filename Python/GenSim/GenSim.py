"""
GenSim
Generic class for simulating dynamic models - inspired from simulink
 - See Test.py for usage examples
"""
import numpy as np
from scipy.integrate import odeint

class GenSim():

    def __init__(self,dt,tspan,x0):
        """
        Constructor initializes the configuration parameters
        """

        # Conifugration parameters
        self.dt     = dt;
        self.tspan  = tspan;
        self.Time   = np.arange(0,tspan+dt,dt)
        self.size_t = self.Time.shape[0] 
        self.stop   = False
        
        self.tsim   = 0
        self.isim   = 0
        
        self.stop_time =[self.Time[-1],self.size_t]

        self.size_x = len(x0)

        self.yout      = np.zeros((self.size_x,self.size_t))
        self.yout[:,0] = x0
        

    def SetUpdateFunc(self,F):
        """
        Set update function which will be used to compute all the 
        inputs required by the dynamics model called by the ode solver.
        """
        self.process = F
    
    def SetOutputFunc(self,F):
        """
        Set the output function. This will be the function called by
        the ode solver
        """
        self.output = F


    def RunSim(self):
        """
        Run the simulation with the configuration settings initialized
        in the constructor.
        """
        t,i  = 0,0
        while (i < self.size_t-1):

            # Stop simulation if force stop has been enabled
            if(self.stop == True):
                self.stop_time = [t,i]
                break

            self.x0          = self.yout[:,i]
            process_out      = self.process(self,t,self.x0)
            self.yout[:,i+1] = self.Sim1Step(self.dt,self.x0,process_out)
            
                
            t,i              = t + self.dt, i+1
            self.tsim        = t
            self.isim        = i
            
        self.Time   = self.Time[0:self.size_t]


    def RunSim2(self):
        """
        Run the simulation with the configuration settings initialized
        in the constructor.
        """

        df         = lambda x,t: self.output(x,t,self,self.process)

        self.x0         = self.yout[:,0]
        y_result   = odeint(df,self.x0,self.Time)

        self.yout[:,:] = np.transpose(y_result)


    def Sim1Step(self,dt,x,args):
        """
        Function to simulate the next time step with an ode solver
        """

        t          = np.array([0.0,0.0+dt])

        df         = lambda x,t: self.output(x,t,args)

        y_result   = odeint(df,x,t)

        return y_result[1,:]



