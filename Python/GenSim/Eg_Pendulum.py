"""
 Example for using the GenSim class to simulate pendulum dynamics
"""

from GenSim import *
from pylab import *
import numpy as np

r2d    = 180.0/np.pi
d2r    = 1.0/r2d

# Pendulum state
# theta,thetadot
x0     = [10*d2r,0]

# Simulation time span and timestep
tspan  = 20
dt     = 0.01

SIM    = GenSim(dt,tspan,x0)

def Control(sim,t,x0):
    U = 0
    return U

def PendulumDyn(x,t,SIM,INPUT):

    # Acceleration due to gravity
    g   = 9.8

    #Pendulum parameters
    l    = 1.0
    c    = 0.1
    dx   = [0,0]

    # Equations of motion (first order form)
    dx[0] = x[1]
    dx[1] = -g/l*np.sin(x[0]) - c*x[1]

    return dx


SIM.SetUpdateFunc(Control)
SIM.SetOutputFunc(PendulumDyn)
SIM.RunSim2()

# Plot simulation outputs
figure(1)
subplot(211)
plot(SIM.Time,SIM.yout[0,0:SIM.size_t]*r2d);
ylabel(r'\theta')
grid(b=1)
subplot(212)
plot(SIM.Time,SIM.yout[1,0:SIM.size_t]*r2d);
ylabel(r'\dot{\theta}')
grid(b=1)
