"""
 Example for using the GenSim class to simulate aircraft takeoff
 dynamics
"""

import os,sys
libpath = os.path.abspath('../')
sys.path.append(libpath)

from AircraftSim.Aircraft import *

from GenSim import *
from pylab import *

model =  '../AircraftModels/GTM.dat'

# Aircraft state
# u,v,w,p,q,r,roll,pitch,yaw,Xe,Ye,h,U,alpha,beta
x0     = [0.1,0,0,0,0,0,0,0.0,0,0,0,0,0,0,0]

# Simulation time span and timestep
tspan  = 20
dt     = 0.1

GTM    = Aircraft(model)
SIM    = GenSim(dt,tspan,x0)

def Control(sim,t,x0):
    U,LG = [0,0,0,107000*2],1
    return U,LG

SIM.SetUpdateFunc(Control)
SIM.SetOutputFunc(GTM.Dynamics2)
SIM.RunSim2()

# Plot simulation outputs
figure(1)
subplot(311)
plot(SIM.Time,SIM.yout[11,0:SIM.size_t]);
ylabel('H')
grid(b=1)
subplot(312)
plot(SIM.Time,SIM.yout[0,0:SIM.size_t]);
ylabel('U')
grid(b=1)
subplot(313)
plot(SIM.Time,SIM.yout[7,0:SIM.size_t]*180/np.pi);
ylabel('Pitch')
grid(b=1)
