"""
Takeoff FSAM state machine
"""
import numpy as np

r2d = 180.0/np.pi

class FSAM:

    def __init__(self):

        # Current state
        self.state1 = 0
        self.state2 = 0

        # Vspeeds
        self.Vmcg = 0.0
        self.V1   = 0.0
        self.VR   = 0.0
        self.Vlof = 0.0
        self.V2   = 0.0

        # Pitch thresholds
        self.p1   = 0.0
        self.p2   = 0.0
        self.p3   = 0.0

        # Altitude thresholds
        self.h1   = 0.0
        self.h2   = 0.0

        # Cross track thresholds
        self.y1   = 0.0
        self.y2   = 0.0

        # Heading thresholds
        self.d1   = 0.0
        self.d2   = 0.0
        


    def GetMode(self,X,U):
        """
        FSAM state machine for takeoff
        Contains longitudinal and lateral takeoff state machines.
        state1 - current state for longitudinal machine
        state2 - current state for lateral machine
        X      - aircraft state vector
        U      - Control input vector
        
        output 
        state1,state2 - current states
        mode1,mode2   - current mode (1 - pilot, 2 - autopilot)
        """
        
        # Longitudinal state machine
        v = X[12]                  # airspeed
        p = X[7]*r2d               # pitch
        d = X[8]*r2d               # heading
        x = X[9]                   # x-position
        y = np.fabs(X[10])         # y-posiiion
        h = np.fabs(X[11])         # Altitude
        T = U[3]                   # Throttle
    
        mode1,mode2 = -1,-1

        
        # Rest
        if(self.state1 == 1):
            mode1 = 1
            if v > 0:
                self.state1 = 2
            
        # 0 < V < Vmcg
        elif(self.state1 == 2):
            mode1 = 1
            if v > self.Vmcg:
                self.state1 = 3

            if T < 100:
                self.state1 = 14

        # Vmcg <= V <= V1
        elif(self.state1 == 3):
            mode1 = 1
            if v > self.V1:
                self.state1 = 4

            if T < 100:
                self.state1 = 14

        # V1 <= V < VR
        elif(self.state1 == 4):
            mode1 = 1
            if v > self.VR:
                self.state1 = 5

            if p > self.p1:
                self.state1 = 9

        # VR <= V < Vlof
        elif(self.state1 == 5):
            mode1 = 1
            
            if p > self.p2 and h < self.h1:
                self.state1 = 10
            elif v>self.Vlof:
                self.state1 = 6

        # Vlof <= V < V2
        elif(self.state1 == 6):
            mode1 = 1
            
            if p > self.p2 and h < self.h1:
                self.state1 = 11
                #self.state1 = 6
            elif v>self.V2:
                self.state1 = 7

        # V2 <= V < Vfp
        elif(self.state1 == 7):
            mode1 = 1
            if (p > self.p2 and h < self.h1):
                self.state1 = 12
                #self.state1 = 7
            elif v > self.Vfp:
                self.state1 = 7
                        
        elif(self.state1 == 9):
            mode1 = 2
        
            if v > self.VR:
                self.state1 = 5

        elif(self.state1 == 10):
            mode1 = 2

            if(v > self.Vlof):
                self.state1 = 6
                if p > self.p2 and h < self.h1:
                    self.state1 = 11
                    

                elif( h > self.h1 ):
                    self.state1 = 6

        elif(self.state1 == 11):
            mode1 = 2

            if(h > self.h1):
                self.state1 = 6

            elif v  > self.V2:
                if p > self.p2 and h < self.h1:
                    self.state1 = 12


        elif(self.state1 == 12):
            mode1 = 2

            if h > self.h1:
                self.state1 = 7


        #=================================================================
        # Lateral state machine
        
        # Rest
        if(self.state2 == 1):
            mode2 = 1
            if v > 0:
                self.state2 = 2

        # 0 < V < Vmcg
        elif(self.state2 == 2):
            mode2 = 1

            if v > self.Vmcg:
                self.state2 = 3

            if y > self.y1:
                self.state2 = 8

        # Vmcg <= V <= V1
        elif(self.state2 == 3):
            mode2 = 1
            
            if v > self.V1:
                self.state2 = 4

            if y > self.y1:
                self.state2 = 9

        # V1 <= V < VR
        elif(self.state2 == 4):
            mode2 = 1
            
            if v > self.VR:
                self.state2 = 5

            if y > self.y1:
                self.state2 = 10

        # VR <= V < Vlof
        elif(self.state2 == 5):
            mode2 = 1

            if v>self.Vlof:
                self.state2 = 6

            if y > self.y1:
                self.state2 = 11

        # Vlof <= V < V2
        elif(self.state2 == 6):
            mode2 = 1

            if v>self.V2:
                self.state2 = 7

            if y > self.y1:
                self.state2 = 12

        # V2 <= V < Vfp
        elif(self.state2 == 7):
            mode2 = 1

            if v > self.Vfp:
                self.state2 = 7

            if y > self.y1:
                self.state2 = 13
            
        elif(self.state2 == 8):
            mode2 = 2

            if v > self.Vmcg:
                self.state2 = 9

        elif(self.state2 == 9):
            mode2 = 2
        
            if v > self.V1:
                self.state2 = 10

        elif(self.state2 == 10):
            mode2 = 2

            if v > self.VR:
                self.state2 = 11
            
        elif(self.state2 == 11):
            mode2 = 2
        
            if v > self.Vlof:
                self.state2 = 12

        elif(self.state2 == 12):
            mode2 = 2
        
            #if y < self.y1:
                #self.state2 = 6

            if v > self.V2:
                self.state2 = 13

        elif(self.state2 == 13):
            mode2 = 2

            #if y < self.y1:
                #self.state2 = 7
        

        return mode1,mode2
