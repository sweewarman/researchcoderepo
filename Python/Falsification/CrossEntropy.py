import numpy as np

r2d = 180.0/np.pi

def DistRectangle(Bounds, point):
    """
    Function to compute distance of a given point from a rectangular set
    - sign convention used is in accordance with convention described in 
    paper "Falsification of temporal properties" - Fainakos et al.
    """

    x_min = Bounds[0]
    x_max = Bounds[1]
    y_min = Bounds[2]
    y_max = Bounds[3]

    x     = point[0]
    y     = point[1]

    # Point lies inside rectangle
    if( (x_min <= x <= x_max) and (y_min <= y <= y_max) ):
                
        """
        Compute distance from four boundaries
        and return the shortest distance
        """

        dist  = []
        dist.append(np.fabs(x - x_min))
        dist.append(np.fabs(x - x_max))
        dist.append(np.fabs(y - y_min))
        dist.append(np.fabs(y - y_max))
        
        dist  = np.sort(dist)
        
        return -dist[0]

    else:
        
        """
        Compute distance from four boundaries and four corners
        and return the shortest distance
        """
        
        dist = []
        dist.append(np.fabs(x - x_min))
        dist.append(np.fabs(x - x_max))
        dist.append(np.fabs(y - y_min))
        dist.append(np.fabs(y - y_max))
        dist.append( np.sqrt( (x - x_min)**2 + (y - y_min)**2 )  )
        dist.append( np.sqrt( (x - x_min)**2 + (y - y_max)**2 )  )
        dist.append( np.sqrt( (x - x_max)**2 + (y - y_min)**2 )  )
        dist.append( np.sqrt( (x - x_max)**2 + (y - y_max)**2 )  )
        
        dist = np.sort(dist)
        
        return dist[0]

    
def ComputeOmega(SIM,YOUT,V_PRM):
    """
    Compute omega value that is based on the distace from unsafe states
    """
    
    K = 0.1

    Trials = YOUT.shape[0]

    Omega  = np.zeros((Trials,1))
    robust = np.zeros((Trials,1))

    for i in range(Trials):
        
        min_dist = 1000;
        for j in range(YOUT.shape[2]):
            pitch = YOUT[i,7,j]*r2d;
            h     = YOUT[i,11,j];
            xe    = YOUT[i,9,j]
            ye    = YOUT[i,10,j]

            dist1  = DistRectangle([10,100,-10,2.3],[pitch,h])
            dist2  = DistRectangle([0,1000,20,1000],[xe,ye])
            dist3  = DistRectangle([0,1000,-20,-1000],[xe,ye])

            min_dist = min([dist1,dist2,dist3,min_dist])

        Omega[i,0]  = np.exp(-K*min_dist)
        robust[i,0] = min_dist

    return Omega,robust
            

def ComputeGamma(SIM,V_PRM,Omega):

    """
    Compute ratio 
    """
    
    Trials = Omega.shape[0]
    Gamma  = np.zeros((Trials,1))

    for i in range(Trials):
        
        kpe = V_PRM[i,0]

        for j in range(len(SIM.kpe_d)):
            if(SIM.kpe_s[j] <= kpe <= SIM.kpe_s[j+1]):
                cell_prob  = SIM.kpe_d[j]
                uni_prob   = 1.0/( SIM.kpe_s[j+1] - SIM.kpe_s[j])
                total_prob1 = cell_prob * uni_prob

                break;

        vr = V_PRM[i,1]

        for j in range(len(SIM.vr_d)):
            if(SIM.vr_s[j] <= vr <= SIM.vr_s[j+1]):
                cell_prob  = SIM.vr_d[j]
                uni_prob   = 1.0/( SIM.vr_s[j+1] - SIM.vr_s[j])
                total_prob2 = cell_prob * uni_prob

                break;

        pref = V_PRM[i,2]

        for j in range(len(SIM.pref_d)):
            if(SIM.pref_s[j] <= pref <= SIM.pref_s[j+1]):
                cell_prob  = SIM.pref_d[j]
                uni_prob   = 1.0/( SIM.pref_s[j+1] - SIM.pref_s[j])
                total_prob3 = cell_prob * uni_prob

                break;

        kpr = V_PRM[i,3]

        for j in range(len(SIM.kpr_d)):
            if(SIM.kpr_s[j] <= kpr <= SIM.kpr_s[j+1]):
                cell_prob  = SIM.kpr_d[j]
                uni_prob   = 1.0/( SIM.kpr_s[j+1] - SIM.kpr_s[j])
                total_prob4 = cell_prob * uni_prob

                break;


        kyp = V_PRM[i,4]

        for j in range(len(SIM.kyp_d)):
            if(SIM.kyp_s[j] <= kyp <= SIM.kyp_s[j+1]):
                cell_prob  = SIM.kyp_d[j]
                uni_prob   = 1.0/( SIM.kyp_s[j+1] - SIM.kyp_s[j])
                total_prob5 = cell_prob * uni_prob

                break;

        lag = V_PRM[i,5]

        for j in range(len(SIM.lag_d)):
            if(SIM.lag_s[j] <= lag <= SIM.lag_s[j+1]):
                cell_prob  = SIM.lag_d[j]
                uni_prob   = 1.0/( SIM.lag_s[j+1] - SIM.lag_s[j])
                total_prob6 = cell_prob * uni_prob

                break;

        Vwind = V_PRM[i,6]

        for j in range(len(SIM.Vwind_d)):
            if(SIM.Vwind_s[j] <= Vwind <= SIM.Vwind_s[j+1]):
                cell_prob  = SIM.Vwind_d[j]
                uni_prob   = 1.0/( SIM.Vwind_s[j+1] - SIM.Vwind_s[j])
                total_prob7 = cell_prob * uni_prob

                break;

        Thrust = V_PRM[i,7]

        for j in range(len(SIM.Thrust_d)):
            if(SIM.Thrust_s[j] <= Thrust <= SIM.Thrust_s[j+1]):
                cell_prob  = SIM.Thrust_d[j]
                uni_prob   = 1.0/( SIM.Thrust_s[j+1] - SIM.Thrust_s[j])
                total_prob8 = cell_prob * uni_prob

                break;

        total_prob = total_prob1*total_prob2*total_prob3*total_prob4\
                     *total_prob5*total_prob6*total_prob7*total_prob8
    
        if(total_prob > 0):
            Gamma[i,0]  = Omega[i,0]/total_prob
        else:
            Gamma[i,0]  = 0

    return Gamma


def ComputeNewDist(SIM,B_vo):
    """
    Function to tilt old distribution and generate new distribution
    """

    V_PRM = B_vo['kpe']
    gamma = B_vo['G']

    Trials = int(0.5*gamma.shape[0])
    

    for j in range(len(SIM.kpe_d)):
        
        Numerator = 0
        Denominator = 0

        for i in range(Trials):
            
            if(SIM.kpe_s[j] <= V_PRM[i] < SIM.kpe_s[j+1]):
                Numerator = Numerator + gamma[i]

            Denominator = Denominator + gamma[i]
        
        SIM.kpe_d[j] = Numerator/Denominator


    V_PRM = B_vo['vr']

    for j in range(len(SIM.vr_d)):
        
        Numerator = 0
        Denominator = 0

        for i in range(Trials):
            
            if(SIM.vr_s[j] <= V_PRM[i] < SIM.vr_s[j+1]):
                Numerator = Numerator + gamma[i]

            Denominator = Denominator + gamma[i]
        
        SIM.vr_d[j] = Numerator/Denominator

    V_PRM = B_vo['kpr']

    for j in range(len(SIM.kpr_d)):
        
        Numerator = 0
        Denominator = 0

        for i in range(Trials):
            
            if(SIM.kpr_s[j] <= V_PRM[i] < SIM.kpr_s[j+1]):
                Numerator = Numerator + gamma[i]

            Denominator = Denominator + gamma[i]
        
        SIM.kpr_d[j] = Numerator/Denominator


    V_PRM = B_vo['kyp']

    for j in range(len(SIM.kyp_d)):
        
        Numerator = 0
        Denominator = 0

        for i in range(Trials):
            
            if(SIM.kyp_s[j] <= V_PRM[i] < SIM.kyp_s[j+1]):
                Numerator = Numerator + gamma[i]

            Denominator = Denominator + gamma[i]

        SIM.kyp_d[j] = Numerator/Denominator


    V_PRM = B_vo['lag']

    for j in range(len(SIM.lag_d)):
        
        Numerator = 0
        Denominator = 0

        for i in range(Trials):
            
            if(SIM.lag_s[j] <= V_PRM[i] < SIM.lag_s[j+1]):
                Numerator = Numerator + gamma[i]

            Denominator = Denominator + gamma[i]

        SIM.lag_d[j] = Numerator/Denominator

    V_PRM = B_vo['Vwind']

    for j in range(len(SIM.Vwind_d)):
        
        Numerator = 0
        Denominator = 0

        for i in range(Trials):
            
            if(SIM.Vwind_s[j] <= V_PRM[i] < SIM.Vwind_s[j+1]):
                Numerator = Numerator + gamma[i]

            Denominator = Denominator + gamma[i]

        SIM.Vwind_d[j] = Numerator/Denominator


    V_PRM = B_vo['Thrust']

    for j in range(len(SIM.Thrust_d)):
        
        Numerator = 0
        Denominator = 0

        for i in range(Trials):
            
            if(SIM.Thrust_s[j] <= V_PRM[i] < SIM.Thrust_s[j+1]):
                Numerator = Numerator + gamma[i]

            Denominator = Denominator + gamma[i]

        SIM.Thrust_d[j] = Numerator/Denominator

    SIM.RanParamsNew[0,:] = SIM.kpe_d
    SIM.RanParamsNew[1,:] = SIM.vr_d
    SIM.RanParamsNew[2,:] = SIM.pref_d
    SIM.RanParamsNew[3,:] = SIM.kpr_d
    SIM.RanParamsNew[4,:] = SIM.kyp_d
    SIM.RanParamsNew[5,:] = SIM.lag_d
    SIM.RanParamsNew[6,:] = SIM.Vwind_d
    SIM.RanParamsNew[7,:] = SIM.Thrust_d
    
    
