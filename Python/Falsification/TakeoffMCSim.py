"""
 Monte Carlo simulations to analyze takeoff dynamics
"""
import os,sys

libpath = os.path.abspath('../')
sys.path.append(libpath)

import random as rd
import shelve

from GenSim.GenSim import *
from TakeoffSim.TK_Aircraft import *
from TakeoffSim.PilotModel import *
from TakeoffSim.EAModel import *
from HelperFunctions import *
from CrossEntropy import *

model =  '../AircraftModels/GTM.dat'
perf  =  '../AircraftModels/GTM_performance.dat'
#====================================================================

# Aircraft state
# u,v,w,p,q,r,roll,pitch,yaw,Xe,Ye,h,U,alpha,beta
x0     = [0.1,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0]

# Simulation time span and timestep
tspan  = 20
dt     = 0.1
d2r    = np.pi/180.0

GTM          = TK_Aircraft(model,perf)
SIM          = GenSim(dt,tspan,x0)
SIM.Aircraft = GTM
SIM.Pilot    = PilotModel()
SIM.EA       = EAModel()

# Set simulation update and output function
SIM.SetUpdateFunc(Update)
SIM.SetOutputFunc(GTM.SimDyn)

GTM.Params[12] = 800                    # Runway length
GTM.Params[13] = 0                      # Wind
GTM.pitchTS    = 11*d2r
GTM.hTS        = 10/3.2 - 1             # Tail strike height

# Compute takeoff performance for selected aircraft
GTM.Compute_Performance(GTM.Params)     

GTM.Wind = [0.0,0.0,0.0]

# Initialize data structure to record all Monte carlo trials
Trials  = 50
YOUT    = np.zeros((Trials,SIM.size_x,SIM.size_t))
UIN     = np.zeros((Trials,4,SIM.size_t))
AUX     = np.zeros((Trials,4,SIM.size_t))
FSMstate= np.zeros((Trials,2,SIM.size_t))
STOP    = np.zeros((Trials,2))
V_PRM   = np.zeros((Trials,8))

Vr      = (GTM.Params[14] + (GTM.Params[1] - GTM.Params[13]) )/2
Thrust  = GTM.Params[19]

#Max and Min values for the Monte carlo variables
SIM.vr_min  ,SIM.vr_max      = Vr-15,Vr+10
SIM.kpe_min ,SIM.kpe_max     = -7,-0.05
SIM.n_min   ,SIM.n_max       = 0,2
SIM.pref_min,SIM.pref_max    = 12,14
SIM.kpr_min ,SIM.kpr_max     = -2,-0.05
SIM.y0_min  ,SIM.y0_max      = -5,5
SIM.T_min   ,SIM.T_max       = 15000,Thrust+5000
SIM.x0_min  ,SIM.x0_max      = 0,2
SIM.u0_min  ,SIM.u0_max      = 0.1,2

#Mean and standard deviations for control parameters
SIM.vr_mu  ,SIM.vr_sig       = Vr     ,0
SIM.kpe_mu ,SIM.kpe_sig      = -4     ,0
SIM.n_mu   ,SIM.n_sig        =  1     ,0
SIM.pref_mu,SIM.pref_sig     =  8     ,0
SIM.kpr_mu ,SIM.kpr_sig      = -1     ,0
SIM.y0_mu  ,SIM.y0_sig       = -2     ,0
SIM.T_mu   ,SIM.T_sig        = Thrust ,0
SIM.x0_mu  ,SIM.x0_sig       = 0      ,0
SIM.u0_mu  ,SIM.u0_sig       = 1      ,0

# Support 
SIM.kpe_s    = linspace(-5,0,5).tolist()
SIM.vr_s     = linspace(40,70,5).tolist()
SIM.pref_s   = linspace(5,12,5).tolist()
SIM.kpr_s    = linspace(-4,0,5).tolist()
SIM.kyp_s    = linspace(-1,0,5).tolist()
SIM.lag_s    = linspace(0,5,5).tolist()
SIM.Vwind_s  = linspace(0,5,5).tolist()
SIM.Thrust_s = linspace(15000,Thrust+5000,5).tolist()

SIM.RanParamsOld = np.zeros((8,4))
SIM.RanParamsNew = np.zeros((8,4))

#Distribution
SIM.kpe_d    = [0.25,0.25,0.25,0.25]
SIM.vr_d     = [0.25,0.25,0.25,0.25]
SIM.pref_d   = [0.25,0.25,0.25,0.25]
SIM.kpr_d    = [0.25,0.25,0.25,0.25]
SIM.kyp_d    = [0.25,0.25,0.25,0.25]
SIM.lag_d    = [0.25,0.25,0.25,0.25]
SIM.Vwind_d  = [0.25,0.25,0.25,0.25]
SIM.Thrust_d = [0.25,0.25,0.25,0.25]

# Probability distribution of discrete events
SIM.Enghealth_dist = [1.0,0.0,0.0]   # [aeo,oei,faill]

# Probability of a proper pilot RTO
SIM.properRTO  = 0.0 

# Array to record control inputs
SIM.Control    = np.zeros((4,SIM.size_t))

# Array to record auxiliary states 
# 1. Mode, 2. Eng Status
SIM.Aux        = np.zeros((4,SIM.size_t))
SIM.fsmstate   = np.zeros((2,SIM.size_t))

mode           = 0  # Set mode to pilot

# EA Parameters
EA_params      = [Vr,-3,1,0,8,Thrust,-0.25,0.1,-1,9,3]
SIM.EA.Init(EA_params)

TK_FSAM        = FSAM()
TK_FSAM.V1     = GTM.Params[14]
TK_FSAM.V2     = GTM.Params[15]
TK_FSAM.Vmcg   = 0.6*TK_FSAM.V1
TK_FSAM.VR     = 1.1*TK_FSAM.V1
TK_FSAM.Vlof   = 1.1*TK_FSAM.VR
TK_FSAM.Vfp    = 1.2*TK_FSAM.V2

TK_FSAM.p1     = 3
TK_FSAM.p2     = 5

TK_FSAM.h1     = 1
TK_FSAM.h2     = 2.5

TK_FSAM.y1     = 10
TK_FSAM.y2     = 20

TK_FSAM.d1     = 5
TK_FSAM.d2     = 10

TK_FSAM.state1 = 1
TK_FSAM.state2 = 1

SIM.TK_FSAM    = TK_FSAM

OutFile = open('RanParams.txt','w')

convergence = False

oi = 0
"""
while(not convergence):

    print "Iteration:"+str(oi+1)

    SIM.RanParamsOld[0,:] = SIM.kpe_d
    SIM.RanParamsOld[1,:] = SIM.vr_d
    SIM.RanParamsOld[2,:] = SIM.pref_d
    SIM.RanParamsOld[3,:] = SIM.kpr_d
    SIM.RanParamsOld[4,:] = SIM.kyp_d
    SIM.RanParamsOld[5,:] = SIM.lag_d
    SIM.RanParamsOld[6,:] = SIM.Vwind_d
    SIM.RanParamsOld[7,:] = SIM.Thrust_d

    if(oi != 0):
        # Minimize KL divergence
        # Compute omega
        Omega,Robust    = ComputeOmega(SIM,YOUT,V_PRM)
        gamma    = ComputeGamma(SIM,V_PRM,Omega)

        dtype  = [('kpe',float),('vr',float),('pref',float),('kpr',float),('kyp',float),\
                  ('lag',int),('Vwind',float),('Thrust',float),('G',float),('O',float)]
        Values = zip(V_PRM[:,0],V_PRM[:,1],V_PRM[:,2],V_PRM[:,3],V_PRM[:,4],\
                     V_PRM[:,5],V_PRM[:,6],V_PRM[:,7],gamma[:,0],-Omega[:,0])
        # Note: Negative on omega is for sorting purposes
        A_vo   = array(Values,dtype)

        B_vo   = sort(A_vo,order='O')
        
        ComputeNewDist(SIM,B_vo)

        # Check for convergence here
        conv = SIM.RanParamsOld - SIM.RanParamsNew

        D1 = np.dot(conv,np.transpose(conv));
        D2 = np.trace(D1)

        NewDist = SIM.RanParamsNew.tolist()
        for l in NewDist:
            OutFile.write("%s\n"%l)

        if(D2 < 1e-1):
            convergence = True;

    oi = oi + 1        
            
    for i in np.arange(Trials):

        # Reinitialize parameters that need to be reset for each trial
        SIM.stop = False
        SIM.stop_time = [SIM.Time[-1],SIM.size_t]
        
        SIM.Aux[0,:]     = mode  
        SIM.Aux[1,:]     = -1  
        SIM.yout[:,:]    = 0
        SIM.Control[:,:] = 0

        TK_FSAM.state1 = 1
        TK_FSAM.state2 = 1

        # Sample parameters from specified distribution
        params1        = genSample(SIM)
        
        # Initialize pilot model with random parameters
        SIM.Pilot.Init(params1)
        
        EA_params[5]  = params1[5]
        SIM.EA.Init(EA_params)
        
        SIM.yout[:,0] = x0
        
        # Run simulation
        SIM.RunSim()

        # Store inputs and output for analysis
        YOUT[i,:,:]     = SIM.yout
        UIN[i,:,:]      = SIM.Control
        AUX[i,:,:]      = SIM.Aux
        STOP[i,:]       = SIM.stop_time
        FSMstate[i,:,:] = SIM.fsmstate
        V_PRM[i,0]      = params1[1]
        V_PRM[i,1]      = params1[0]
        V_PRM[i,2]      = params1[4]
        V_PRM[i,3]      = params1[6]
        V_PRM[i,4]      = params1[8]
        V_PRM[i,5]      = params1[3]
        V_PRM[i,6]      = SIM.Aircraft.Wind[1]
        V_PRM[i,7]      = params1[5]

#=====================================================================
# Save variables for analysis
size_t      = SIM.size_t
Time        = SIM.Time
GTM_params  = GTM.Params

Constraints = []
Constraints.append(GTM.pitchTS)
Constraints.append(GTM.hTS)
Constraints.append(TK_FSAM.y1)
Constraints.append(TK_FSAM.y2)


SaveData  = ['Trials','YOUT','size_t','Time','GTM_params','UIN','AUX',\
              'STOP','Constraints','FSMstate']

shelf_id  = 'output.out'
my_shelf  = shelve.open(shelf_id,'n')
Workspace = globals()

for key in SaveData:
    
    try:
        my_shelf[key] = globals()[key]
    except TypeError:
        #
        # __builtins__, my_shelf, and imported modules can not be shelved.
        #
        print('ERROR shelving: {0}'.format(key))
    
    my_shelf[key] = Workspace[key]
    

my_shelf.close()
"""
