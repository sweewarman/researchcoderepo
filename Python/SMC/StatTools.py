import numpy as np
from scipy.integrate import quad

def B(t,a,b):
    return (t**(a-1))*(1-t)**(b-1)

def G(x,a,b):
    fac =  quad(B,0,1,args=(a,b))
    r   = (x**(a-1))*((1-x)**(b-1))*1/fac[0]
    return r

# Cumulative distributive function
def F(t,a,b):
    fac = quad(G,0,t,args=(a,b))
    return fac[0];
