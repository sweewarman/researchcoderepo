# Import path of Takeoff class module

import shelve
import numpy as np
from StatTools import F


# Load data for plotting

filename = 'output.out'

ReqData  = ['Time','size_t','YOUT','Trials','GTM_params','UIN','STOP','AUX',\
            'Constraints']
my_shelf = shelve.open(filename)

for key in ReqData:
    globals()[key] = my_shelf[key]

my_shelf.close()
#=====================================================================
r2d = 180.0/np.pi

vln           = 0;

alpha         = 4.12
beta          = 2.76

pitchTS = Constraints[0]
hTS     = Constraints[1]


# Run GTM discrete simulation
for l in np.arange(Trials):

    tc_check    = 0;

    # Run simulation for the above random parameters
    for i in range(size_t):

        altitude = YOUT[l,11,i]
        pitch    = YOUT[l,7,i]*r2d
        
        if(np.fabs(altitude) <= hTS and pitch >= pitchTS and tc_check == 0):
            vln = vln + 1
            tc_check  = 1

    trialn = l+1 

    post = (vln + alpha)/(trialn + alpha + beta)

    t0  = post - 0.1
    t1  = post + 0.1

    if(t0 <= 0):
        t0 = 0
        t1 = 2*0.1
    elif(t1 >= 1):
        t0 = 1 - 2*0.1
        t1 = 1
        
 
    PostProb = F(t1,vln + alpha,trialn-vln+beta) - F(t0,vln + alpha,trialn-vln+beta)
    print PostProb, t0,t1, post

   
