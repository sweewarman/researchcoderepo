import os,sys

libpath = os.path.abspath('../')
sys.path.append(libpath)

from TakeoffSim.TK_Aircraft import *
from pylab import *
import shelve

# Load data for plotting

filename = 'output.out'

ReqData  = ['Time','size_t','YOUT','Trials','GTM_params','UIN','STOP','AUX']
my_shelf = shelve.open(filename)

for key in ReqData:
    globals()[key] = my_shelf[key]

my_shelf.close()

#=====================================================================

r2d = 180.0/np.pi

#fig_env = plot_envelope(GTM_params,partitions=1)

for i in np.arange(Trials):

    size_t  = STOP[i,1]
    Time1   = Time[0:size_t]

    #figure(fig_env.number)
    #plot(YOUT[i,12,0:size_t],YOUT[i,9,0:size_t])

    # Plot simulation outputs
    figure(2)
    subplot(311)
    plot( Time1,YOUT[i,11,0: size_t]+1);
    ylabel('H')
    grid(b=1)
    subplot(312)
    plot( Time1,YOUT[i,12,0: size_t]);
    ylabel('U')
    grid(b=1)
    subplot(313)
    plot( Time1,YOUT[i,7,0: size_t]*r2d);
    ylabel('Pitch')
    grid(b=1)


    figure(3)
    subplot(211)
    plot( Time1,YOUT[i,10,0: size_t]);
    ylabel('Ye')
    grid(b=1)
    subplot(212)
    plot( Time1,YOUT[i,8,0: size_t]*r2d);
    ylabel('Yaw')
    grid(b=1)

    
    figure(4)
    subplot(111)
    plot( Time1,YOUT[i,13,0: size_t]);
    ylabel('alpha')
    grid(b=1)
    
    figure(5)
    subplot(411)
    plot( Time1,UIN[i,0,0: size_t]*r2d);
    ylabel('ele')
    grid(b=1)
    subplot(412)
    plot( Time1,UIN[i,1,0: size_t]*r2d);
    ylabel('ail')
    grid(b=1)
    subplot(413)
    plot( Time1,UIN[i,2,0: size_t]*r2d);
    ylabel('rud')
    grid(b=1)
    subplot(414)
    plot( Time1,UIN[i,3,0: size_t]);
    ylabel('Thrt')
    grid(b=1)

    figure(6)
    subplot(211)
    plot(Time1,YOUT[i,4,0:size_t]*r2d)
    ylabel('pitch rate (deg/s)')
    subplot(212)
    plot(Time1,YOUT[i,7,0:size_t]*r2d)
    ylabel('pitch (deg)')

    figure(7)
    subplot(211)
    plot(Time1,AUX[i,2,0:size_t])
    ylabel('mode1')
    subplot(212)
    plot(Time1,AUX[i,3,0:size_t])
    ylabel('mode2')

show()
