"""
Utility function to support Monte Carlo simulations
"""

import os,sys
libpath = os.path.abspath('../')
sys.path.append(libpath)

import random as rd
from numpy import fabs
import numpy as np

from Utilities.Sampling import *

from FSAM import FSAM

def genSample(sim):
    """
    Function to generate random samples for Monte Carlo simulation
    """
    #Generate the guassian random variables
    vr           = rd.gauss(sim.vr_mu,sim.vr_sig)
    kpe          = rd.gauss(sim.kpe_mu,sim.kpe_sig)
    n            = int(fabs(rd.gauss(sim.n_mu,sim.n_sig)))
    pitch_ref    = rd.gauss(sim.pref_mu,sim.pref_sig)
    kpr          = rd.gauss(sim.kpr_mu,sim.kpr_sig)
    y0           = rd.gauss(sim.y0_mu,sim.y0_sig)
    Tmax         = rd.gauss(sim.T_mu,sim.T_sig)

    # Truncate gaussian distribution
    vr           = Truncate(vr, sim.vr_min, sim.vr_max)
    kpe          = Truncate(kpe, sim.kpe_min, sim.kpe_max)
    n            = Truncate(n, sim.n_min, sim.n_max)
    pitch_ref    = Truncate(pitch_ref, sim.pref_min, sim.pref_max)
    kpr          = Truncate(kpr, sim.kpr_min, sim.kpr_max)
    y0           = Truncate(y0, sim.y0_min, sim.y0_max)
    Tmax         = Truncate(Tmax, sim.T_min, sim.T_max)

    kde          = 1
    n            = 1
    kdr          = 0.2
    kyp          = 0.0

    sim.engStatus  = SampleArb(sim.Enghealth_dist)
    sim.failpoint  = np.random.uniform(0.1,1)*sim.Aircraft.Params[12]  

    sim.engfailure = 0

    sim.RTO        = SampleArb([sim.properRTO,1-sim.properRTO])

    Kpe_index      = SampleArb(sim.kpe_s);

    

    sim.modeSwitchIndex = rd.uniform(2,sim.size_t)

    u0           = rd.uniform(20,50)

    return [vr,kpe,kde,n,pitch_ref,Tmax,kpr,kdr,kyp]


def Update(sim,t,x0):
    """
    Function used by GenSim to generate inputs for the takeoff 
    simulation
    """

    i          = int(round(t/sim.dt))
    sim.x0     = sim.Aircraft.ChkTailStrike(sim.x0)
    
    
    # Initiate engine failure based on sampling
    if(sim.x0[9] > sim.failpoint):
        sim.engfailure = sim.engStatus

    # Use FSAM state machines to determine mode here
    mode1,mode2  = sim.TK_FSAM.GetMode(sim.x0,sim.Control[:,i-1]);

    # Compute control input for current mode
    U1          = sim.Pilot.getPilotInput(sim)
    
    U2          = sim.EA.getEAInput(sim)

    U1[2] = 0

    if(mode1 == 1 and mode2 == 1):
        U = U1
    elif(mode1 == 2 and mode2 == 2):
        U = U2
    elif(mode1 == 1 and mode2 == 2):
        U    = [0.0,0.0,0.0,0.0]
        U[0] = U1[0]
        U[1] = U2[1]
        U[2] = U2[2]
        U[3] = U1[3]
    elif(mode1 == 2 and mode2 == 1):
        U    = [0.0,0.0,0.0,0.0]
        U[0] = U2[0]
        U[1] = U1[1]
        U[2] = U1[2]
        U[3] = U2[3]
    else:
        import pdb; pdb.set_trace()
      
    #U[0] = 0
    #U[2] = 0
    
  
    # Landing gear is always down for takeoff
    LG         = 1

    # Store control inputs and aux variables
    sim.Control[:,i] = U
    sim.Aux[1,i]     = sim.engfailure;    # Engine failure
    sim.Aux[2,i]     = mode1
    sim.Aux[3,i]     = mode2

    # Stopping criteria
    if( int(sim.engfailure) != 0 and sim.x0[12] < 0.5 ):
        #print "Stopping simulation"
        sim.stop = True
    
    if( (sim.x0[9] > sim.Aircraft.Params[12] + 100) or (sim.x0[12] > 90) ):
        #print "Stopping simulation"
        sim.stop = True

    if( ( sim.x0[12] < 0.1 ) and ( U[3] < 1 ) ):
        #print "Stopping simulation"
        sim.stop = True


    if ( sim.x0[12] < 0 ):
        #print "Stopping simulation"
        sim.stop = True

    
    return U,LG
