"""
Functions to define individual probabilities
"""

def ProbV(vj,vi,pi,ri,fi,mi):
    """
    Defines airspeed probabilities
    """

    if(mi == 1):
        # Normal pitch and roll attitude
        if( (pi == 1 or pi == 2) and (ri==1 or ri==2) ):
            if(vi == 1):
                if(vj==1):
                    prob = 0.6
                elif(vj==2):
                    prob = 0.4
                else:
                    prob = 0.0
            
            elif(vi == 2):
                if(vj==1):
                    prob = 0.4
                elif(vj==2):
                    prob = 0.1
                elif(vj == 3):
                    prob = 0.5
                else:
                    prob = 0.0

            elif(vi == 3):
                if(vj==2):
                    prob = 0.1
                elif(vj==3):
                    prob = 0.2
                elif(vj == 4):
                    prob = 0.7
                else:
                    prob = 0.0

            elif(vi == 4):
                if(vj==3):
                    prob = 0.4
                elif(vj==4):
                    prob = 0.5
                elif(vj == 5):
                    prob = 0.1
                else:
                    prob = 0.0

            elif(vi == 5):
                if(vj==4):
                    prob = 0.4
                elif(vj==5):
                    prob = 0.3
                elif(vj == 6):
                    prob = 0.3
                else:
                    prob = 0.0

            elif(vi == 6):
                if(vj==5):
                    prob = 0.3
                elif(vj==6):
                    prob = 0.7
                else:
                    prob = 0.0
                
        # Normal pitch attitude but unusual roll attitude
        elif( (pi == 1 or pi == 2) and ri in [3,4,5,6] ):
            if(vi == 1):
                if(vj==1):
                    prob = 0.1
                elif(vj==2):
                    prob = 0.9
                else:
                    prob = 0.0
            
            elif(vi == 2):
                if(vj==1):
                    prob = 0.1
                elif(vj==2):
                    prob = 0.2
                elif(vj == 3):
                    prob = 0.7
                else:
                    prob = 0.0

            elif(vi == 3):
                if(vj==2):
                    prob = 0.1
                elif(vj==3):
                    prob = 0.2
                elif(vj == 4):
                    prob = 0.7
                else:
                    prob = 0.0

            elif(vi == 4):
                if(vj==3):
                    prob = 0.2
                elif(vj==4):
                    prob = 0.5
                elif(vj == 5):
                    prob = 0.3
                else:
                    prob = 0.0

            elif(vi == 5):
                if(vj==4):
                    prob = 0.3
                elif(vj==5):
                    prob = 0.5
                elif(vj == 6):
                    prob = 0.2
                else:
                    prob = 0.0

            elif(vi == 6):
                if(vj==5):
                    prob = 0.5
                elif(vj==6):
                    prob = 0.5
                else:
                    prob = 0.0


        # High nose up attitude and unusual roll attitude results in 
        # decreasing airspeed
        elif( pi in [3,4]  ):
            if(vi == 1):
                if(vj==1):
                    prob = 0.6
                elif(vj==2):
                    prob = 0.4
                else:
                    prob = 0.0
            
            elif(vi == 2):
                if(vj==1):
                    prob = 0.7
                elif(vj==2):
                    prob = 0.2
                elif(vj == 3):
                    prob = 0.1
                else:
                    prob = 0.0

            elif(vi == 3):
                if(vj==2):
                    prob = 0.7
                elif(vj==3):
                    prob = 0.2
                elif(vj == 4):
                    prob = 0.1
                else:
                    prob = 0.0

            elif(vi == 4):
                if(vj==3):
                    prob = 0.7
                elif(vj==4):
                    prob = 0.2
                elif(vj == 5):
                    prob = 0.1
                else:
                    prob = 0.0

            elif(vi == 5):
                if(vj==4):
                    prob = 0.7
                elif(vj==5):
                    prob = 0.2
                elif(vj == 6):
                    prob = 0.1
                else:
                    prob = 0.0

            elif(vi == 6):
                if(vj==5):
                    prob = 0.9
                elif(vj==6):
                    prob = 0.1
                else:
                    prob = 0.0
                    

        # high nose down attitude and unusual roll attitude results in 
        # decreasing airspeed
        elif( pi in [5,6] ):
            if(vi == 1):
                if(vj==1):
                    prob = 0.1
                elif(vj==2):
                    prob = 0.9
                else:
                    prob = 0.0
            
            elif(vi == 2):
                if(vj==1):
                    prob = 0.1
                elif(vj==2):
                    prob = 0.2
                elif(vj == 3):
                    prob = 0.7
                else:
                    prob = 0.0

            elif(vi == 3):
                if(vj==2):
                    prob = 0.1
                elif(vj==3):
                    prob = 0.2
                elif(vj == 4):
                    prob = 0.7
                else:
                    prob = 0.0

            elif(vi == 4):
                if(vj==3):
                    prob = 0.1
                elif(vj==4):
                    prob = 0.2
                elif(vj == 5):
                    prob = 0.7
                else:
                    prob = 0.0

            elif(vi == 5):
                if(vj==4):
                    prob = 0.3
                elif(vj==5):
                    prob = 0.6
                elif(vj == 6):
                    prob = 0.1
                else:
                    prob = 0.0

            elif(vi == 6):
                if(vj==5):
                    prob = 0.5
                elif(vj==6):
                    prob = 0.5
                else:
                    prob = 0.0


    elif(mi == 2):
        # Normal pitch and roll attitude
        
        if(vi == 1):
            if(vj==1):
                prob = 0.0
            elif(vj==2):
                prob = 0.1
            else:
                prob = 0.0
                
        elif(vi == 2):
            if(vj==1):
                prob = 0.0
            elif(vj==2):
                prob = 0.3
            elif(vj == 3):
                prob = 0.7
            else:
                prob = 0.0
                    
        elif(vi == 3):
            if(vj==2):
                prob = 0.1
            elif(vj==3):
                prob = 0.2
            elif(vj == 4):
                prob = 0.7
            else:
                prob = 0.0

        elif(vi == 4):
            if(vj==3):
                prob = 0.4
            elif(vj==4):
                prob = 0.5
            elif(vj == 5):
                prob = 0.1
            else:
                prob = 0.0

        elif(vi == 5):
            if(vj==4):
                prob = 0.7
            elif(vj==5):
                prob = 0.2
            elif(vj == 6):
                prob = 0.1
            else:
                prob = 0.0

        elif(vi == 6):
            if(vj==5):
                prob = 1.0
            elif(vj==6):
                prob = 0.0
            else:
                prob = 0.0
                

    return prob

def ProbP(pj,pi,mi):
    """
    Function to generate transition probabilities for dynamic pitch states
    """

    if(mi == 1):
        if(pi == 1):
            if(pj == 1):
                prob = 0.6
            elif(pj == 2):
                prob = 0.4
            else:
                prob = 0.0

        elif(pi == 2):
            if(pj == 1):
                prob = 0.4
            elif(pj == 5 or pj == 6):
                prob = 0.3
            else:
                prob = 0.0
            
        elif(pi == 3 or pi == 4):
            if(pj == 2):
                prob = 1
            else:
                prob = 0.0

        elif(pi == 5 or pi == 6):
            if(pj == 5 or pj == 6):
                prob = 0.4
            elif(pj == 2):
                prob = 0.2
            else:
                prob = 0.0

    

    elif(mi == 2):
        if(pi == 1):
            if(pj == 1):
                prob = 0.9
            elif(pj == 2):
                prob = 0.1
            else:
                prob = 0.0

        elif(pi == 2):
            if(pj == 1):
                prob = 0.9
            elif(pj == 5 or pj == 6):
                prob = 0.1
            else:
                prob = 0.0
            
        elif(pi == 3 or pi == 4):
            if(pj == 2):
                prob = 1
            else:
                prob = 0.0

        elif(pi == 5 or pi == 6):
            if(pj == 5 or pj == 6):
                prob = 0.1
            elif(pj == 2):
                prob = 0.8
            else:
                prob = 0.0


    return prob

def ProbR(pj,pi,mi):
    """
    Function to generate transition probabilities for dynamic roll states
    """

    if(mi == 1):
        if(pi == 1):
            if(pj == 1):
                prob = 0.6
            elif(pj == 2):
                prob = 0.4
            else:
                prob = 0.0

        elif(pi == 2):
            if(pj == 1):
                prob = 0.4
            elif(pj == 5 or pj == 6):
                prob = 0.3
            else:
                prob = 0.0
            
        elif(pi == 3 or pi == 4):
            if(pj == 2):
                prob = 1
            else:
                prob = 0.0

        elif(pi == 5 or pi == 6):
            if(pj == 5 or pj == 6):
                prob = 0.4
            elif(pj == 2):
                prob = 0.2
            else:
                prob = 0.0

    elif(mi == 2):
        if(pi == 1):
            if(pj == 1):
                prob = 0.9
            elif(pj == 2):
                prob = 0.1
            else:
                prob = 0.0

        elif(pi == 2):
            if(pj == 1):
                prob = 0.9
            elif(pj == 5 or pj == 6):
                prob = 0.1
            else:
                prob = 0.0
            
        elif(pi == 3 or pi == 4):
            if(pj == 2):
                prob = 1
            else:
                prob = 0.0

        elif(pi == 5 or pi == 6):
            if(pj == 5 or pj == 6):
                prob = 0.1
            elif(pj == 2):
                prob = 0.8
            else:
                prob = 0.0
    
    return prob


def ProbF(fj,fi,mi):

    """
    Defines probabilities for deviation from flight plan 
    """

    if(mi == 1):
        if(fi == 1):
            if(fj==1):
                prob = 0.7
            elif(fj == 2):
                prob = 0.3
            else:
                prob = 0.0

        if(fi == 2):
            if(fj==1):
                prob = 0.4
            elif(fj == 2):
                prob = 0.4
            elif(fj == 3):
                prob = 0.2
            else:
                prob = 0.0

        if(fi == 3):
            if(fj==2):
                prob = 0.4
            elif(fj == 3):
                prob = 0.4
            elif(fj == 4):
                prob = 0.2
            else:
                prob = 0.0

        
                
    if(mi == 2):
        if(fi == 1):
            if(fj==1):
                prob = 0.9
            elif(fj == 2):
                prob = 0.1
            else:
                prob = 0.0

        if(fi == 2):
            if(fj==1):
                prob = 0.9
            elif(fj == 2):
                prob = 0.1
            elif(fj == 3):
                prob = 0.0
            else:
                prob = 0.0

        if(fi == 3):
            if(fj==2):
                prob = 1.0
            elif(fj == 3):
                prob = 0.0
            elif(fj == 4):
                prob = 0.0
            else:
                prob = 0.0

        
                
    return prob


def ProbB(bj,bi,vi,mi):

    if(mi == 1):
        if(vi == 1):
            if(bi == 1):
                if(bj == 1):
                    prob = 0.3
                elif(bj == 2):
                    prob = 0.7
                else:
                    prob = 0

            if(bi == 2):
                if(bj == 1):
                    prob = 0.1
                elif(bj == 2):
                    prob = 0.2
                else:
                    prob = 0.7

            if(bi == 3):
                if(bj == 1):
                    prob = 0
                elif(bj == 2):
                    prob = 0.2
                else:
                    prob = 0.8

        else:
            if(bi == 1):
                if(bj == 1):
                    prob = 0.8
                elif(bj == 2):
                    prob = 0.2
                else:
                    prob = 0

            if(bi == 2):
                if(bj == 1):
                    prob = 0.6
                elif(bj == 2):
                    prob = 0.3
                else:
                    prob = 0.1

            if(bi == 3):
                if(bj == 1):
                    prob = 0
                elif(bj == 2):
                    prob = 0.4
                else:
                    prob = 0.6


    if(mi == 2):
       
        if(bi == 1):
            if(bj == 1):
                prob = 0.99
            elif(bj == 2):
                prob = 0.01
            else:
                prob = 0

        if(bi == 2):
            if(bj == 1):
                prob = 0.9
            elif(bj == 2):
                prob = 0.1
            else:
                prob = 0.0

        if(bi == 3):
            if(bj == 1):
                prob = 0
            elif(bj == 2):
                prob = 0.9
            else:
                prob = 0.1


    return prob

def ProbS(sj,si):

    if(si == 1):
        if(sj == 1):
            prob = 1
        else:
            prob = 0

    if(si == 2):
        if(sj == 1):
            prob = 0
        else:
            prob = 1
                

    return prob

       
