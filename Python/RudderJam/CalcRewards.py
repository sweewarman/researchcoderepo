
def CalcRewards(v,b,p,r,f,m,s,a):
    """
    Function to calculate rewards
    """

    # Rewards for airspeed states
    if(v == 1):
        R1 = -1
    elif(v == 6):
        R1 = -1
    else:
        R1 = 0

    # Reward for angle of attack/side slip deviation
    if(b == 3):
        R2 = -1
    else:
        R2 = 0

    # Rewards for dynamic pitch states
    if(p in [3,4,5,6]):
        R3 = -1
    else:
        R3 = 0

    # Rewards for dynamic roll states
    if(r in [3,4,5,6]):
        R4 = -1
    else:
        R4 = 0

    # Rewards for flight envelope deviation states
    if(f == 3):
        R5 = -1
    else:
        R5 = 0

    
    # Rewards for P/EA states and actions
    if  (m == 1 and s == 1 and a == 1):
        R6 = 0
    elif(m == 1 and s == 1 and a == 2):
        R6 = -1
    elif(m == 1 and s == 2 and a == 1):
        R6 = -1
    elif(m == 1 and s == 2 and a == 2):
        R6 =  0
    elif(m == 2 and s == 1 and a == 1):
        R6 =  -0.5
    elif(m == 2 and s == 1 and a == 2):
        R6 =  0
    elif(m == 2 and s == 2 and a == 1):
        R6 =  0
    elif(m == 2 and s == 2 and a == 2):
        R6 = -1
    else:
        R6 = 0
    
    
    w1 = 100
    w2 = 100
    w3 = 50
    w4 = 50
    w5 = 50
    w6 = 10

    R = w1*R1 + w2*R2 +  w3*R3 + w4*R4 + w5*R5 + w6*R6

    return R


    
