"""
  Script to generate transition probability matrices for rudder jam test case
  Note: Transition probabilities are generated manually.
        - I decouple the total probablity P(s_{n+1}|s_n,a_n)
          into its individual components. 
        - Use markov assumption to eliminate dependency between features at the same
          time step.
        - Assume conditional independence between certain state features. This makes my life
          easier to manually construct these transitions. Ideally, this would be generated 
          using Monte Carlo simulations which would capture all dependencies.

  State space: [V,B,P,R,F,M,S]
  V \in [v1,v2,...,v6]         - Airspeed 
  B \in [b1,b2,b3]             - AOA and sideslip
  P \in [p1,p2,p3,p4,p5,p6]    - Dynamic pitch control
  R \in [r1,r2,r3,r4,r5,r6]    - Dynamic roll control
  F \in [f1,f2,f3]             - Flight plan deviation
  M \in [P,EA]                 - Current mode
  S \in [P,EA]                 - Mode select switch
"""

import numpy as np
from Probabilities import *
from CalcRewards import *

V = range(1,7)
B = range(1,4)
P = range(1,7)
R = range(1,7)
F = range(1,4)
M = range(1,3)
S = range(1,3)
A = range(1,3)

TotalS  = len(V)*len(B)*len(P)*len(R)*len(F)*len(M)*len(S)

#TotalS  = len(V)*len(B)*len(P)*len(R)*len(F)*len(M)

#TotalS  = len(V)

#TotalS  = 12
TotalSA = TotalS*len(A)


Space   = [(si,mi,fi,ri,pi,bi,vi) for si in S
                                  for mi in M 
                                  for fi in F
                                  for ri in R
                                  for pi in P
                                  for bi in B
                                  for vi in V]



"""
Space = [ (mi,vi) for mi in M
                  for vi in V]
"""

TransMat   = np.zeros((TotalS,TotalS))
TensorNOOP = np.zeros((TotalS,TotalS))
TensorOVRD = np.zeros((TotalS,TotalS))

for Si in range(TotalS):
    for Sj in range(TotalS):
        
        si = Space[Si][0]
        sj = Space[Sj][0]

        mi = Space[Si][1]
        mj = Space[Sj][1]

        fi = Space[Si][2]
        fj = Space[Sj][2]

        ri = Space[Si][3]
        rj = Space[Sj][3]

        pi = Space[Si][4]
        pj = Space[Sj][4]

        bi = Space[Si][5]
        bj = Space[Sj][5]

        vi = Space[Si][6]
        vj = Space[Sj][6]
        

        prob = 1
        prob = prob * ProbV(vj,vi,pi,ri,fi,mi)
        prob = prob * ProbB(bj,bi,vi,mi)
        prob = prob * ProbP(pj,pi,mi)
        prob = prob * ProbR(rj,ri,mi)
        prob = prob * ProbF(fj,fi,mi)
        prob = prob * ProbS(sj,si)

        if(mi != mj):
            prob = 0
        
        #fi=fj=ri=rj=pi=pj=1

        TransMat[Si,Sj] = prob

    # Normalization to ensure probabilites are true
    RowSum = np.sum(TransMat[Si,:])
    TransMat[Si,:] = TransMat[Si,:]/RowSum
        
TensorNOOP = TransMat
TensorOVRD[0:TotalS/2,TotalS/2:]   = TransMat[TotalS/2:,TotalS/2:]        
TensorOVRD[TotalS/2:,0:TotalS/2]   = TransMat[0:TotalS/2,0:TotalS/2]        

np.savetxt('Tensor01.txt',TensorNOOP,fmt='%2.2f')
np.savetxt('Tensor02.txt',TensorOVRD,fmt='%2.2f')

Rewards  = np.zeros((TotalSA,1))

for sa in range(TotalSA):
    if(sa < TotalSA/2):
        a = 1
        i = sa
    else:
        a = 2
        i = sa - TotalSA/2
    
    
    s = Space[i][0]
    m = Space[i][1]
    f = Space[i][2]
    r = Space[i][3]
    p = Space[i][4]
    b = Space[i][5]
    v = Space[i][6]
    
    
    Rewards[sa] = CalcRewards(v,b,p,r,f,m,s,a)
    
    
np.savetxt('Rewards01.txt',Rewards[0:TotalSA/2,0],fmt='%2.2f')
np.savetxt('Rewards02.txt',Rewards[TotalSA/2:,0],fmt='%2.2f')
