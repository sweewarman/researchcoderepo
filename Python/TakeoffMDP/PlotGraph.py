"""
Script to visualize the markov chains
"""

import numpy as np
import os,sys

libpath = os.path.abspath('../Utilities')
sys.path.append(libpath);

from TransitionSystem import TransitionSystem

TensorNOOP = np.loadtxt('Tensor01.txt')
TensorOVRD = np.loadtxt('Tensor02.txt')
nStates    = TensorNOOP.shape[0]

Feat_Label  = ['M','E','T','P','S']
Feat_size   = [2,3,2,8,18] 

MarkovChain = TransitionSystem(5,Feat_size,Feat_Label,2)

MarkovChain.FeatValLabel[0] = ['M1','M2']
MarkovChain.FeatValLabel[1] = ['E1','E2','E3']
MarkovChain.FeatValLabel[2] = ['T1','T2']
MarkovChain.FeatValLabel[3] = ['P'+str(i) for i in range(1,9)]
MarkovChain.FeatValLabel[4] = ['S'+str(i) for i in range(0,18)]

MarkovChain.TransMat[0,:,:] = TensorNOOP

ic = MarkovChain.GetIndex([0,0,1,0,1])

MarkovChain.PrintGraph(0,'TensorNOOP',init=ic)
