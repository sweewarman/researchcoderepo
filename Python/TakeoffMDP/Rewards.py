"""
Script to generate reward matrices for the MDP
"""

import os,sys
libpath = os.path.abspath('../Utilities/')
sys.path.append(libpath)
from Indexing import *
import numpy as np

def RewardsVX(vx):

    """
    Reward function for V,X pair
    """
    
    if (vx in [15,16]):
        return -1
    else:
        return 0

def RewardsPH(ph):

    """
    Reward function for P,H pair
    """
    
    if (int(ph) == 3):
        return -1
    else:
        return 0


def RewardsM(M,A):
    """
    Reward function for Mode, Action
    """

    if (M == 0 and A == 1):
        return -1
    elif (M == 1 and A == 0):
        return -0.1
    elif (M == 1 and A == 1):
        return -0.05
    else:
        return 0



Features = [2,2,8,18]                     #[M,E,T,VX]
Total_S  = np.prod(Features)

RM      = np.zeros((Total_S*2,1))


# Weights for rewards
a1 = 10000.0
a2 = 100.0
a3 = 5.0

# Construct reward Function
for i in range(Total_S*2):

    if(i >= Total_S):
        A = 1
    else:
        A = 0


    [M,T,PH,VX]   = ind2sub(i,Features)

    RM[i,0] += a1 * RewardsVX(VX)
    RM[i,0] += a2 * RewardsPH(PH)
    RM[i,0] += a3 * RewardsM(M,A)
    
# Store reward functions, and transition matrices in a file
np.savetxt('Rewards01.txt',RM[0:Total_S,0])
np.savetxt('Rewards02.txt',RM[Total_S:,0])
