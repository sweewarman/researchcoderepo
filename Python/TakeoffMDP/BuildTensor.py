"""
Script to generate MDP
"""
import shelve
import numpy as np
from Abstraction import *

# Import data from Monte Carlo trials

filename = '../TakeoffMonteCarlo/output_060915.out'
ReqData  = ['YOUT','Trials','GTM_params','UIN','AUX','STOP']
my_shelf = shelve.open(filename)

for key in ReqData:
    globals()[key] = my_shelf[key]

my_shelf.close()
#=====================================================================

# State features
Features = [2,2,8,18]                     #[M,E,T,PH,VX]
Total_S  = np.prod(Features)
nsf      = np.prod(Features[1:])

# State transition matrix
TM1     = np.zeros((Total_S,Total_S))
TM2     = np.zeros((Total_S,Total_S))
TM_NOOP = np.zeros((Total_S,Total_S))
TM_OVRD = np.zeros((Total_S,Total_S))


# Construct Transition matrices from trajectories
for i in range(Trials):
    
    yout = YOUT[i,:,:]
    uin  = UIN[i,:,:]
    aux  = AUX[i,:,:]
    stop = STOP[i,:]
 
    Mat1,Mat2 = getTransMat(yout,uin,aux,stop,GTM_params,Features)
   
    TM1   = TM1 + Mat1
    TM2   = TM2 + Mat2

# Construct transition probabilities for the MDP
TM_P     = np.copy(TM1[0:nsf,0:nsf])
TM_EA    = np.copy(TM1[nsf:Total_S,nsf:Total_S])

TM_NOOP  = TM1
TM_OVRD[0:nsf,nsf:Total_S] = TM_EA
TM_OVRD[nsf:Total_S,0:nsf] = TM_P
TM_OVRD = TM_OVRD + TM2

for i in range(Total_S):
    
    ts1 = np.sum( TM_NOOP[i,:] )
    if( ts1 != 0.0 ):
        TM_NOOP[i,:] = TM_NOOP[i,:]/ts1  

    ts2 = np.sum( TM_OVRD[i,:] )
    if( ts2 != 0.0 ):
        TM_OVRD[i,:] = TM_OVRD[i,:]/ts2  

# Save transition matrices
np.savetxt('Tensor01.txt',TM_NOOP)
np.savetxt('Tensor02.txt',TM_OVRD)

