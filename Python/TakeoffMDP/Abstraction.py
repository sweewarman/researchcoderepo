import numpy as np

import os,sys
libpath1 = os.path.abspath('../TakeoffSim/')
libpath2 = os.path.abspath('../Utilities/')
sys.path.append(libpath1)
sys.path.append(libpath2)

from TK_Abstraction import GetVXstate,GetPQHstate,GetThruststate
from Indexing import *

def getTransMat(yout,uin,aux,stop,GTM_params,features):
    """
    Function to construct the transition matrix based on abstract 
    states
    """

    Total_S = np.prod(features)
    TM1     = np.zeros((Total_S,Total_S))
    TM2     = np.zeros((Total_S,Total_S))


    size_t  = stop[1]

    for i in np.arange(size_t-1):
        
        # Hold last values of variables if simulation stopped prematurely
        if( int(aux[1,i+1]) == -1 ):
            aux[1,i+1] = aux[1,i]
            yout[:,i+1] = yout[:,i]


        si = getAbsState(yout[:,i],uin[:,i],aux[:,i],GTM_params,features)
        sj = getAbsState(yout[:,i+1],uin[:,i+1],aux[:,i+1],GTM_params,features)

        if( int(aux[0,i]) == int(aux[0,i+1]) ):
            if(si != sj):
                TM1[si,sj] += 1.0
        else:
            if(si != sj):
                TM2[si,sj] += 1.0

    return TM1,TM2

def getAbsState(yout,uin,aux,GTM_params,features):
    """
    Function to extract abstract state index based on continuous state
    """
 

    r2d = 180/np.pi

    X = yout[9]
    V = yout[12]
    P = yout[7]*r2d
    Q = yout[4]*r2d
    H = yout[11]
    T = uin[3]
    M = aux[0]
    E = aux[1]

    PartitionP = [0,4,7,10,50]
    PartitionH = [0,2,100000]
    PartitionT = [0,100,300000]

    vx = GetVXstate(V,X,GTM_params)
    ph = GetPQHstate(P,Q,H,PartitionP,PartitionH)
    t  = GetThruststate(T,PartitionT)

    current_state = [M,t,ph,vx]
    
    s  = sub2ind(current_state,features)

    return s

