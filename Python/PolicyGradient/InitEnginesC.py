"""
 Script to initialize object to perform Monte carlo simulation
 Must be executed on each engine.
"""
import os,sys

libpath1 = os.path.abspath('../TakeoffSim/')
libpath2 = os.path.abspath('../../C/ODE/Aircraft')
libpath3 = os.path.abspath('../Utilities')
sys.path.append(libpath1)
sys.path.append(libpath2)
sys.path.append(libpath3)

from AircraftC2PyODE import py_ode
from TK_Aircraft import *
import random as rd
import pylab as plt
from Sampling import SampleArb

model =  '../AircraftModels/GTM.dat'
perf  =  '../AircraftModels/GTM_performance.dat'
#====================================================================

# Aircraft state
# u,v,w,p,q,r,roll,pitch,yaw,Xe,Ye,h,U,alpha,beta
x0     = [0.1,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0]

# Simulation time span and timestep
tspan  = 20
dt     = 0.1
size_t = int(tspan/dt)

GTM          = TK_Aircraft(model,perf)

GTM.Params[12] = 800                    # Runway length
GTM.Params[13] = 0                      # Wind
GTM.hTS        = 10/3.2 - 1             # Tail strike height

# Compute takeoff performance for selected aircraft
GTM.Compute_Performance(GTM.Params)     

ACParams = np.array([GTM.Sref,GTM.M,GTM.c,GTM.b, \
                      GTM.c1, GTM.c2, GTM.c3,  \
                      GTM.c4, GTM.c5, GTM.c6,  \
                      GTM.c7, GTM.c8, GTM.c9,  ])

nACParams = ACParams.shape[0]

AeroParams = GTM.t[0,:]
nAeroParams = AeroParams.shape[0]

GearParams = np.array([ GTM.lxL, GTM.lyL, GTM.lzL, \
                         GTM.lxR, GTM.lyR, GTM.lzR, \
                         GTM.lxN, GTM.lyN, GTM.lzN, \
                         GTM.k_L, GTM.k_R, GTM.k_N, \
                         GTM.c_L, GTM.c_R, GTM.c_N, \
                         GTM.mu ])

nGearParams = GearParams.shape[0]

ThrustParams  = [0.287,0.2362,-0.9,0.29,\
                 -0.08,0.6,-0.99,0.4]

ControlParams = np.array(ThrustParams)


nCpm = ControlParams.shape[0]

RandomParams   = np.array([0,0])
nRpm = RandomParams.shape[0]

PARAMS = np.zeros((nACParams + \
                   nAeroParams + \
                   nGearParams + \
                   nCpm + nRpm))

PARAMS[0:13]                = ACParams[:]
PARAMS[13:13+45]            = AeroParams[:]
PARAMS[13+45:13+45+16]      = GearParams[:]

PARAMS[74:74+nCpm]                = ControlParams[:]
PARAMS[74+nCpm:74+nCpm+nRpm]      = RandomParams[:]

SimParams = np.array([15,tspan,dt])
Init      = np.array(x0)


Trials   = 100


RD      = []
IC      = []
YOUT    = np.zeros((Trials,size_t,15))
STOP    = np.zeros((Trials))


# Select random variables before simulations
for i in range(Trials):

    x_ic = np.random.uniform(0,1)*800
    v_ic = np.random.uniform(0,1)*70
    IC.append((v_ic,x_ic))

    AEO = SampleArb([1.0,0.0])

    if(AEO == 1):
        fail_time = np.random.uniform(0,1)*0;
    else:
        fail_time = tspan+100
        
    RD.append([fail_time,AEO])

Init = np.array(x0)

yout = np.zeros((201,16))

def MCSim(arg):
    """
    Wrapper function that can be called by client to start 
    Monte carlo trials
    """

    for i in range(Trials):

        init_condition  = IC[i]
        RandomParams[:] = RD[i]

        Init[0]       = init_condition[0]
        Init[9]       = init_condition[1]
        Init[12]      = init_condition[0]
        
        PARAMS[74:74+nCpm] = ThrustParams
        PARAMS[74+nCpm:74+nCpm+nRpm] = RandomParams[:]

   
        status = py_ode(SimParams,PARAMS,Init,yout)
        
        if(status < 0):
            print "status failure received"
            break
   
        if(status >= 0):
            size_t = status;
            STOP[i] = size_t
     
        # Store inputs and output for analysis
        YOUT[i,:size_t,:]  = yout[:size_t,1:]
       
        #plt.plot(yout[:size_t,1],yout[:size_t,10])

#MCSim(0)
#plt.show()
