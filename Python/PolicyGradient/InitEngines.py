"""
 Script to initialize object to perform Monte carlo simulation
 Must be executed on each engine.
"""
import os,sys

libpath1 = os.path.abspath('../TakeoffSim/')
libpath2 = os.path.abspath('../GenSim/')
sys.path.append(libpath1)
sys.path.append(libpath2)

from GenSim import *
from TK_Aircraft import *
import random as rd
import shelve


model =  '../AircraftModels/GTM.dat'
perf  =  '../AircraftModels/GTM_performance.dat'
#====================================================================

# Aircraft state
# u,v,w,p,q,r,roll,pitch,yaw,Xe,Ye,h,U,alpha,beta
x0     = [0.1,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0, \
          0.0,0.0,0.0]

# Simulation time span and timestep
tspan  = 20
dt     = 0.1


GTM          = TK_Aircraft(model,perf)
SIM          = GenSim(dt,tspan,x0)
SIM.Aircraft = GTM

GTM.Params[12] = 800                    # Runway length
GTM.Params[13] = 0                      # Wind
GTM.hTS        = 10/3.2 - 1             # Tail strike height

# Compute takeoff performance for selected aircraft
GTM.Compute_Performance(GTM.Params)     

# Parameters for thrust controller
SIM.ThrustParams = [0.1,0.2,-1,0.1]

Trials   = 100

SIM.IC   = []
SIM.Tmax = 250000
SIM.Vmax = 70
SIM.Xmax = 1000 

# Array to record control inputs
SIM.Control  = np.zeros((4,SIM.size_t))

YOUT    = np.zeros((Trials,SIM.size_x,SIM.size_t))
UIN     = np.zeros((Trials,4,SIM.size_t))
STOP    = np.zeros((Trials,2))


# Select random variables before simulations
for i in range(Trials):

    x_ic = np.random.uniform(0,1)*800
    v_ic = np.random.uniform(0,1)*70
    SIM.IC.append((v_ic,x_ic))


def MCSim(init_condition):
    """
    Wrapper function that can be called by client to start 
    Monte carlo trials
    """

    for i in range(Trials):
        
        # Reinitialize parameters that need to be reset for each trial
        SIM.stop      = False
        SIM.stop_time = [SIM.Time[-1],SIM.size_t]
        
        SIM.yout[:,:]    = 0
        SIM.Control[:,:] = 0
        
        init_condition = SIM.IC[i]

        x0[9]         = init_condition[1]
        x0[0]         = init_condition[0]
        x0[12]        = init_condition[0]
        
        SIM.yout[:,0] = x0
        
        # Run simulation
        SIM.RunSim2()
        
        # Store inputs and output for analysis
        YOUT[i,:,:]  = SIM.yout
        UIN[i,:,:]   = SIM.Control
        STOP[i,:]    = SIM.stop_time
        
        
def Update(sim,t,x0):
    """
    Update function required to compute inputs
    for monte carlo simulations
    """

    Rlength = sim.Aircraft.Params[12]
    V2      = sim.Aircraft.Params[15]

    if(x0[9] >= Rlength+100 or x0[12] >= V2+10):
        sim.stop = True
    
   
    params = sim.ThrustParams

    nParams = len(params)

    Xmax = sim.Xmax
    Vmax = sim.Vmax

    X = x0[9]/Xmax
    V = x0[12]/Vmax

    features = [1,V,X,V**3]
  
    theta = np.zeros((1,nParams))
    feat  = np.zeros((nParams,1))
    
    theta[0,:] = params
    feat[:,0]  = features

    Z = np.dot(theta,feat)
    G = Sigmoid(Z[0])
    
    if(G >= 0.5):
        T = sim.Tmax
    else:
        T = 0

    if(x0[12] <= 0.3 and T==0 ):
        sim.stop = True

    U  = [0,0,0,T]
    LG = 1
    
    return U,LG


def Sigmoid(z):
    """
    Sigmoid function 
    """
    g = 1.0/(1+np.exp(-z))

    return g


# Set simulation update and output function
SIM.SetUpdateFunc(Update)
SIM.SetOutputFunc(GTM.Dynamics2)

