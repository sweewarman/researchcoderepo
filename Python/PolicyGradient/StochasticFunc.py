"""
Script to test a stochastic function
"""

import numpy as np
import pylab as plt

a0 = 0.248
a1 = 0.246
a2 = -0.89
a3 = 0.341

def func(x,stoch,a0,a1,a2,a3):
    
    if(stoch is False):
        return -(a0 + a1*x + a3*x**3)/a2
    else:
        return 2 + 1*x + 0.5*x**2 + np.random.normal(0,1)


fun1 = lambda X:func(X,False,0.1,0.1,-1,0.2)
fun2 = lambda X:func(X,False,a0,a1,a2,a3)

X  = np.linspace(0,1,100)
Y1 = np.array(map(fun1,X))
Y2 = np.array(map(fun2,X))


plt.figure()
plt.plot(X*70,Y1*1000,'b')
plt.plot(X*70,Y2*1000,'r')
#plt.scatter(X,Y2,color='r')
plt.grid('on')
plt.show()

