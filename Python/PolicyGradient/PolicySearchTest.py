"""
Script that does the policy gradient based search
"""

import numpy as np
import pylab as plt

Trials = 1000


random_fix = [np.random.normal(0,1) for i in range(Trials)]


def fun(params,x,r):
    
    a0 = params[0]
    a1 = params[1]

    true_func = 10.0 + 0.5*x**2

    test_func = a0*1.0 + a1*x**2 + random_fix[r]

    return np.fabs(true_func - test_func)**2

def Utility(params,x):

    U = []

    for i in range(Trials):

         y = fun(params,x,i)
         U.append(y)

    return sum(U)/Trials



Theta  = [0,0]
nParam = len(Theta)
alpha = -0.001


param_old    = np.array([[0] for i in range(nParam)])
param_error  = 0.1


Test_X      = np.linspace(0,5,10)

for test_x in Test_X:
    param_error = 0.1
    count_param    = 0
    while(param_error >= 1e-3):
        
        count_param += 1
        
        grad_error = 0.1 
        Xold       = np.array([[0] for i in range(nParam)])
        DelTheta   = []
        DelJ       = []
        
        count_gradient = 0
        
        while(grad_error >= 1e-4):

            count_gradient += 1

            delTheta     = [ np.random.uniform(0.00001,0.001) for i in range(nParam) ]
            ThetaPlusDel = [ sum(i) for i in zip(Theta,delTheta)]
            
            J1   = Utility(Theta,test_x)
            J2   = Utility(ThetaPlusDel,test_x)
            
            delJ = [J2 - J1]
            
            DelTheta.append(delTheta)
            
            DelJ.append(delJ)
            
            A = np.array(DelTheta)
            B = np.array(DelJ)
            
            X = np.linalg.lstsq(A,B)[0]
            
            grad_error = np.linalg.norm(Xold-X)
            Xold = X
            
        
        Gradient = X
        param   = np.array([[i] for i in Theta]) 

        # Graident ascent rule
        param   = param + alpha*Gradient
        
        Theta    = [i[0] for i in param]
        
        param_error = np.linalg.norm(param_old - param)
        param_old   = param
        
        fun_value = fun(param,test_x,0)
        
        #print "Param iteration = %d,error =%f,fun=%f,Gradient run = %d,Gradient=[%f,%f]"\
            #%(count_param,param_error,fun_value,count_gradient,Gradient[0],Gradient[1])
        

fun1 = lambda x:10.0 + 0.5*x**2
fun2 = lambda x:param[0]*1.0 + param[1]*x**2 
fun3 = lambda x:10.0 + 0.5*x**2 + np.random.normal(0,1)

X  = np.linspace(0,5,100)
Y1 = map(fun1,X)
Y2 = map(fun2,X)
Y3 = map(fun3,X)

plt.plot(X,Y1,'b')
plt.plot(X,Y2,color='r')
plt.scatter(X,Y3,color='g')
plt.grid('on')
plt.show()
