"""
Script to perform policy search
"""

from IPython.parallel import Client
import numpy as np
import pylab as plt

#from InitEngines import *


rc = Client()
rc[:].block = True
rc[:].run('InitEnginesC.py')

Interval = 10
nEngines = 4

V2      = rc[0]['GTM.Params[15]']
RLength = rc[0]['GTM.Params[12]']

#V2      = SIM.Aircraft.Params[15]
#RLength = SIM.Aircraft.Params[12]

gamma   = 0.999

def UtilityTest(params1):
    """
    Function to compute expected utility
    """

    #import pdb; pdb.set_trace()

    rc[:4].push(dict(POLICY_PARAMS=params1,X0=0))
    
    status1 = rc[:4].execute('ThrustParams = POLICY_PARAMS')
    status1 = rc[:4].execute('MCSim(0)')
    
    Trials = rc[0]['Trials']

    #ThrustParams = params1
    #MCSim(x0)


    #Vplt = np.linspace(0,1,100)
    #Xfun = lambda V: -(params1[0] + params1[1]*V + params1[3]*V**3)/params1[2]
    #Xplt = np.array(map(Xfun,Vplt))

    #Vplt = Vplt*70
    #Xplt = Xplt*1000

    #plt.figure(1)
    #plt.plot(Vplt,Xplt,'r',linewidth=2.5)


    
    R_list = []
    for i in range(4):
        
        yout     =  rc[i]['YOUT']
        stoptime =  rc[i]['STOP']
        
        #Time     =  SIM.Time
        #yout     =  YOUT
        #controls =  UIN
        #stoptime =  STOP
        
        for j in range(Trials):
            
            n = 1
            
            R = 0

            tmax = int(stoptime[j])

            for k in range(tmax):
                
                if(k%Interval == 0):
                    n += 1
                
                r = Rewards(yout[j,k,:])

                R += r*gamma**n

                if(r<0):
                    break
            
            R_list.append(R)
            
            #plt.plot(yout[j,:tmax,12],yout[j,:tmax,9])

    ExpReward1 = sum(R_list)/(Trials*4)
   


    #plt.xlim([0,70])
    #plt.ylim([0,800])
    #plt.show()

    return ExpReward1



def Utility(params1,params2):
    """
    Function to compute expected utility
    """

    #import pdb; pdb.set_trace()

    rc[:4].push(dict(PARAMS=params1,X0=0))
    rc[4:8].push(dict(PARAMS=params2,X0=0))
    status1 = rc[:4].execute('SIM.ThrustParams = PARAMS')
    status1 = rc[4:8].execute('SIM.ThrustParams = PARAMS')
    status1 = rc[:4].execute('MCSim(X0)')
    status2 = rc[4:8].execute('MCSim(X0)')
    status2 = rc[4:8].execute('MCSim(X0)')
    
    Trials = rc[0]['Trials']

    #SIM.ThrustParams = params
    #MCSim(x0)


    #Vplt = np.linspace(0,1,100)
    #Xfun = lambda V: -(params[0] + params[1]*V + params[3]*V**3)/params[2]
    #Xplt = np.array(map(Xfun,Vplt))

    #Vplt = Vplt*70
    #Xplt = Xplt*1000

    #plt.figure(1)
    #plt.plot(Vplt,Xplt,'r',linewidth=2.5)


    
    R_list = []
    for i in range(4):
        Time     =  rc[i]['SIM.Time']
        yout     =  rc[i]['YOUT']
        controls =  rc[i]['UIN']
        stoptime =  rc[i]['STOP']

        #Time     =  SIM.Time
        #yout     =  YOUT
        #controls =  UIN
        #stoptime =  STOP
        
        for j in range(Trials):
            
            n = 1
            
            R = 0

            tmax = int(stoptime[j,1])

            for k in range(tmax):
                
                if(k%Interval == 0):
                    n += 1
                
                R += Rewards(yout[j,:,k])*gamma**n
            
            R_list.append(R)

            #plt.scatter(yout[j,12,0],yout[j,9,0])

    ExpReward1 = sum(R_list)/(Trials*4)

    
    R_list = []
    for i in range(4,8):
        Time     =  rc[i]['SIM.Time']
        yout     =  rc[i]['YOUT']
        controls =  rc[i]['UIN']
        stoptime =  rc[i]['STOP']

        #Time     =  SIM.Time
        #yout     =  YOUT
        #controls =  UIN
        #stoptime =  STOP
        
        for j in range(Trials):
            
            n = 1
            
            R = 0

            tmax = int(stoptime[j,1])

            for k in range(tmax):
                
                if(k%Interval == 0):
                    n += 1
                
                R += Rewards(yout[j,:,k])*gamma**n
            
            R_list.append(R)

            #plt.scatter(yout[j,12,0],yout[j,9,0])

    ExpReward2 = sum(R_list)/(Trials*4)
    
    delJ = ExpReward2 - ExpReward1
   
    
    #plt.xlim([0,70])
    #plt.ylim([0,1000])
    #plt.show()

    return delJ



def Rewards(state):

    x = state[9]
    v = state[12]

    if(v < V2 and x >= RLength):
        return -2
    elif(v >= V2):
        return 1
    else:
        return 0


Theta  = [0.1,0.1,-1.0,0.2,\
          -0.1,0.1,-1.0,0.2]

nParam = len(Theta)
alpha  = 0.001


ThetaDel     = []
ThetaPlusDel = []

eps    = 0.05


for i in range(nParam):
    TD = [0 for j in range(nParam)]
    TD[i] = eps
    
    ThetaDel.append(TD)


X0 = [ [0.0] for j in range(nParam)]

#import pdb; pdb.set_trace()

v0 = 5
x0 = 5

param_old    = np.array([[0] for i in range(nParam)])
param_error  = 0.1


param_error = 0.1
count_param    = 0
while(param_error >= 1e-2):
        
    count_param += 1
        
    grad_error = 0.1 
    Xold       = np.array([[0] for i in range(nParam)])
    DelTheta   = []
    DelJ       = []
        
    count_gradient = 0

    #Vinit        = 0.05
    #Xinit        = Theta[0]+ Theta[1]*Vinit + Theta[2]*Vinit**3

    #Vinitn = Vinit*70
    #Xinitn = Xinit*1000

    #test_x = [0,0]

    J1   = UtilityTest(Theta)

    X    = np.array(X0)
    for j in range(nParam):
        ThetaPlusDel = [sum(k) for k in zip(Theta,ThetaDel[j])]
        Jtd = UtilityTest(ThetaPlusDel)
        X[j,0] = (Jtd - J1)/eps

    
    Gradient = X
    param   = np.array([[i] for i in Theta]) 
    
    # Graident ascent rule
    param   = param + alpha*Gradient
    
    Theta    = [i[0] for i in param]
    
    param_error = np.linalg.norm(param_old - param)
    param_old   = param
    
    
    print "Param iteration = %d,error =%f,\n\tparam=[%f,%f,%f,%f]"\
        %(count_param,param_error,param[0],param[1],param[2],param[3])



