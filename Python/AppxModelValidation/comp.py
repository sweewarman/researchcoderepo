"""
 This script compares responses of the nonlinear model (GTM) with the
 approximate model used in the thesis.

 The simplified model approximates the nonlinear model very closely.
 Pitch response is a different - I attribute this to the high 
 uncertainty in the aerodynamic coefficient that multiplies alpha*q.
 If this aerodynamic coefficient is reasonably small, then I have a 
 perfect approximation.

"""

import os,sys

libpath1 = os.path.abspath('../TakeoffSim/')
libpath2 = os.path.abspath('../GenSim/')
sys.path.append(libpath1)
sys.path.append(libpath2)

from GenSim import *
from TK_Aircraft import *
from pylab import *

model =  '../AircraftModels/GTM.dat'
perf =   '../AircraftModels/GTM_performance.dat'

d2r    = np.pi/180.0
r2d    = 180.0/np.pi


# Aircraft state
# u,v,w,p,q,r,roll,pitch,yaw,Xe,Ye,h,U,alpha,beta
x0     = [30,0,0,0,0,0,0,0.0,0,0,0,0,30,0,0]

# Simulation time span and timestep
tspan  = 15
dt     = 0.1

GTM    = TK_Aircraft(model,perf)
SIM1   = GenSim(dt,tspan,x0)
Tmax   = 107000*2

GTM.Params[19] = Tmax

pitch_ref = 8 
yaw_ref   = 0
Kp1       = -0.5
Kd1       =  0.1   
Kp2       = -0.5
Kd2       =  0.1 



def Control1(sim,t,x0):

    if(x0[12] > 40):
        Uele = Kp1*(pitch_ref - x0[7]*r2d) + Kd1*x0[4]*r2d
    else:
        Uele = 5*d2r

    Urud = Kp2*(yaw_ref - x0[8]*r2d) + Kd2*x0[5]*r2d

    U,LG = [Uele,0,Urud,107000*2],1
    return U,LG


SIM1.SetUpdateFunc(Control1)
SIM1.SetOutputFunc(GTM.SimDyn)
SIM1.RunSim()

#=====================================================================
# Response from approximate model

AB     = AnalyticalModel(GTM.Params)

xapp0  = [0,30,0,0,0*d2r,0,0,0]
SIM2   = GenSim(dt,tspan,xapp0)

def App_Dyn(x,t,args):

    ue    = args[0]
    ur    = args[1]

    Cmq   = -44.34
    Cmde  = -1.785
    Cnr   = -0.405
    Cndr  = -0.129
    rho   = 1.224
    Sref  = 122.4
    cbar  = 4.194
    b     = 34.1
    Ixx   = 2262000.0
    Iyy   = 3172000.0
    Izz   = 3337000.0
    Ixz   = 1500.0
    dyn   = 0.5*rho*x[1]**2
    fac1  = dyn*Sref*cbar/Iyy
    CL    = 0.4646
    c9    = Ixx/(Ixx*Izz - Ixz**2)
    fac2  = dyn*Sref*b*c9
    

    L     = dyn*Sref*cbar*CL
    W     = 445116.0
    N     = W - L
    mu_y  = 0.1
    Fy    = mu_y*N
    lyR   = 3.795
    My    = Fy*lyR


    dx    = [0,0,0,0,0,0,0,0]
    
    dx[0] = x[1]
    dx[1] = AB[0] - AB[1]*x[1]**2

    dx[2] = x[3]
    dx[3] = (Cmq*x[3] + Cmde*ue)*fac1

    dx[4] = x[5]
    dx[5] = (Cndr*ur)*fac2
    dx[6] = x[1]*sin(x[4]) + x[7]*cos(x[4])
    dx[7] = -x[5]*x[1]
        

    return dx

def App_Inputs(sim,t,xs):

    if(xs[1] > 40.0):
        Uele = Kp1*(pitch_ref - xs[2]*r2d) + Kd1*xs[3]*r2d
    else:
        Uele = 0;

    Urud = Kp2*(yaw_ref - xs[4]*r2d) + Kd2*xs[5]*r2d

    return [Uele,Urud]

SIM2.SetUpdateFunc(App_Inputs)
SIM2.SetOutputFunc(App_Dyn)
SIM2.RunSim()

# Plot simulation outputs
figure(1)
subplot(311)
plot(SIM1.Time,SIM1.yout[11,0:SIM1.size_t]);
ylabel('H')
grid(b=1)
subplot(312)
plot(SIM1.Time,SIM1.yout[12,0:SIM1.size_t],'b',label='NL model');
plot(SIM2.Time,SIM2.yout[1,:],'r',label='Appx model');
legend(loc=4)
ylabel('U')
grid(b=1)
subplot(313)
plot(SIM1.Time,SIM1.yout[7,0:SIM1.size_t]*180/np.pi,label='NL model');
plot(SIM2.Time,SIM2.yout[2,:]*r2d,'r',label='Appx model');
ylabel('Pitch')
grid(b=1)
legend(loc=4)

figure(2)
subplot(311)
plot(SIM1.Time,SIM1.yout[10,0:SIM1.size_t],label='NL model');
plot(SIM2.Time,SIM2.yout[6,:],'r',label='Appx model');
ylabel('Y')
legend(loc=4)
grid(b=1)
subplot(312)
plot(SIM1.Time,SIM1.yout[8,0:SIM1.size_t],label='NL model');
plot(SIM2.Time,SIM2.yout[4,:],'r',label='Appx model');
ylabel('yaw')
legend(loc=4)
grid(b=1)
subplot(313)
plot(SIM1.Time,SIM1.yout[5,0:SIM1.size_t],label='NL model');
plot(SIM2.Time,SIM2.yout[5,:],'r',label='Appx model');
ylabel('rate')
legend(loc=4)
grid(b=1)

