"""
    Author: Swee Warman B.
    University of Michigan
    04-27-14
    
    Description:
    Script to create a dot graph for the given policy
    
"""

import numpy as np

def Gen_Traces(S0,States,A,Total_states,V,policy,MC):
    fp = open('Traces.gv','w');

    fp.write('digraph{\n');

    X_curr = np.zeros((1,Total_states));

    # starting node
    X_curr[0,S0] = 1;

    # List of nodes to be visited
    Visit   = [];

    # List of noded already visited
    Visited = []

    Visit.append(S0);

    

    while(Visit!=[]):
    
        #print 'Visit list=',Visit;

        X_next   = np.dot(X_curr,MC);
    
        # List of children of the parent node 
        Children = [i for i in range(Total_states) if (X_next[0,i]!=0)];
    
        #print 'Children  =', Children;
    
        while(Children!=[]):
            n1 = Visit[0];
            n2 = Children[0];
        
            # write n1->n2 
            fp.write(str(n1)+'->'+str(n2));
        
            # write transition details

            if(policy[n1] <= 0): 
                fp.write('[label="'+str(A[policy[n1]])+'\\n"]\n');
            else:
                fp.write('[color=red,label="'+str(A[policy[n1]])+'\\n"]\n');
        
            # write n1 node description
            fp.write(str(n1)+' [label="'+str(States[n1])+'\\n'+'%.2f' %(V[n1])+'"];\n');
        
            # write n2 node description
            fp.write(str(n2)+' [label="'+str(States[n2])+'\\n'+'%.2f' %(V[n2])+'"];\n');
        
            # Remove the child and place it on the to visit list
            Children.pop(0);
        
            Visit.append(n2);
        
        # Remove parent from vist list, place it on visited list
        Visited.append(Visit.pop(0));
    
        #print 'Visited   =', Visited
        
        # Remove the next parent if already visited
        while( Visit != [] and Visit[0] in Visited ):
            Visit.pop(0);  
            #print 'Visit list=',Visit;
            
            # Set the 'next current state'
        if(Visit!=[]):
            X_curr = np.zeros((1,Total_states));
            X_curr[0,Visit[0]] = 1;
        

    fp.write('}');        
    
    fp.close();
