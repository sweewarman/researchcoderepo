"""
    Author: Swee Warman B.
    University of Michigan, Ann Arbor
    04-26-2014

    Description
    ======================================================
    Reward functions for the STEM MDP formulation
"""

def R1(S,T):
    """
    Reward/Costs on S and T (velocity-position states) 
    and thrust settings
    """

    #Safe sets with Tidle
    Safe_Tidle = set(['SO','S1','S2','S3','S4', 
                      'S8','S9','S10','S11','S17']);
    


    if(T == 'Tidle'):
        if(S in Safe_Tidle):
            r1 = 200.0;
        else:
            if(S in set(['S5','S6','S7'])):
                r1 = -1000.0;
            elif(S in set(['S12','S13'])):
                r1 = -2500.0;
            elif(S == 'S15'):
                r1 = -5000.0;
            else:
                r1 = -20000.0;
    else:
        if(S in set(['S0','S1','S2','S3','S4'])):
            r1 = 500.0;
        elif(S in set(['S5','S6','S7'])):
            r1 = 1000.0;
        elif(S in set(['S8','S9','S10','S11'])):
            r1 = -5000.0;
        elif(S in set(['S12','S13'])):
            r1 = -9000.0;
        elif(S == 'S15'):
            r1 = -20000.0;
        elif(S == 'S17'):
            r1 = -2000;
        else:
            r1 = -1000.0;

    return r1;


def R2(M,A):
    """
    Rewards for the override actions to prevent
    unnessary overrides. Also, penalize autopilot 
    in command states to revert control back to pilot
    """

    C = (M,A);

    if(C == ('P','NOOP')):
        r2 = 0;
    elif(C == ('P','OVRD')):
        r2 = -100.0;
    elif(C == ('AP','NOOP')):
        r2 = -50.0;
    else:
        r2 = 0;

    return r2;

def R3(G):
    """
    Rewards/Costs for inappropriate pitch attitude
    during takeoff. 
    """

    if(G=='G3'):
        r3 = -1.0;
    else:
        r3 = 0;

    return r3;
    
def R(Q,T,E,M,A):
    """
    Reward function to distribute rewards and costs for 
    the MDP states  Q,T,E,M,A

    Rewards are positive and costs are negative
    """
    
    S  = Q[0];
    G  = Q[1];

    # Weights on different reward characteristics
    a1 = 1;
    a2 = 5;
    a3 = 5000;
    
    # Rewards
    r  = a1 * R1(S,T) + a2 * R2(M,A) + a3*R3(G);

    return r;
