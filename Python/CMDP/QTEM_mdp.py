"""
    AUTHOR: SWEE WARMAN B.
    UNIVERSITY OF MICHIGAN, ANN ARBOR
    04-26-2014

    DESCRIPTION
    ======================================================
    THE GOAL OF THIS PROGRAM IS TO SOLVE THE FSAM MDP USING
    A LINEAR PROGRAMMING APPROACH AND VALIDATE THE APPROACH
    USING THE SOLUTIONS OBTAINED FROM VALUE ITERATION
    
    STATES USED ARE Q,T,E,M
    
    Q - (S,G)
    S - (V,X)
    G - (P,H)
    V - VELOCITY, X - POSITION, P - DYNAMIC PITCH,
    H - ALTITUDE, T - THRUST,   E - ENGINES
    M - MODE
"""

import numpy as np
from cvxopt import matrix, solvers

# Importing the Reward  modules
from Rewards import *
          

# STATES FOR THE MDP
S = ['S' + str(i) for i in range(1,18)]
G = ['G' + str(j) for j in range(8)];
Q = [(s,g) for s in S for g in G];
T = ['Tidle','Tmax'];
E = ['Efail','Egood'];
M = ['P','AP'];

# Actions for the MDP
A = ['NOOP','OVRD'];

States =  [(q,t,e,m) for q in Q 
                     for t in T
                     for e in E
                     for m in M];

Total_states = len(States);

SA = [(q,t,e,m,a) for a in A
                  for q in Q 
                  for t in T
                  for e in E
                  for m in M];

# Value function to be maximized (Primal problem)
C = np.array([R(q,t,e,m,a) for a in A
                           for q in Q 
                           for t in T
                           for e in E
                           for m in M]);

# Loading the probability tensors for the 2 actions
P1 = np.loadtxt('Tensor1.txt');
P2 = np.loadtxt('Tensor2.txt');

# Discount facotr
gamma = 0.7;

I      = np.hstack([np.eye(Total_states),np.eye(Total_states)]);
P      = np.hstack([P1.T,P2.T]);

Aeq    = I - gamma*P;

Beq    = np.zeros((Total_states,1));
Beq[6] = 1;

Aineq  = np.eye(Total_states*2);
Bineq  = np.zeros((Total_states*2,1));

# Solving the linear program
#  min Cx
#  s.t Ax <= B


sol    = solvers.lp(matrix(-C),matrix(-Aineq),matrix(Bineq),
                    matrix(Aeq),matrix(Beq)); 

# Value of the states reachable from Beq distribution is the 
# solution to the dual problem
V      = sol['y'];
V      = [V[i] for i in range(1088)];

# Get all the occupational measures (solution to the primal problem)
x      = np.array(sol['x']);

# Compute the policy from the occupational measures
x1     = x[:1088].copy();
x2     = x[1088:].copy();

A1     = [x1[i]/(x1[i] + x2[i]) for i in range(1088)];
A2     = [x2[i]/(x1[i] + x2[i]) for i in range(1088)];

A1_i   = [i for i in range(1088) if abs(A1[i]-1) < 0.3];

policy = np.array([1 for i in range(1088)]);

# Final policy
policy[A1_i]  = 0;

# Markov matrix for the computed policy
MC  = np.zeros((1088,1088));
P3d = np.array([P1,P2]);

for i in range(1088):
    MC[i,:] = P3d[policy[i],i,:];


np.savetxt('MC.txt',MC);
np.savetxt('Output.txt',np.hstack([V,policy]));




    
    
