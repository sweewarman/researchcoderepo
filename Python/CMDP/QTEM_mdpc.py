"""
    AUTHOR: SWEE WARMAN B.
    UNIVERSITY OF MICHIGAN, ANN ARBOR
    04-26-2014

    DESCRIPTION
    ======================================================
    THE GOAL OF THIS PROGRAM IS TO SOLVE THE FSAM MDP USING
    A LINEAR PROGRAMMING APPROACH.
    
    STATES USED ARE Q,T,E,M
    
    Q - (S,G)
    S - (V,X)
    G - (P,H)
    V - VELOCITY, X - POSITION, P - DYNAMIC PITCH,
    H - ALTITUDE, T - THRUST,   E - ENGINES
    M - MODE
"""

# Importing the numpy and cvx modules
import numpy as np
from cvxopt import matrix, solvers

# Importing the Reward  modules
from Rewards import *
          

# FEATURES of the MDP
S = ['S' + str(i) for i in range(1,18)]  # Velocity-position
G = ['G' + str(j) for j in range(8)];    # Dynamic pitch - altitude
Q = [(s,g) for s in S for g in G];       # ((V,X),(P,H))
T = ['Tidle','Tmax'];                    # Thrust
E = ['Efail','Egood'];                   # Engines
M = ['P','AP'];                          # Control modes

# ACTIONS of the MDP
A = ['NOOP','OVRD'];

# STATES (Cartesion product of all the features)
States =  [(q,t,e,m) for q in Q for t in T for e in E for m in M];

Total_states = len(States);

# Cartesian product of all the states and actions
SA = [(q,t,e,m,a) for a in A for q in Q for t in T for e in E for m in M];

# Value function to be maximized (Primal problem)
C = np.array([R(q,t,e,m,a) for a in A for q in Q for t in T for e in E for m in M]);

# Loading the probability tensors for the 2 actions
P1 = np.loadtxt('Tensor1.txt');
P2 = np.loadtxt('Tensor2.txt');

# Discount factor
gamma = 0.7;

# 1st equality constraint
I       = np.hstack([np.eye(Total_states),np.eye(Total_states)]);
P       = np.hstack([P1.T,P2.T]);

Aeq1    = I - gamma*P;
Beq1    = np.zeros((Total_states,1));

Beq1[6] = 1;

# 2nd equality constraint (for tail strike)
Aeq2         = np.zeros((2,Total_states*2));
Beq2         = np.zeros([2,1]);

Aeq2[0,414]  = 1;
Aeq2[1,1502] = 1;

# Expressing equality constraints 1 and 2 as a single constraint
Aeq     = np.vstack([Aeq1,Aeq2]);
Beq     = np.vstack([Beq1,Beq2]);

# Inequality constraints on the occupation measure
Aineq  = np.eye(Total_states*2);
Bineq  = np.zeros((Total_states*2,1));


# Solving the linear program
#        min Cx
#  s.t Ax <= B, Gx = H

sol    = solvers.lp(matrix(-C),matrix(-Aineq),matrix(-Bineq),
                    matrix(Aeq),matrix(Beq)); 

# Value of the states reachable from Beq distribution is the 
# solution to the dual problem
V      = sol['y'];
V      = [V[i] for i in range(1088)];

# Get all the occupational measures (solution to the primal problem)
x      = np.array(sol['x']);

# Compute the policy from the occupational measures
x1     = x[:1088].copy();
x2     = x[1088:].copy();

A1     = [x1[i]/(x1[i] + x2[i]) for i in range(1088)];
A2     = [x2[i]/(x1[i] + x2[i]) for i in range(1088)];

A1_i   = [i for i in range(1088) if abs(A1[i]-1) < 0.3];

policy = np.array([1 for i in range(1088)]);

policy[A1_i]  = 0;

# Markov matrix for the computed policy
MC  = np.zeros((1088,1088));
P3d = np.array([P1,P2]);

for i in range(1088):
    MC[i,:] = P3d[policy[i],i,:];

# Saving data
np.savetxt('MC.txt',MC);
np.savetxt('Output.txt',np.hstack([V,policy]));
