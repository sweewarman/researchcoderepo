"""
 Envelope Aware controller
"""

import numpy as np
from TK_Abstraction import GetVXstate

r2d       = 180.0/np.pi
d2r       = np.pi/180

class EAModel():
    
    def __init__(self):
        return

    def Init(self,params):
        
        self.vr     = params[0]
        self.kpe    = params[1]
        self.kde    = params[2]
        self.n      = params[3]
        self.pref   = params[4]
        self.Tmax   = params[5]

        self.kpr    = params[6]
        self.kdr    = params[7]
        self.kpy    = params[8]

        # Tail strike protection thresholds
        self.TSprot = params[9]
        self.Hprot  = params[10]

        self.pitch_hist = [0 for i in range(self.n)]
        self.yaw_hist   = [0 for i in range(self.n)]


    def Elev(self,x):
        """
        EA controller elevator input
        """

        #Store pitch input onto pitch history list
        self.pitch_hist.append(x[7])
        
        pitch      = self.pitch_hist.pop(0)
        
        pitch_ref  = self.pref*d2r
        pitch_ref0 = (self.pref - 2)*d2r
        q          = x[4]
        kp         = self.kpe
        kd         = self.kde
        vr         = self.vr
        h          = x[11]

        pitchTS    = self.TSprot*d2r
        hTS        = self.Hprot

        if(x[12] > vr):
            if( (pitchTS - pitch) < 0 and (hTS - h) > 0 ):
                u     = kp * (pitch_ref0 - (pitch)) + kd*q
            else:
                u     = kp * (pitch_ref - (pitch)) + kd*q
        else:
            u     = kp * (0 - (pitch)) + kd*q
                
        if(u >= 30*d2r):
            u = 30*d2r
        elif(u <= -30*d2r):
            u = -30*d2r
        

        return u


    def Ail(self,x):
        return 0

    def Rud(self,x):
        """
        EA controller rudder input
        """

        #Store pitch input onto pitch history list
        self.yaw_hist.append(x[8])
        
        yaw       = self.yaw_hist.pop(0)

        
        yaw_ref   = self.kpy * x[10]
        q         = x[5]
        kp        = self.kpr
        kd        = self.kdr

        r2d       = 180.0/np.pi
        
        u         = kp * (yaw_ref - (yaw*r2d)) + kd*q*r2d
                
        if(u >= 30*d2r):
            u = 30*d2r
        elif(u <= -30*d2r):
            u = -30*d2r

        return u

    def Throttle(self,sim):


        V1  = sim.Aircraft.Params[14]
        mur = sim.Aircraft.Params[4]
        mub = sim.Aircraft.Params[5] + 0.15

        X   = sim.x0[9]
        V   = sim.x0[12]
   
        if(V < 0):
            V = 0
        
        if(X < 0):
            X = 0
            
    
        s   = GetVXstate(V,X,sim.Aircraft.Params)

        if(sim.engfailure == 0):
            if(s in [8,9,10,11,12,13,15,17]):
                sim.Aircraft.mu = mub
                T = 0.0
            else:
                sim.Aircraft.mu = mur
                T = self.Tmax
            
        elif(sim.engfailure == 1):
            if(s in [1,2,8,9,10,11,12,13,15,17]):
                sim.Aircraft.mu = mub
                T = 0.0
            else:
                sim.Aircraft.mu = mur
                T = self.Tmax/2.0

        else:
            sim.Aircraft.mu = mub
            T = 0.0

        return T
            
            
    def getEAInput(self,sim):
        
        x = sim.x0

        ue = self.Elev(x)
        ua = self.Ail(x)
        ur = self.Rud(x)
        ut = self.Throttle(sim)

        U  = [ue,ua,ur,ut]

        return U
