import os,sys

libpath = os.path.abspath('../')
sys.path.append(libpath)

path1  = '../AircraftModels/'

aircraft = path1 + 'GTM.dat'
perf     = path1 + 'GTM_performance.dat'

from TK_Aircraft import *
from TK_Abstraction import GetVXstate

GTM      = TK_Aircraft(aircraft,perf)

# params1
# Vstall, Vlof, g, W, mu, mub, CD, CL, rho,
# Sref, gamma, alpha0, RLength, wind

# wind speed (+ tail wind / - head wind)
GTM.Params[12] = 1500
GTM.Params[13] = 0

# Parameters computed based on initial set of parameters
# V1, V2, V2f, Vef, Vab, S0, Tmax
GTM.Compute_Performance(GTM.Params)

fig1 = plot_envelope(GTM.Params,partitions=1,vecfield=0,plotgrid=0)

"""
#=====================================================================
# Envelope with wind
# + positive for tail wind/ - negative for headwind

GTM.Params[13] = 5
GTM.Compute_Performance(GTM.Params)
fig2 = plot_envelope(GTM.Params,partitions=1)
xlim([0,70])
"""

"""
#=====================================================================
# Comparison with analytical model

libpath = os.path.abspath('../GenSim')
sys.path.append(libpath);
from GenSim import *

# Aircraft state
# u,v,w,p,q,r,roll,pitch,yaw,Xe,Ye,h,U,alpha,beta
x0     = [0.1,0,0,0,0,0,0,0.0,0,0,0,0,0,0,0]

# Simulation time span and timestep
tspan  = 45
dt     = 0.1

SIM          = GenSim(dt,tspan,x0)
SIM.Aircraft = GTM
SIM.Uin      = zeros((4,SIM.size_t))
SIM.GearFx   = zeros((1,SIM.size_t))
SIM.rto      = 590

def Control(sim,t,x0):
    i = int(t/sim.dt)
    U,LG = [0,0,0,GTM.Params[19]],1
    sim.Uin[:,i] = U
    
    if(x0[9] > SIM.rto):
        U[3] = U[3]/2
        sim.Aircraft.mu = sim.Aircraft.mu

    Fg,Mg = GearDynamics(sim.Aircraft,x0)

    roll,pitch,yaw = x0[6],x0[7],x0[8]

    DCM   = sim.Aircraft.DCM(roll,pitch,yaw)
    
    Fge   = np.dot(DCM,Fg)

    sim.GearFx[0,i] = Fge[0][0]

    return U,LG

SIM.SetUpdateFunc(Control)
SIM.SetOutputFunc(GTM.SimDyn)
SIM.RunSim()

# Plot simulation outputs
plot(SIM.yout[12,0:SIM.size_t],SIM.yout[9,0:SIM.size_t]);

A_alpha  = zeros((1,SIM.size_t))
B_alpha  = zeros((1,SIM.size_t))
A_CL     = zeros((1,SIM.size_t))
A_CD     = zeros((1,SIM.size_t))
B_CL     = zeros((1,SIM.size_t))
B_CD     = zeros((1,SIM.size_t))
A_Fx     = zeros((1,SIM.size_t))
B_Fx     = zeros((1,SIM.size_t))
A_L      = zeros((1,SIM.size_t))
B_L      = zeros((1,SIM.size_t))
A_D      = zeros((1,SIM.size_t))
B_D      = zeros((1,SIM.size_t))

for i in range(SIM.size_t):
    alpha  = np.arctan(SIM.yout[2,i]/SIM.yout[0,i])

    if(int(SIM.yout[12,i]) == 0):
        asin = 0
    else:
        asin   = SIM.yout[1,i]/SIM.yout[12,i];

    beta   = np.arcsin(asin)
    p,q,r  = SIM.yout[3,i],SIM.yout[4,i],SIM.yout[5,i]
    e,a,d  = SIM.Uin[0,i],SIM.Uin[1,i],SIM.Uin[2,i]

    C            = GTM.GenCoeff(alpha,beta,p,q,r,e,a,d)
    B_CL[0,i]    = C[2]
    B_CD[0,i]    = C[0]
    A_CL[0,i]    = GTM.Params[7]
    A_CD[0,i]    = GTM.Params[6]
    B_alpha[0,i] = alpha*180.0/np.pi
    A_alpha[0,i] = GTM.alpha0
    
    V        = SIM.yout[12,i]
    La       = 0.5*GTM.rho*V**2*GTM.Sref*A_CL[0,i]
    Da       = 0.5*GTM.rho*V**2*GTM.Sref*A_CD[0,i]
    W        = GTM.W

    mu       = GTM.Params[4]
    if(SIM.yout[9,i] > SIM.rto):
        mu = GTM.mu

    A_Fx[0,i] = -mu*(W-La)
    B_Fx[0,i] = SIM.GearFx[0,i]
    A_L[0,i]  = La
    A_D[0,i]  = Da
    B_L[0,i]  = 0.5*GTM.rho*V**2*GTM.Sref*C[2]
    B_D[0,i]  = 0.5*GTM.rho*V**2*GTM.Sref*C[0]

figure()
subplot(321)
plot(SIM.Time[0:SIM.size_t],A_CL[0,0:SIM.size_t],'r')
plot(SIM.Time[0:SIM.size_t],B_CL[0,0:SIM.size_t],'b')
ylabel('CL')
subplot(322)
plot(SIM.Time[0:SIM.size_t],A_CD[0,0:SIM.size_t],'r')
plot(SIM.Time[0:SIM.size_t],B_CD[0,0:SIM.size_t],'b')
ylabel('CD')
ax = subplot(323)
plot(SIM.Time[0:SIM.size_t],A_L[0,0:SIM.size_t],'r')
plot(SIM.Time[0:SIM.size_t],B_L[0,0:SIM.size_t],'b')
ax.yaxis.major.formatter.set_powerlimits((0,0)) 
ylabel('Lift')
ax = subplot(324)
plot(SIM.Time[0:SIM.size_t],A_D[0,0:SIM.size_t],'r')
plot(SIM.Time[0:SIM.size_t],B_D[0,0:SIM.size_t],'b')
ax.yaxis.major.formatter.set_powerlimits((0,0)) 
ylabel('Drag')
ax = subplot(325)
plot(SIM.Time[0:SIM.size_t],A_alpha[0,0:SIM.size_t],'r')
plot(SIM.Time[0:SIM.size_t],B_alpha[0,0:SIM.size_t],'b')
ax.yaxis.major.formatter.set_powerlimits((0,0)) 
ylabel('alpha')
ax = subplot(326)
plot(SIM.Time[0:SIM.size_t],A_Fx[0,0:SIM.size_t],'r')
plot(SIM.Time[0:SIM.size_t],B_Fx[0,0:SIM.size_t],'b')
ax.yaxis.major.formatter.set_powerlimits((0,0)) 
ylabel('Friction')

show()
"""
