"""
 Human pilot model - See McRuer et al
"""
import numpy as np

r2d       = 180.0/np.pi
d2r       = np.pi/180


class PilotModel():
    
    def __init__(self):
        return
        
    def Init(self,param):
        
        self.vr   = param[0]
        self.kpe  = param[1]
        self.kde  = param[2]
        self.n    = param[3]
        self.pref = param[4]
        self.Tmax = param[5]

        self.kpr    = param[6]
        self.kdr    = param[7]
        self.kpy    = param[8]

        self.pitch_hist = [0 for i in range(self.n)]
        self.yaw_hist   = [0 for i in range(self.n)]

    def Elev(self,x):
        """
        Pilot elevator input
        """

        #Store pitch input onto pitch history list
        self.pitch_hist.append(x[7])
        
        pitch     = self.pitch_hist.pop(0)
        
        pitch_ref = self.pref*d2r
        q         = x[4]
        kp        = self.kpe
        kd        = self.kde
        vr        = self.vr

        if(x[12] > vr):
            u     = kp * (pitch_ref - (pitch)) + kd*q
        else:
            u     = kp * (0 - (pitch)) + kd*q
                
     
        if(u >= 30*d2r):
            u = 30*d2r
        elif(u <= -30*d2r):
            u = -30*d2r
           
        return u


    def Ail(self,x):
        return 0

    def Rud(self,x):
        """
        Pilot rudder input
        """

        #Store pitch input onto pitch history list
        self.yaw_hist.append(x[8])
        
        yaw       = self.yaw_hist.pop(0)

        
        yaw_ref   = self.kpy * x[10]
        q         = x[5]
        kp        = self.kpr
        kd        = self.kdr
        
        u         = kp * (yaw_ref - (yaw*r2d)) + kd*q*r2d
                
        if(u >= 30*d2r):
            u = 30*d2r
        elif(u <= -30*d2r):
            u = -30*d2r

        return u

    def Throttle(self,sim):


        V1  = sim.Aircraft.Params[14]
        mur = sim.Aircraft.Params[4]
        mub = sim.Aircraft.Params[5] + 0.15

        if(sim.engfailure == 0):
        # Nominal case - no engine failure
            sim.Aircraft.mu = mur
            return self.Tmax
        elif(sim.engfailure == 1):
            if(int(sim.RTO) == 0):
            # Proper rejected takeoff scenario
                if(sim.x0[12] > V1):
                    # Don't abort if airspeed is greater than V1
                    sim.Aircraft.mu = mur
                    return self.Tmax/2.0
                else:
                    # Abort if airspeed is less than V1
                    sim.Aircraft.mu = mub
                    return 0
            else:
                # Improper RTO scenario
                sim.Aircraft.mu = mur
                return self.Tmax/2.0
        else:
            # All engines failure
            sim.Aircraft.mu = mub
            return 0
            
            
    def getPilotInput(self,sim):
        
        x = sim.x0

        ue = self.Elev(x)
        ua = self.Ail(x)
        ur = self.Rud(x)
        ut = self.Throttle(sim)

        U  = [ue,ua,ur,ut]

        return U
