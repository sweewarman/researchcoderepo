## @package Takeoff Aircraft Class
#  @detail Contains a generic takeoff aircraft class with 
#  attributes and objects to perform takeoff simulations
#  @author Sweewarman Balachandran
#  @date   12-22-2014


import os, sys

from numpy import log,sqrt,pi,sin,cos
from AircraftSim.Aircraft import *
from AnalyticalModel import *
from TK_constraints import *
from PlotEnvelope import *
from openopt import NLP


##
# Class derived from Aircraft
class TK_Aircraft(Aircraft):
    
    def __init__(self,filename1,filename2):

        Aircraft.__init__(self,filename1)
        
        fp2 = open(filename2,'r')

        # ignore first entry
        fp2.readline();
        
        self.Tmax    = float(fp2.readline().split()[1])
        self.gamma   = float(fp2.readline().split()[1])
        self.RLength = float(fp2.readline().split()[1])
        self.hobs    = float(fp2.readline().split()[1])
        self.mu      = float(fp2.readline().split()[1])
        self.mub     = float(fp2.readline().split()[1])
        self.CL0     = float(fp2.readline().split()[1])
        self.CLmax   = float(fp2.readline().split()[1])
        self.CD0     = float(fp2.readline().split()[1])
        self.rho     = float(fp2.readline().split()[1])
        self.alpha0  = float(fp2.readline().split()[1])
        self.pitchTS = float(fp2.readline().split()[1]) * pi/180 
        self.hTS     = float(fp2.readline().split()[1])          
        self.Cmq     = -44.34
        self.Cmde    = -1.785
        


        # Takeoff performance speeds
        self.Vstall  = sqrt(2*self.W/(self.rho * self.Sref * self.CLmax));
        self.Vlof    = 1.05*self.Vstall;
        self.V1      = 0;
        self.V2      = 0;
        self.Vmcg    = 0;
        self.Vab0    = 0;
        self.Vef0    = 0;
        self.Vc      = 0;
        self.Vr      = 0;


        self.Params        = [0 for i in range(20)]
        self.Params[0]     = self.Vstall 
        self.Params[1]     = self.Vlof   
        self.Params[2]     = self.g      
        self.Params[3]     = self.W      
        self.Params[4]     = self.mu     
        self.Params[5]     = self.mub    
        self.Params[6]     = self.CD0    
        self.Params[7]     = self.CL0   
        self.Params[8]     = self.rho   
        self.Params[9]     = self.Sref   
        self.Params[10]    = self.gamma  
        self.Params[11]    = self.alpha0  
        self.Params[12]    = self.RLength 
        self.Params[13]    = 0              #wind speed
        self.Params[14]    = 0              #V1
        self.Params[15]    = 0              #V2
        self.Params[16]    = 0              #Vef
        self.Params[17]    = 0              #Vab
        self.Params[18]    = 0              #S
        self.Params[19]    = 0              #Tmax
        
        
        
    def Compute_Performance(self,params,LB=0,UB=0,X0=0):
        
        # Objective function to minimize
        f = lambda X: params[19]
        
        # inequality constraints
        c = lambda X: constraints(self,X,params,1);
        
        # equality constraints
        h = lambda X: constraints(self,X,params,2);
        
        # Lower and Upper bounds on the solution
        # May need to change Lower and upper bounds based on aircraft type
        
        if(int(LB) is 0):
            
            print "Using default LB,UB,X0"
            
            LB = [1,1,1,1,-500,500];    
            UB = [3.0*76,3.0*76,3.0*76,3.0*76,300,400000];

            # Initial guess
            # X = [V1r,V2,Vef0,Vab0,T]
            X0 = [20,30,20,20,0,200000];

            
        # setup the Non linear program
        p  = NLP(f,X0,c=c,h=h,lb=LB,ub=UB);
        
        # Solve the non linear program
        sol = p.solve('ralg');

        # Get solution
        Xsol = sol.xf;

        print "Ground speeds"
        print "V1    = %f\nV2    = %f\nVef0  = %f\nVab0  = %f\nS0    = %f\nTmax  = %f\n" \
          %(Xsol[0],Xsol[1],Xsol[2],Xsol[3],Xsol[4],Xsol[5])

        self.Params[14] = Xsol[0]
        self.Params[15] = Xsol[1]
        self.Params[16] = Xsol[2]
        self.Params[17] = Xsol[3]
        self.Params[18] = Xsol[4]
        self.Params[19] = Xsol[5]
        

        return Xsol
        
   
    
    def ChkTailStrike(self,X):
        
        pitch = X[7]
        h     = X[11]

        pitchTS = self.pitchTS
        hTS     = self.hTS

        # Check for tail strike
        if ( (pitch >= pitchTS) and (h <= hTS) ):
            X[7]  = pitchTS
            X[4]  = 0
            #print "***Tail strike***"

        # Stop negative altitude
        if( h <= -1):
            X[11] = -1
            
        return X

    def AppxDynamics(self,x,t,U):


        X     = x[9]
        V     = x[12]
        Pitch = x[7]
        q     = x[4]
        H     = x[11]

        ue    = U[0]

        dyn   = 0.5*self.rho*V**2
        
        
        AB      = AnalyticalModel(self.Params)
        
        A10 = AB[0];
        B10 = AB[1];
        A50 = AB[8];
        B50 = AB[9];
         
        Sref   = self.Params[9]
        gamma  = self.Params[10]

        Vw     = self.Params[13]
        Vlof   = self.Params[1]

        
        
        if(V < Vlof ):
            dX = V
            dV = ( A10 - B10*(V**2) )
            dH = 0
        else:
            dX = V*cos(gamma*pi/180)
            dV = ( A50 - B50*(V**2) )
            dH = V*sin(gamma*pi/180)
            

        dPitch = q

        dq     = (self.Cmq*q + self.Cmde*ue)*dyn*Sref*self.c/self.Iyy

        dx     = [0,0,0,0,dq,0,0,dPitch,
                     0,dX,0,dH,dV,0,0]

        return dx


    def SimAppDyn(self,x,t,args):

        """
        wrapper for dynamics
        """

        U  = args
                
        y  =  self.AppxDynamics(x,t,U)

        return y

