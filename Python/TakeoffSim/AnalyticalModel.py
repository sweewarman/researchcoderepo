"""
   
    Analytical takeoff model
    
"""
from numpy import sin,cos,pi

def AnalyticalModel(Params):
    

    g       = Params[2]
    W       = Params[3]
    mu      = Params[4]
    mub     = Params[5]
    CD      = Params[6]
    CL      = Params[7]
    rho     = Params[8]
    S       = Params[9]
    gamma   = Params[10]
    alpha   = Params[11]
    Tmax    = Params[19]
    
    
    # Takeoff with TOGA (Tmax) thrust setting
    A10 = g * ( Tmax / (W ) - mu );
    B10 = g/W * ( 0.5 * rho * S * ( CD - mu * CL ) );
    
    # Continued takeoff after single  engine failure (twin engine aircraft)
    A20 = g * ( 0.5 * Tmax / (W ) - mu );
    B20 = B10;
    
    # Lift off after continued takeoff with single engine failure
    A30 = g * ( 0.5 * Tmax / (W)*cos(alpha * pi/180) - sin( gamma * pi/180 ));
    B30 = g/W * ( 0.5 * rho * S *  CD ); 

    # Deceleration after rejected takeoff
    A40 = g * ( 0 / (W ) - mub );
    B40 = g/W * ( 0.5 * rho * S * ( CD - mub * CL ) );

    # Lift off after continued takeoff
    A50 = g * ( Tmax / (W )*cos(alpha * pi/180) - sin( gamma * pi/180 ));
    B50 = g/W * ( 0.5 * rho * S *  CD ); 

    return [A10,B10,A20,B20,A30,B30,A40,B40,A50,B50]
