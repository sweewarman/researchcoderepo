NOTES
=====

See Test.py for example

1. Set up directory to access Aircraft class 
2. Instantiate a Takeoff class
3. Compute Performance must be run manually with the parameters as arguments
4. Parameter description (20 values): 
   Vstall, Vlof, g, W, mu, mub, CD, CL, rho, Sref,gamma,alpha,RLength,wind
   V1,V2,Vef,Vab,S0,Treq
   
Takeoff Envelope Computation
----------------------------

The takeoff envelopes can be computed in multiple ways. A basic solution approach would be based on balanced field limit.
This would involve computing a balance field limit weight given a thrust setting. The approach used in this code set (at least for now)
makes use of fied limit length considering an inital offset distance.