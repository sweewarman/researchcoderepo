"""
    constraints for the takeoff phase used to determine the V speeds
"""

from numpy import log,sin,cos,tan,pi
from AnalyticalModel import *
from scipy.integrate import quad

def Integrand(V,A,B,Vw,gamma):
    return ((V + Vw)*cos(gamma))/(A+B*V**2)
    

def constraints(self,X,Params,con_type):
    """
    function to define equality and inequality constraints

    @param  X  variables over which optimization is performed


    V1   :=  latest speed at which a takeoff can be aborted.
    V2   :=  takeoff safety speed. 
    Vef0 :=  velocity of engine failure at the beginning of the runway 
             to continue takeoff.
    Vab0 :=  largest airspeed at the beginning of the runway to abort
             the takeoff safely.

    @param Params Parameters for the envelope  
    
    @param  con_type  constraint type. 1-Inequality, 2-Equality

    @retval returns the specified constraint type

    @bug not sure what would happen if Vab0 is <= V1r
    """
    
    Vw    = Params[13]
    V1    = X[0] 
    V2    = X[1] 
    Vef0  = X[2] 
    Vab0  = X[3]
    S0    = X[4]
    Tmax  = X[5]

    Vstall  = Params[0]  
    Vlof    = Params[1]
    g       = Params[2]
    W       = Params[3]
    mu      = Params[4]
    mub     = Params[5]
    CD      = Params[6]
    CL      = Params[7]
    rho     = Params[8]
    S       = Params[9]
    gamma   = Params[10]
    alpha0  = Params[11]
    RLength = Params[12] 

    Params[19] = Tmax

    AB      = AnalyticalModel(Params)

    A10 = AB[0];
    B10 = AB[1];
    A20 = AB[2];
    B20 = AB[3];
    A30 = AB[4];
    B30 = AB[5];
    A40 = AB[6];
    B40 = AB[7];
    A50 = AB[8];
    B50 = AB[9];
        
    S1  = quad(Integrand,-Vw,V1,args=(A10,B10,Vw,0))[0]
    S3  = quad(Integrand,Vlof,V2,args=(A30,B30,Vw,0.14))[0]
    S4  = quad(Integrand,V1,-Vw,args=(A40,B40,Vw,0))[0]
    S5  = quad(Integrand,Vef0,V1,args=(A20,B20,Vw,0))[0]
    S6  = quad(Integrand,Vab0,V1,args=(A40,B40,Vw,0))[0]
    S7  = quad(Integrand,V1,Vlof,args=(A20,B20,Vw,0))[0]

    # Inequality constraints
    Cineq1   =  Vlof - V2
    Cineq2   =  V1 - V2
        
    # Equality constraints
    Ceq1     = S3*tan(gamma * pi/180) - self.hobs;
    Ceq2     = S0 + S1 + S4 - RLength;
    Ceq3     = S0 + S1 + S7 + S3 - RLength
    Ceq4     = S5 - S1 - S0;
    Ceq5     = S6 - S1 - S0;
 
    Cineq    = [Cineq1,Cineq2];
    Ceq      = [Ceq1,Ceq2,Ceq3,Ceq4,Ceq5];

    if(con_type == 1):
        return Cineq
    elif(con_type == 2):
        return Ceq
    
    
    
