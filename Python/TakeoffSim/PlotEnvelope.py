"""
    Function to plot envelopes
"""
from pylab import *
import numpy as np
from AnalyticalModel import *
from TK_constraints import Integrand, quad

def plot_envelope(Params,partitions=0,vecfield=0,plotgrid=1):
  

    Vw      = Params[13]
   

    V1      = Params[14] 
    V2      = Params[15] 
    Vef0    = Params[16] 
    Vab0    = Params[17]
    Vlof    = Params[1] 
    gamma   = Params[10]
    alpha0  = Params[11]
    RLength = Params[12]
    S0      = Params[18]

    AB      = AnalyticalModel(Params)

    A10 = AB[0];
    B10 = AB[1];
    A20 = AB[2];
    B20 = AB[3];
    A30 = AB[4];
    B30 = AB[5];
    A40 = AB[6];
    B40 = AB[7];
    A50 = AB[8];
    B50 = AB[9];
    
    Va1 = np.linspace(0,V1,100);
    Va2 = np.linspace(V1,Vlof,100);
    Va3 = np.linspace(Vlof,V2,100);
    Va4 = np.linspace(0,Vab0,100);
    Va5 = np.linspace(Vef0,V1,100);

    Sa1 = np.zeros((Va1.size));
    Sa2 = np.zeros((Va2.size));
    Sa3 = np.zeros((Va3.size));
    Sa4 = np.zeros((Va4.size));
    Sa5 = np.zeros((Va5.size));

    # Distance from V=0 to V=V1
    for i,v in enumerate(Va1):
        Sa1[i] = S0 + quad(Integrand,-Vw,v,args=(A10,B10,Vw,0))[0]

    # Distance from V=V1 to V=Vlof
    for i,v in enumerate(Va2):
        Sa2[i] = Sa1[-1] + quad(Integrand,V1,v,args=(A20,B20,Vw,0))[0]

    # Distance from V=Vlof to V=V2
    for i,v in enumerate(Va3):
        Sa3[i] = Sa2[-1] + quad(Integrand,Vlof,v,args=(A30,B30,Vw,8*pi/180))[0]

    SV2 = Sa2[-1]\
        + quad(Integrand,Vlof,V2,args=(A30,B30,Vw,0))[0]*cos(gamma*pi/180)

    # Distance from V = Vab0 to V = 0 (Tricky!!!)
    for i,v in enumerate(Va4):
        Sa4[i] = quad(Integrand,Vab0,v,args=(A40,B40,Vw,0))[0]

    # Distance travelled from V=Vef0 to V1
    for i,v in enumerate(Va5):
        Sa5[i] =  quad(Integrand,Vef0,v,args=(A20,B20,Vw,0))[0]

    # x and y limits for plotting
    xlimit = V2 + 10;
    ylimit = RLength + 100;

    # Runway 
    RunwayX = np.arange(0,V2 + 10);
    RunwayY = np.ones(RunwayX.size) * RLength;

    # Partitions of the envelop
    p_x1 = Vef0;
    p_x2 = V1;
    p_x3 = V2;
    p_x4 = Vab0;
    p_x5 = Vlof;
    p_y1 = Sa1[-1];
    p_y2 = RLength;
		 
    Fig_envelop = figure()
    plot(Va1,Sa1,color="blue");
    plot(Va2,Sa2,color="blue");
    plot(Va3,Sa3,color="blue");
    plot(Va4,Sa4,color="blue");
    plot(Va5,Sa5,color="blue");
    plot(RunwayX,RunwayY,color="red");

    #scatter(V2,SV2,marker="o")


    if(partitions):
        plot(p_x1*np.ones(int(RLength)),np.arange(int(RLength)),color="black");
        plot(p_x2*np.ones(int(RLength)+100),np.arange(int(RLength)+100),color="black");
        plot(p_x3*np.ones(int(RLength)+100),np.arange(int(RLength)+100),color="black");
        plot(np.arange(int(p_x3+1)),p_y1*np.ones(int(p_x3+1)),color="black");


    xlim(0,xlimit);
    ylim(0,ylimit);
    if(plotgrid):
        grid(b='on')
    
    xlabel("Airspeed (m/s)")
    ylabel("Distance (m)")



    if(vecfield):
        vGridVec = np.linspace(0,V2+10,30);
        xGridVec = np.linspace(0,RLength+100,30);
        vGrid,xGrid = meshgrid(vGridVec,xGridVec);
        
        xdot = vGrid
        vdot = (A40 - B40*(vGrid)**2) * 17;
        
        vdot = vdot
        
        #Vlength = np.sqrt(vGrid**2 + xGrid**2)
        #vdot = vdot/Vlength;
        #xdot = xdot/Vlength;

        quiver(vGrid,xGrid,vdot,xdot,color=(1,0,1))

    return Fig_envelop
    
