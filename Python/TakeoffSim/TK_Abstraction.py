"""
Function mapping continuous state values to abstract states
"""

import numpy as np
from numpy import log
from AnalyticalModel import *
from TK_constraints import Integrand
from scipy.integrate import quad

def GetVXstate(V,X,params):

    
    Vstall  = params[0]
    Vlof    = params[1] 
    g       = params[2]
    W       = params[3]
    mu      = params[4]
    mub     = params[5]
    CD      = params[6]
    CL      = params[7]
    rho     = params[8]
    S       = params[9]
    gamma   = params[10]
    gamma   = params[11]
    RLength = params[12]
    Vw      = params[13]    
    V1      = params[14]
    V2      = params[15]
    Vef0    = params[16]
    Vab0    = params[17]
    S0      = params[18]
    Tmax    = params[19]

    AB      = AnalyticalModel(params)
        
    A10     = AB[0];
    B10     = AB[1];
    
    x1   = Vef0; # Vef0                                    
    x2   = V1;   # V1                                            
    x3   = V2;   # V2                                           
    x4   = Vab0; # Vab0                                          
    x5   = Vlof; # Vlof                                     
    y1   = S0 + quad(Integrand,-Vw,V1,args=(A10,B10,Vw))[0]
    y2   = RLength
    fac  = 0;

    z    = [X,V]

    if(X < 0):
        X = 0
    elif(X>RLength+100):
        X = RLength+100

    if(V < 0):
        V = 0
    elif(V > max(V2,Vab0) + 10):
        V = max(V2,Vab0)
    

    #Checking for the point within 0 and x1                          
    if( z[1]>=0 and z[1]<=x1):

        if( z[0]>=0 and z[0]<= y1 ):
            if((z[0] - Grnd_Roll_Pos(AB,z[1],0,1,params) + fac) <= 0):
                R = 1;
            else:
                R = 8;

        if( z[0]>y1 and z[0]<= y2 ):
            if( (z[0] - Grnd_Roll_Pos(AB,z[1],x4,0,params) + fac) <= 0 ):
                R = 10;
            else:
                R = 12;

        if( z[0]>y2 ):
            R = 15;


    if(z[1] == 0 and (z[0] > 0 and z[0] <= RLength) ):
        R = 17;
    elif(z[1] == 0 and (z[0] > RLength)):
        R = 15

    #Checking for the point within x1 and x2                                
    if( z[1]>x1 and z[1]<=x2):
        if( z[0]>=0 and z[0]<= y1 ):
            if((z[0] - Grnd_Roll_Pos(AB,z[1],x1,0.5,params) + fac) <= 0):
                R = 3;
            elif( (z[0] - Grnd_Roll_Pos(AB,z[1],x1,0.5,params) + fac) >= 0  and\
                    (z[0] - Grnd_Roll_Pos(AB,z[1],0,1,params) + fac) <= 0 ):
                R = 2;
            else:
                R = 9;

        if( z[0]>y1 and z[0]<= y2 ):
            if( (z[0] - Grnd_Roll_Pos(AB,z[1],x4,0,params) + fac) <= 0 ):
                R = 11;
            else:
                R = 13;

        if( z[0]>y2 ):
            R = 15;

    #Checking for the point within x2 and x3                                
    if( z[1]>x2 and z[1]<=x3 ):
        if( z[0]>=0 and z[0]<= y1 ):
            if((z[0] - Grnd_Roll_Pos(AB,z[1],x4,0,params) + fac) <= 0):
                R = 4;
            else:
                R = 5;

        if( z[0]>y1 and z[0]<= y2 ):
            if( (z[0] - Grnd_Roll_Pos(AB,z[1],x1,0.5,params) + fac) <= 0 ):
                R = 6;
            else:
                R = 14;

        if( z[0]>y2 ):
            R = 16;

    if( z[1]>x3 ):
        R = 7;

    if( z[1]< 0 ):
        R = 17;

    return R
    

def Grnd_Roll_Pos(AB,V,V0,Tfac,params):

    A10 = AB[0];
    B10 = AB[1];
    A20 = AB[2];
    B20 = AB[3];
    A30 = AB[4];
    B30 = AB[5];
    A40 = AB[6];
    B40 = AB[7];
    A50 = AB[8];
    B50 = AB[9];
    
    Vw      = params[13]
    V1      = params[14]
    V2      = params[15]
    Vef0    = params[16]
    Vab0    = params[17]
    S0      = params[18]

    #Takeoff distance covered for a given V starting from V0                 
    if(Tfac==1):
        if(V <= V1):
            S  = S0 + quad(Integrand,-Vw,V,args=(A10,B10,Vw))[0]
        elif( V > V1 and V <= Vlof ):
            S  = S0 + quad(Integrand,-Vw,V1,args=(A10,B10,Vw))[0]
            S  = S + quad(Integrand,V1,V,args=(A20,B20,Vw))[0]
        else:
            S  = S0 + quad(Integrand,-Vw,V1,args=(A10,B10,Vw))[0]
            S  = S + quad(Integrand,V1,Vlof,args=(A20,B20,Vw))[0]
            S  = S + quad(Integrand,Vlof,V,args=(A30,B30,Vw))[0]*cos(ac.gamma*pi/180);
    elif(Tfac<1 and Tfac>0):
        S = quad(Integrand,Vef0,V,args=(A20,B20,Vw))[0]
    else:
        S = quad(Integrand,Vab0,V,args=(A40,B40,Vw))[0]
        
        
    return S


def GetPQHstate(P,Q,H,partitionP,partitionH):
    """
    Function to get discrete state corresponding to given
    values of pitch (P), pitch rate (Q) and altitude (H)
    
    partitionP is a list of pitch values that partition the state-space

    partitionH is a list of altitude values that partition the state-space

    angles in deg, height in m
    """

    P = P
    Q = Q

    if(P < partitionP[0]):
        P = partitionP[0]
    elif(P > partitionP[-1]):
        P = partitionP[-1]
    
    if(H < partitionH[0]):
        H = partitionH[0]
    elif(H > partitionH[-1]):
        H = partitionH[-1]
    

    lenH = len(partitionH)-1
    lenP = len(partitionP)-1

    for i in range(lenH):
        if( partitionH[i] <= H < partitionH[i+1] ):
            for j in range(lenP):
                if( partitionP[j] <= P < partitionP[j+1] ):
                    return (i*lenP+j)
                
    
def GetThruststate(T,PartitionT):
    """
    Function to obtain discrete thrust state
    """

    lenT = len(PartitionT) - 1

    for i in range(lenT):
        if (PartitionT[i] <= T < PartitionT[i+1]):
            return i
    
