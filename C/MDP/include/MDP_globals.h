#ifndef MDP_GLOBALS_H
#define MDP_GLOBALS_H

#include "MDP.h"

EXTERN features_t *States;   /**< Pointer to a feature struct for states */
EXTERN features_t *Actions;  /**< Pointer to a feature struct for action */

EXTERN int nStates,nActions; /**< Number of state features, action features */
EXTERN int solver;           /**< Solver type 1-Value Iteration, 2-Policy Iteration */
EXTERN int init_condition;   /**< Initial condition */
 
EXTERN int Size_TS;          /**< Total number of states */
EXTERN int Size_TA;          /**< Total number of actions */
EXTERN int Size_VS;          /**< Size of value space (all state-action pairs) */

EXTERN double* SnapShot_FA;  /**< Array of state and action feature instantiation */
EXTERN double* SnapShot_F;   /**< Array of state feature instantiation*/
EXTERN int* F_A_Size;        /**< Array of size of each state and action feature */
EXTERN int* F_Size;          /**< Array of size of each state feature */

EXTERN int* F_A_Indices;     /**< Array of state and action feature subscripts */
EXTERN int* F_Indices;       /**< Array of state feature subscripts */
EXTERN int* A_Indices;       /**< Array of action feature subscripts */

EXTERN double* VALUE;        
EXTERN double* VALUE_OLD; 
EXTERN double* POLICY; 
EXTERN double* POLICY_OLD; 

EXTERN gsl_matrix* V;      /**< gsl matrix of Values */
EXTERN gsl_matrix* V_old;  /**< gsl matrix to hold old Values of each state */
EXTERN gsl_matrix* V_temp; /**< gsl matrix to hold Values for algebra */
EXTERN gsl_matrix* Policy; /**< gsl matrix for policy */

EXTERN tensor_t* Tensor;   /**< struct for transition probabilities */
EXTERN rewards_t* Rewards; /**< struct for rewards */
EXTERN value_t* Values;    /**< struct for values */

EXTERN double* ValueSpace; 

EXTERN double disc_fac;       /**< Discount factor */ 

#endif
