#ifndef MDP_H
#define MDP_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>


#define MIN_VAL -1e10 /** Minimum value for max operation */

/**
 *@brief Struct to define state and action features
 */
typedef struct FEATURES features_t;
struct FEATURES{

    double *FeatureVal;     /**< Pointer to an array of values of the feature */
    unsigned int Size;     /**< Size of the feature */
    char FeatureName[100];  /**< Char array to store name of feature */
    char **FeatureValName;  /**< Pointer to an array of labels feature values */
};


/**
 *@brief Struct to create a linked list for trace generation 
 */
typedef struct NODE node_t;
struct NODE{
    int index;             /**< State index */
    double prob;           /**< Probability of transition to a different node */
    int terminal;          /**< Flag denoting terminal node (1/0) 1- Terminal node */
    node_t *next;          /**< Pointer to next node */
};

/**
 *@brief Struct for transition probability matrix.
 *@details This struct contains a gsl matrix. The purpose of wrapping a gsl matrix inside a struct is 
 *         enable a tensor implementation.
 */
typedef struct TENSOR tensor_t;
struct TENSOR{
    gsl_matrix *P;
};

/**
 *@brief Struct for rewards
 *@details This struct contains a gsl matrix. The purpose of wrapping a gsl matrix inside a struct is 
 *         enable a tensor implementation.
 */
typedef struct REWARDS rewards_t;
struct REWARDS{
    gsl_matrix *R;
};

/**
 *@brief Struct for values
 *@details This struct contains a gsl matrix. The purpose of wrapping a gsl matrix inside a struct is 
 *         enable a tensor implementation.
 */
typedef struct VALUES value_t;
struct VALUES{
    gsl_matrix *V;
};

/**
 *@brief   Function to parse the configuration file
 *@details This function reads the config.txt file in the /bin folder
 *         and sets all the parameters required to setup the MDP.
 *@param   filename name of configuration file
 */
void ParseConfig(char* filename);

/**
 *@brief   Initializes the MDP variables
 *@details This function allocates memory and initializes 
 *         auxiliary data structures used to solve the MDP.
 *@retval  Returns 1 if successful, else 0
 */
int Init_MDP();

/**
 *@brief   Initializes probability tensor 
 *@details This function initializes the probability tensors [\link Tensor \endlink]
           with the values found in the TensorXX.txt files
	   There should be one .txt file for each action.
  @retval  Returns 1 if successful, else 0
 */
int Init_Tensor();

/**
 *@brief    Initializes rewards
 *@details  This function initializes the reward vector [\link Rewards \endlink] with values
 *          found in the RewardsXX.txt file for each state action pair.
 *          
 */
int Init_Rewards();

/**
 *@brief Perfoms value iteration
 *@details This function perfroms the value iteration. The Rewards and Probability 
 *         Tensors are initialized prior to calling these functions. The discount factor
 *         gamma is set by the user in the config file. The convergence tolerance is current
 *         hard coded in the code. 
 */
void ValueIteration(void);

/**
 * @brief Perfroms policy iteration
 * @details This function performs policy iteration. The Rewards and Probability                                        
 *         Tensors are initialized prior to calling these functions. The discount factor                                 
 *         gamma is set by the user in the config file. The convergence tolerance is current                             
 *         hard coded in the code.
 */	
void Policy_Iteration(void);    

/**
 *@brief Checks for convergence given current and old values
 *@details This function looks are the difference between the current value of the states and
 *         the old value of the states to determine convergence.
 *@param[in] current Vector of current state values
 *@param[in] old     Vector of old state values
 *@retval Returns 1 for convegence, else 0
 */
unsigned char ChkCvg(gsl_matrix* current,gsl_matrix* old);

/**
 *@brief Determines the max value of all the states
 *@details This function determines the max value of the states according to the expression
 *         \f[ V(s) = max_a{V(s,a)} \f] Here \f$s,a\f$ denote the states and actions respectively
 */
void get_max_V();

/**
 *@brief Determines state feature subscripts given an index
 *@details A state is the composition of several features. 
 *         An index identifies a state. A subscript identifies the individual features that the state is made of.
 *         Given a state index, this function determines
 *         the subscripts corresponding to the individual features that the state is composed of.
 *@param[in] index Index of the state
 *@param[in] subsizes Pointer to an 1d Array of length n that holds the size of the n feature
 *@param[in] size Number of features (n)
 *@param[in,out] subscripts Pointer to an array that will be populated with the answer. 
 *                          i.e the subscripts of the individual features
 */
int ind2sub(int index,int* subsizes,int size,int *subscripts);

/**
 *@brief Determines index given subscripts of individual state features
 *@details 
 *@param[in] nFeatures number of state features
 *@param[in] F_size array of size of each feature
 *@param[in] subscripts to be coverted into index
 *@param[out] index corresonding to given subscripts
 */
int sub2ind(int nFeatures,int* F_size,int* subscripts);

/**
 *@brief   Computes all reachable states from an initial node
 *@details This function creates a graph of all the nodes reachable from the current node. It also 
 *         creates a .gv file containing the nodes and edges in the DOT format
 *@param[in]   node Index of the initial node from which all other reachables nodes must be computed.
 */

void TraceFinder(int node);

/**
 *@brief   Prints a transition to the file for the DOT graph
 *@details This is a helper function used by the TraceFinder
 *@param[in] A Starting node
 *@param[in] B Ending node
 *@param[in] k Flag to print in red (0/1)
 *@param[in] prob Transition probability
 */
void PrintTraces(int A,int B,int k,double prob);

/**
 *@brief Output policy to a file
 *@details This function writes output to a file in a formatted fashion
 */
void Output2File(void);

/**
 *@brief Expands the current node
 *@param[in] P Parent node
 *@param[in,out] C Pointer to children
 */
void Expand(NODE* P,NODE** C);					

/**
 *@brief Gets next node to expand
 *@param[in] P1 Pointer to parent node
 *@param[in] P2 Pointer to children
 */
void GetNextHead(NODE** P1,NODE** P2);

/**
 *@brief Deallocates memory
 */
void Dealloc();



#endif
