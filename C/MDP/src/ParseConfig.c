#ifndef PARSE_CFG
#define PARSE_CFG

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define EXTERN extern
#include "MDP.h"
#include "MDP_globals.h"
#define DEBUG


void ParseConfig(char *filename){
    
    char linedata[100];
    char buffer[100];
    char buf_label[100];
    char name[100];

    int char_itr = 0;    
    int buf_itr  = 0;
    
    unsigned char state     = 0;

    FILE *config = fopen(filename,"r");

    int feature_index;
    int feature_size;

    int ICindex[100];
    int FeatSizes[100];

    while(!feof(config)){
	fgets(linedata,100,config);

	while( linedata[0] == '\n' || linedata[0]=='\0' )
	    fgets(linedata,100,config);

	if(linedata[0] == '#'){
	    state += 1;
	    continue;
        }

	if(state == 1){
	    
	    nStates        = atoi(linedata);
	    States        = (features_t*)calloc(nStates,sizeof(features_t));

#ifdef DEBUG
	    printf("Total State features = %d\n",nStates);
#endif
	}

	if(state == 2){
	    
	    nActions = atoi(linedata);
	    Actions = (features_t*) calloc(nActions,sizeof(features_t));

#ifdef DEBUG
	    printf("Total Action features = %d\n",nActions);
#endif
	}

	if(state == 3){
	    
	    char_itr = 0;
	    buf_itr  = 0;
	    
	    /* Read the index of the state till a comma is encountered */
	    while( linedata[char_itr] != ','){
		buffer[buf_itr] = linedata[char_itr];
		char_itr++;
		buf_itr++;
	    }
	    
	    buffer[buf_itr] = '\0';
	    char_itr++;
	    buf_itr = 0;
	    
	    feature_index = atoi(buffer) - 1;

	    memset(buffer,0,sizeof(buffer));

	    /* Read the label of the state till a comma is encountered */
	    while( linedata[char_itr] != ','){
		buffer[buf_itr] = linedata[char_itr];
		char_itr++;
		buf_itr++;
	    }

	    buffer[buf_itr] = '\0';
	    char_itr++;
	    buf_itr = 0;
	  
	    strcpy(States[feature_index].FeatureName,buffer);
	    
#ifdef DEBUG
	    printf("Feature = %s, ",
		   States[feature_index].FeatureName);
#endif
	    memset(buffer,0,sizeof(buffer));
	      
	    /* Read the size of the state feature */
	    
	    while( linedata[char_itr] != '\n'){
		buffer[buf_itr] = linedata[char_itr];
		char_itr++;
		buf_itr++;
	    }

	    buffer[buf_itr] = '\0';
	    
	    feature_size = atoi(buffer);
	    
	    States[feature_index].Size           = feature_size;
	    States[feature_index].FeatureValName = new char*[feature_size];
	    States[feature_index].FeatureVal     = new double[feature_size];

	    for(int i=0;i<feature_size;i++){
		States[feature_index].FeatureValName[i] = new char[100];
	    }
	    

#ifdef DEBUG
	    printf("size = %d\n",feature_size);
#endif
	    
	}

	if(state == 4){
	    
	    char_itr = 0;
	    buf_itr  = 0;
	    
	    /* Read the index of the state till a comma is encountered */
	    while( linedata[char_itr] != ','){
		buffer[buf_itr] = linedata[char_itr];
		char_itr++;
		buf_itr++;
	    }
	    
	    buffer[buf_itr] = '\0';
	    char_itr++;
	    buf_itr = 0;
	    
	    feature_index = atoi(buffer) - 1;

	    memset(buffer,0,sizeof(buffer));

	    /* Read the label of the state till a comma is encountered */
	    while( linedata[char_itr] != ','){
		buffer[buf_itr] = linedata[char_itr];
		char_itr++;
		buf_itr++;
	    }

	    buffer[buf_itr] = '\0';
	    char_itr++;
	    buf_itr = 0;
	  
	    strcpy(Actions[feature_index].FeatureName,buffer);
	   
#ifdef DEBUG
	    printf("Action = %s, ",
		   Actions[feature_index].FeatureName);
#endif

	    memset(buffer,0,sizeof(buffer));
	      
	    /* Read the size of the state feature */
	    
	    while( linedata[char_itr] != '\n'){
		buffer[buf_itr] = linedata[char_itr];
		char_itr++;
		buf_itr++;
	    }
	    
	    buffer[buf_itr] = '\0';
	    
	    feature_size = atoi(buffer);
	    
	    Actions[feature_index].Size = feature_size;
	    Actions[feature_index].FeatureValName = new char*[feature_size];
	    Actions[feature_index].FeatureVal     = new double[feature_size];

	    for(int i=0;i<feature_size;i++){
		Actions[feature_index].FeatureValName[i] = new char[100];
	    }


#ifdef DEBUG
	    printf("size = %d\n",feature_size);
#endif
	    
	}
	
	if(state == 5){
	   
	    
	    char_itr = 0;
	    buf_itr  = 0;
	    
	    /* Read the index of the state till a comma is encountered */
	    while( linedata[char_itr] != ','){
		buffer[buf_itr] = linedata[char_itr];
		char_itr++;
		buf_itr++;
	    }
	    
	    buffer[buf_itr] = '\0';
	    char_itr++;
	    buf_itr = 0;
	    
	    feature_index = atoi(buffer) - 1;

	    memset(buffer,0,sizeof(buffer));

	    char_itr++;

	          
	    printf("State size of %s = %d\n",States[feature_index].FeatureName,States[feature_index].Size);

	    /* Read the size of the state feature */
	    for(int i=0;i<States[feature_index].Size;i++){
		while( linedata[char_itr] != ',' && linedata[char_itr] != ':' 
		                               && linedata[char_itr] != '}' ){
		    
		    buffer[buf_itr] = linedata[char_itr];
		    
		    char_itr++;
		    
		    buf_itr++;
		}

		buffer[buf_itr] = '\0';
		

		if( linedata[char_itr] == ':' ){
		        	
		    sprintf(buf_label,"%s%d",buffer,i+1);
		 
		    
		    memcpy(States[feature_index].FeatureValName[i],buf_label,sizeof(buf_label));
		    States[feature_index].FeatureVal[i]      = i;

#ifdef DEBUG
		    printf("Feature value of %d is %s\n",i,States[feature_index].FeatureValName[i]);
#endif
		}
		else{

		    memcpy(States[feature_index].FeatureValName[i],buffer,sizeof(buffer));
		    
    
#ifdef DEBUG
		    printf("Feature value of %d is %s\n",i,States[feature_index].FeatureValName[i]);
#endif
		    
		    States[feature_index].FeatureVal[i]     = i;

		    memset(buffer,0,sizeof(buffer));
		    
		    char_itr++;
		    buf_itr = 0;
		    
		}

	    }	    
	    
	}


	if(state == 6){
	    
	    char_itr = 0;
	    buf_itr  = 0;
	    
	    /* Read the index of the state till a comma is encountered */
	    while( linedata[char_itr] != ','){
		buffer[buf_itr] = linedata[char_itr];
		char_itr++;
		buf_itr++;
	    }
	    
	    buffer[buf_itr] = '\0';
	    char_itr++;
	    buf_itr = 0;
	    
	    feature_index = atoi(buffer) - 1;

	    memset(buffer,0,sizeof(buffer));

	    char_itr++;

	          
	    /* Read the size of the state feature */
	    for(int i=0;i<Actions[feature_index].Size;i++){
		while( linedata[char_itr] != ',' && linedata[char_itr] != ':' 
		                               && linedata[char_itr] != '}' ){
		    
		    buffer[buf_itr] = linedata[char_itr];
		    
		    char_itr++;
		    
		    buf_itr++;
		}

		buffer[buf_itr] = '\0';
		
		if( linedata[char_itr] == ':' ){
		        	
		    sprintf(buf_label,"%s%d",buffer,i+1);
		    
		    memcpy(Actions[feature_index].FeatureValName[i],buf_label,sizeof(buf_label));
		    Actions[feature_index].FeatureVal[i]     = i;
		    
#ifdef DEBUG
		    printf("Feature value of %d is %s\n",i,Actions[feature_index].FeatureValName[i]);
#endif
		}
		else{
		    
		    memcpy(Actions[feature_index].FeatureValName[i],buffer,sizeof(buffer));
		    Actions[feature_index].FeatureVal[i]      = i;

#ifdef DEBUG
		    printf("Feature value of %d is %s\n",i,Actions[feature_index].FeatureValName[i]);
#endif
		    memset(buffer,0,sizeof(buffer));
		    
		    char_itr++;
		    buf_itr = 0;
		    
		    
		    
		}
	    }
	    
	    
	    
	}

	if(state==7){
	    
	    char_itr = 0;
	    buf_itr  = 0;

	    while( linedata[char_itr] != '\n'){
		buffer[buf_itr] = linedata[char_itr];
		char_itr++;
		buf_itr++;
	    }

	    buffer[buf_itr] = '\0';

	    solver = atoi(buffer);

#ifdef DEBUG
	    printf("alg = %d\n",solver);
#endif
	}

	if(state==8){
	    
	  char_itr = 0;
	  buf_itr  = 0;
 
	  for(int i=0;i<nStates;i++){
	    
	   
	    while( !(linedata[char_itr] == ',' || linedata[char_itr] == '\n') ){
	      
	      buffer[buf_itr] = linedata[char_itr];
	      char_itr++;
	      buf_itr++;
	    }
	    
	    buffer[buf_itr] = '\0';
	    char_itr++;
	    buf_itr = 0;
	    
	    ICindex[i] = atoi(buffer) - 1;
	    
	    FeatSizes[i] = States[i].Size;
	  }

	  init_condition = sub2ind(nStates,FeatSizes,ICindex);

	   
	    
	  /*
	  printf("%c",linedata[char_itr]);
	      while( linedata[char_itr] != '\n'){
		buffer[buf_itr] = linedata[char_itr];
		char_itr++;
		buf_itr++;
	    }

	    buffer[buf_itr] = '\0';
	    

	    init_condition = atoi(buffer);
	  */
#ifdef DEBUG
	    printf("IC = %d\n",init_condition);
#endif
	    
	    
	}

	if(state == 9){

	    char_itr = 0;
	    buf_itr  = 0;

	    while( linedata[char_itr] != '\n'){
		buffer[buf_itr] = linedata[char_itr];
		char_itr++;
		buf_itr++;
	    }

	    buffer[buf_itr] = '\0';

	    disc_fac = atof(buffer);

	    state = 10;

	}



       	
    }
    // printf("Configured state action formulation\n");
    
}


#endif
