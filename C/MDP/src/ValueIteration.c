
#define EXTERN extern
#include "../include/MDP.h"
#include "../include/MDP_globals.h"

/** Tolerance define for determining convergence */
#define TOL 1e-5 

void ValueIteration(){

    int convg = 0;

    while(!convg){

	/* Run the bellman update on each action */
	for(int i=0;i<Size_TA;i++){
	    
	    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,disc_fac,Tensor[i].P,V,0.0,Values[i].V); 
	    
	    gsl_matrix_add(Values[i].V,Rewards[i].R);
	}

	/* Get max value */
	get_max_V();

	convg = ChkCvg(V,V_old);

    }
    
}

void get_max_V(){

    for(int i=0;i<Size_TS;i++){
	for(int j=0;j<Size_TA;j++){

	    if(j==0){
		V->data[i]      = Values[j].V->data[i];
		Policy->data[i] = j;
	    }
	    else{
		if( Values[j].V->data[i] > V->data[i] ){
		    V->data[i]      = Values[j].V->data[i];
		    Policy->data[i] = j;
		}
	    }
	}
    }
}

unsigned char ChkCvg(gsl_matrix* V1,gsl_matrix* V2){
    
    unsigned char check = 1;

    memcpy(V_temp->data,V1->data,sizeof(double)*Size_TS); 

    gsl_matrix_sub(V_temp,V2);
    gsl_vector_view column = gsl_matrix_column (V_temp, 0);
    
    double d;

    d = gsl_blas_dnrm2 (&column.vector);

    printf("||V_old - V|| = %f\n",d);

    if(d < TOL){
      return 1;
    }
    else{
      memcpy(V2->data,V1->data,sizeof(double)*Size_TS);
      return 0;
    }

}
