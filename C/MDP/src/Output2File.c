#define EXTERN extern
#include "../include/MDP.h"
#include "../include/MDP_globals.h"

void Output2File(){

    int P_i;

    int F1,F2;

    FILE * fp1;
    FILE * fp2;

    if(solver==1)
	fp1 = fopen("output_VI.txt","w");
    if(solver==2)
	fp1 = fopen("output_PI.txt","w");

    fp2 = fopen("policy.txt","w");

    fprintf(fp1,"%3c|",'#');

    for(int i=0;i<nStates;i++)
	fprintf(fp1,"%8s|",States[i].FeatureName);

    fprintf(fp1,"%8s|%8s\n","Value","Policy");

    int *F_Indices = (int *)calloc(Size_TS,sizeof(int));
    
    for(int i=0;i<Size_TS;i++){
	
	ind2sub(i,F_Size,nStates,F_Indices);

	fprintf(fp1,"%3d|",i);
	for(int j=0;j<nStates;j++){   
	    fprintf(fp1,"%8s|",States[j].FeatureValName[F_Indices[j]]);
	}

	//Policy is stored as the index of "StatexAction" space.                                                       
	P_i = (int)Policy->data[i];

	fprintf(fp1,"%8.1f|%8s\n",V->data[i],Actions->FeatureValName[P_i]);
	fprintf(fp2,"%d\n",P_i);
    }

    fclose(fp2);



}
