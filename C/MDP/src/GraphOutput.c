/**
 * @file    GraphOutput.c
 * @details This file contains all the functions required to output to
 *          a .gv file. 
 */

#define EXTERN extern

#include "../include/MDP.h"
#include "../include/MDP_globals.h"

// Node that will be the head of the visited list */
struct NODE *Visit_list = NULL;	

//Variable to compute a normalizer for total probabilites of terminal nodes
double Normalizer=0;	        

FILE* fp2;

//Function that finds the traces
void TraceFinder(int StartIndex) {

    int policy_ind;
    int B;

    int F1,F2;

    if(solver==1)
	fp2 = fopen("Traces_VI.gv","w");
    if(solver==2)
	fp2 = fopen("Traces_PI.gv","w");

    fprintf(fp2,"digraph{\n");			

    NODE *Head;	        //Node that will always hold the next state to visit.
    NODE *Traces;   	//A list of all the states that are reachable from the Head, in the next time step. 
    NODE *vptr;	        //Temporary pointer for visit list							

    Traces       = new NODE;
    Traces->next = NULL;
	
    Head         = new NODE;
    Head->index  = StartIndex;    //Storing the start state in the Head node.
    Head->prob   = 1;	          //Initializing a probability of 1 for the head
    Head->next   = NULL;

    //Explore all the states that can be reached from Head in the next time step and printing to file

    while( Head != NULL ) {							
	Expand(Head,&Traces);	        //Expand the head node and get a pointer to the children queue
	GetNextHead(&Head,&Traces);     //Get the next head and avoid reflexive transitions
    }


    //Printing the total probabilities of reaching terminal nodes and cleaning up the visit list

    while(Visit_list != NULL){
	vptr       = Visit_list;
	if(vptr->terminal==1){
	    B = vptr->index;
	    printf("state %d = %f\n",B,vptr->prob/Normalizer);
		
	    ind2sub(B,F_Size,nStates,F_Indices);

	    fprintf(fp2,"%d [fontcolor=blue,label=\"",B);
	    for(int i=0;i<nStates-1;i++){
		    
		fprintf(fp2,"%s,",States[i].FeatureValName[F_Indices[i]]);
		    
	    }
	    fprintf(fp2,"%s\n%6.3f\n%1.3f\"]\n",States[nStates-1].FeatureValName[F_Indices[nStates-1]],
		    V->data[B],vptr->prob/Normalizer);
	    fprintf(fp2," ");
	}
	Visit_list = Visit_list->next;
	delete vptr;
    }
	
    fprintf(fp2,"}");	
	
}

void Expand(NODE *h,NODE **c){
	
	int policy_ind,next_policy_ind,i;
	double prob;
	NODE *v,*vptr,*cptr;

	v = h;
	
	//Place the head in the visit list
	if(Visit_list == NULL){
	    Visit_list = new NODE;
	    Visit_list->index= v->index;
	    Visit_list->next = NULL;;
	}
	else{
	    vptr = Visit_list;
	    while(Visit_list->next != NULL) {
		Visit_list = Visit_list->next;
	    }
	    Visit_list->next      = new NODE;
	    Visit_list            = Visit_list->next;  
	    *(Visit_list)         = (*v);
	    Visit_list->next      = NULL;
	    Visit_list = vptr;
	}
	
	i = h->index;
	
	policy_ind = (int)Policy->data[i];		//Get the Policy corresponding to the Head state.
	
	cptr = *c;
	
	for(int j=0;j<Size_TS;j++){
	    
	    prob = gsl_matrix_get(Tensor[policy_ind].P,i,j);
	    
	    if(prob>0){	               //Check for all the states to which a transition from Head is probable.
		while(cptr->next!=NULL){
		    cptr= cptr->next;
		}
		
		(cptr) ->index = j;		//Add the new transition.
		(cptr) ->prob  = h->prob*prob;
		
		next_policy_ind = (int)Policy->data[j];
		
		//Checking if this child is a terminal node by looking for a self transition probability of 1.
		if(gsl_matrix_get(Tensor[next_policy_ind].P,j,j) == 1){ 
		    cptr->terminal = 1;
		    if(h->index != cptr->index){
			//Computing normalizer to normalize the total probability to add up to one.
			Normalizer = Normalizer + cptr->prob;		
		    }
		}
		else{
		    cptr->terminal = 0;
		}
		
		(cptr) ->next  = new NODE;
		(cptr)		   = (cptr)->next;
		cptr->next     = NULL;
		
		//printf("%d->%d\n",i,j);
		//Print traces to file
		PrintTraces(i,j,policy_ind,prob);				
	    }
	}
}
void GetNextHead(NODE** h,NODE** c){
    
    int visited = 1;
    NODE *hptr,*v,*cptr;
    
    v    = Visit_list;
    
    //Check for heads already visited
    while(visited){ 
	if((*c)->next!=NULL){
	    hptr = (*h);

	    // Get the next head from the children queue
	    (*h) = (*c);									
	    
	    // Get the next child
	    (*c) = (*c)->next;
	    
	    while(v){
		if((*h)->index == v->index){
		    visited = 1;

		    // Trying to avoid reflexive transitions
		    // Add the terminal probabilites - Total probability of reaching the terminal node
		    if( ((hptr->index) != (*h)->index)      
			&& ((*h)->terminal == 1)){         
			v->prob = v->prob + (*h)->prob;
		    }
		    v = Visit_list;
		    break;
		}
		else{
		    visited = 0;
		    v = v-> next;
		}
	    }
	    
	    //Delete the old head
	    delete hptr; 
	}
	else{
	    hptr = (*h);
	    (*h) = NULL;
	    delete hptr;
	    visited = 0;
	}
    }
}	

//Printing the traces to a file according to the DOT language script

void PrintTraces(int A,int B,int k,double prob){
    
    int F1,F2;
    
    ind2sub(A,F_Size,nStates,F_Indices);
    
    //Printing the nodes and edges, labeling the edges 
    if(k==1){
	fprintf(fp2,"%d->%d",A,B);
	fprintf(fp2,"[color=\"red\",label=\"");
	fprintf(fp2,"%s\\n%1.3f\"];\n",Actions->FeatureValName[k],prob);
    }
    else {
	fprintf(fp2,"%d->%d",A,B);
	fprintf(fp2,"[label=\"");
	fprintf(fp2,"%s\\n%1.3f\"];\n",Actions->FeatureValName[k],prob);
    }
    
    //Labeling the "From" nodes
    fprintf(fp2,"%d [label=\"",A);
    for(int i=0;i<nStates-1;i++){
	
	fprintf(fp2,"%s,",States[i].FeatureValName[F_Indices[i]]);
	
    }
    
    fprintf(fp2,"%s\\n%6.3f\"];\n",States[nStates-1].FeatureValName[F_Indices[nStates-1]],V->data[A]);
	
    //Labeling the "To" nodes
    
    ind2sub(B,F_Size,nStates,F_Indices);

    fprintf(fp2,"%d [label=\"",B);
    for(int i=0;i<nStates-1;i++){
	
	fprintf(fp2,"%s,",States[i].FeatureValName[F_Indices[i]]);
	
    }
    
    fprintf(fp2,"%s\\n%6.3f\"];\n",States[nStates-1].FeatureValName[F_Indices[nStates-1]],V->data[B]);
    
    
}
