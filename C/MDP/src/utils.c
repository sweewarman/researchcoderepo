#ifndef UTILS_H
#define UTILS_H


#define EXTERN extern
#include "../include/MDP.h"
#include "../include/MDP_globals.h"

#define MIN_VAL -1e10

int Init_MDP(){

    int temp;
	
    Size_TS = 1;
    Size_TA = 1;
    Size_VS = 1;

    /* Snapshot of current features and actions 
        Snapshot can be different from F_A_Indices */
    SnapShot_FA  = (double*) calloc((nStates + nActions),sizeof(double));

    /* Snapshot of current features */
    SnapShot_F   = (double*) calloc(nStates,sizeof(double));

    /* Size of each individual state and action feature */
    F_A_Size     = (int*) calloc((nStates + nActions),sizeof(int));
    F_Size       = (int*) calloc((nStates),sizeof(int));

    /* Indices corresponding to each State and Action*/
    F_A_Indices  = (int*) calloc((nStates + nActions),sizeof(int));
    F_Indices    = (int*) calloc(nStates,sizeof(int));
    A_Indices    = (int*) calloc(nActions,sizeof(int));

    /* Computing the size of the total state space
	Storing the size of each feature   - required for loop automation */
    for(int i=0;i<nStates;i++) {
	Size_TS        = Size_TS*States[i].Size;
	F_A_Size[i]    = States[i].Size;
	F_Size[i]      = States[i].Size;
    }

    /* Computing the size of the total action space
        Storing the size of each action    - required for loop automation */
    for(int i=0;i<nActions;i++) {
	Size_TA                = Size_TA*Actions[i].Size;
	F_A_Size[i+nStates]    = Actions[i].Size;
    }

    /* Computing the size of "state x action" space */
    Size_VS = Size_TA*Size_TS;

    /* Allocating memory for the VALUEs (memory is equal to the size of the state space.
	Actions not included here!) */
    VALUE      = (double*) calloc(Size_TS,sizeof(double)); 
    VALUE_OLD  = (double*) calloc(Size_TS,sizeof(double)); 
    POLICY     = (double*) calloc(Size_TS,sizeof(double)); 
    POLICY_OLD = (double*) calloc(Size_TS,sizeof(double)); 

    /* Initializing the VALUEs */
    for(unsigned int i=0;i<Size_TS;i++) {
	VALUE[i]      = 0;
	VALUE_OLD[i]  = 0;
	POLICY[i]     = 0;
	POLICY_OLD[i] = 0;
    }

    /* Allocating memory for the total "state x action" space
        Holds VALUES for each combination of states and actions.  */
    ValueSpace = (double*) calloc(Size_VS,sizeof(double));
   
    /* GSL matrices */
    V      = gsl_matrix_calloc(Size_TS,1);
    V_old  = gsl_matrix_calloc(Size_TS,1);
    V_temp = gsl_matrix_calloc(Size_TS,1);
    Policy = gsl_matrix_calloc(Size_TS,1); 

    Tensor  = (tensor_t *)calloc(Size_TA,sizeof(tensor_t));
    Rewards = (rewards_t *)calloc(Size_TA,sizeof(rewards_t)); 
    Values  = (value_t *)calloc(Size_TA,sizeof(value_t));

    for(int i=0;i<Size_TA;i++){
	Tensor[i].P  = gsl_matrix_calloc(Size_TS,Size_TS);
	Rewards[i].R = gsl_matrix_calloc(Size_TS,1);
	Values[i].V  = gsl_matrix_calloc(Size_TS,1);
    }

    return EXIT_SUCCESS;
     
}

int Init_Tensor(){

    FILE *fp;

    char filename[25];

    double val;
    
    for(int i=0;i<Size_TA;i++){

	/* File name */
	sprintf(filename,"Tensor%02d.txt",i+1);
  
	fp = fopen(filename,"r"); 
	
	if(fp == NULL){
	    printf("Unable to open file %s\n",filename);
	    return EXIT_FAILURE;
	}

	double *Matrix = (double*) calloc((Size_TS*Size_TS),sizeof(double));
	
	/* Read values from file */
	for(int j=0;j<(Size_TS*Size_TS);j++){
	    fscanf(fp,"%lf",&val);
	    Matrix[j] = val;
	}
	
	/* store the read values in the gsl matrix */
	memcpy(Tensor[i].P->data,Matrix,(Size_TS*Size_TS)*sizeof(double));

	fclose(fp);
	free(Matrix);
    }

    return EXIT_SUCCESS;

}

int Init_Rewards(){

    FILE *fp;

    char filename[25];

    double val;
    
    for(int i=0;i<Size_TA;i++){

	/* File name */
	sprintf(filename,"Rewards%02d.txt",i+1);
  
	fp = fopen(filename,"r"); 
	
	if(fp == NULL){
	    printf("Unable to open file %s\n",filename);
	    return EXIT_FAILURE;
	}

	double *Matrix = (double*) calloc((Size_TS),sizeof(double));

	/* Read values from the file */
	for(int j=0;j<Size_TS;j++){
	    fscanf(fp,"%lf",&val);
	    Matrix[j] = val;
	}
	
	/* store values in the gsl matrix */
	memcpy(Rewards[i].R->data,Matrix,sizeof(double)*Size_TS);

	fclose(fp);
	free(Matrix);
    }

    return EXIT_SUCCESS;

}


int ind2sub(int index,int* subsizes,int size,int *subscripts){

    size = size - 1;
    while(size>=0){
	subscripts[size] = index % subsizes[size];
	index            = index / subsizes[size];
	size             = size - 1;
    }

}

int sub2ind(int nFeatures,int* F_size,int* subscripts){
  
  int i=0;
  int index=0;
  int j;
  int product;
  while(i<nFeatures) {
    j = i + 1;
    product = 1;
    while(j<nFeatures) {
      product = product*F_size[j];
      j       = j + 1;
    }
    index = index + subscripts[i]*product;
    i     = i + 1;
  }
  
  return index; 
}



void Dealloc(){

    free(SnapShot_FA);
    free(SnapShot_F);
    free(F_A_Size);
    free(F_Size);
    free(F_A_Indices);
    free(F_Indices);
    free(A_Indices);
    free(VALUE);
    free(VALUE_OLD);
    free(POLICY_OLD);
    free(POLICY);
    free(ValueSpace);
    gsl_matrix_free(V);
    gsl_matrix_free(V_old);
    gsl_matrix_free(V_temp);
    gsl_matrix_free(Policy);
    
    for(int i=0;i<Size_TA;i++){
	gsl_matrix_free(Tensor[i].P);
	gsl_matrix_free(Rewards[i].R);
	gsl_matrix_free(Values[i].V);
    }
    
    free(Tensor);
    free(Rewards);
    free(Values);


}

#endif
