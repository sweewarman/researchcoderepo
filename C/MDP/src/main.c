/**
 * @mainpage  MARKOV DECISION PROCESS SOLVER
 * @author    Sweewarman Balachandran
 *            
 * @details   This is a program to solve an MDP. This code base depends on the GSL libraries.
 *            The configuration file is used to setup the MDP to be solved instead of 
 *            having to modify the source program everytime. 
 *            The configuration file is provided as a command line argument when running the binary. 
 *            (e.g: ./solveMDP config.txt )
 *            The configuration file defines the following parameters that
 *            are needed by this program. (NOTE: The order of these parameters should not be changed)
 *            1. Number of state features \n
 *            2. Number of action features \n
 *            3. id,name,size of state feature \n
 *            4. id,name,size of action feature \n
 *            5. Label of individual state features \n
 *            6. Label of individual action features \n
 *            7. Algorithm used
 *            8. Initial condition
 *            9. Discount factor
 *            
 * @date      12-22-2014
 */

#include <string.h>

/** 
    Include this define in main.c before including MDP_globals.h 
    and then include the define EXTERN extern in other .c files before including
    MDP_globals.h
 */
#define EXTERN 
#include "../include/MDP.h"
#include "../include/MDP_globals.h"


int main(int argc,char **argv){

    char filename[100];

    if(argc < 2){
	printf("\nNo configiruation file specified. \n");
	printf("......Exiting Program......\n");
	return 0;
    }
    else{
	strcpy(filename,argv[1]);
    }

    FILE* fp;
    double **Tensors;
    
    /* Parse configuration file to setup MDP */
    ParseConfig(filename);

    /* Initialize MDP and auxiliary variables */
    Init_MDP();

    /* Initialize the MDP tensors */
    Init_Tensor();

    /* Initialize reward function for each state-action pair */
    Init_Rewards();


    if(solver == 1)
	ValueIteration();

    /* Output to file */
    Output2File();
    
    /* Compute policy for given initial condition */
    TraceFinder(init_condition);


    /* Deallocate memory */
    Dealloc();

    return 1;

}
