#include <stdio.h>
#include <Python.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_odeiv2.h>
#include <math.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

#define nACParams 11
#define nGearParams 16
#define nAeroParams 45
#define nCT_PARAMS 8
#define nRD_PARAMS 2
#define nFsmParams 13

#include "AircraftSim.c"

unsigned char ODE_SOLVER(double *solverparams, double *initConditions, 
			  double *params,double *X){

  double *y;
  double t     = 0;

  int dim   = (int)solverparams[0];
  double t1 = solverparams[1];
  double dt = solverparams[2];

  int N      = (int)t1/dt;
  int stride  = dim + 1;
  int stride2 = stride + 8;

  gsl_odeiv2_system sys   = {func, NULL, dim, params};

  gsl_odeiv2_driver * d = 
    gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd,
				   1e-6, 1e-6, 0.0);

  unsigned char stop_sim = 0;
  
  y = (double*) calloc(dim,sizeof(double));

  X[0] = t;
  for(int i=1;i<stride;i++){
    X[i] = initConditions[i-1];
    y[i-1] = initConditions[i-1];
  }

  fsam.state1 = 1;
  fsam.state2 = 1;

  for (int i = 1; i <= N; i++){
  
    double ti = i * t1 / N;
    int status = gsl_odeiv2_driver_apply (d, &t, ti, y);

    //Impose tail strike constraints
    if( (y[7] > ACParams[8]) && (y[11] < ACParams[9]) )
      y[7] = ACParams[8];
    
    if(y[0] < 0.001 )
      stop_sim = 1;
    else if(y[0] > fsam.Vfp)
      stop_sim = 1;
    else if(y[9]>ACParams[10])
      stop_sim = 1;
      
    if (status != GSL_SUCCESS){
      printf ("error, return value=%d\n", status);
      free(y);
      return 0;
    }
    
    if(stop_sim){
      gsl_odeiv2_driver_free (d);
      free(y);
      return i;
    }
    
    double u[4];
    unsigned char mode[2];

    // Recompute inputs to store in output variable
    ControlInput((double*)params,t,y,u,mode);

    X[i*stride2+0] = t;
    
    for (int j=1;j<stride;j++){
      X[i*stride2+j] = y[j-1];
    }

    for(int j = stride;j<stride+4;j++){
      X[i*stride2+j] = u[j-stride];
    }
    
    X[i*stride2+stride+4] = mode[0];
    X[i*stride2+stride+5] = mode[1];
    X[i*stride2+stride+6] = fsam.state1;
    X[i*stride2+stride+7] = fsam.state2;
  }
  
  free(y);

  gsl_odeiv2_driver_free (d);
  
  return N;
}


static PyObject* py_ode(PyObject* Self,PyObject* args){

  PyArrayObject* SolverParams;
  PyArrayObject* Params;
  PyArrayObject* InitConditions;
  PyArrayObject* out_array;

  double *solverparams, *params, *initConditions, *V;
  
  if(  !PyArg_ParseTuple(args,"OOOO",&SolverParams,&Params,
		       &InitConditions,&out_array)   ){
    return Py_BuildValue("i",-1);
  }

  solverparams   = (double*) PyArray_DATA(SolverParams);
  params         = (double*) PyArray_DATA(Params);
  initConditions = (double*) PyArray_DATA(InitConditions);
  V              = (double*) PyArray_DATA(out_array);

  int status     = 1; 
  
  status = (int)ODE_SOLVER(solverparams,initConditions,params,V);
      
  return Py_BuildValue("i",status);
  
}

static PyMethodDef AircraftC2PyODE[] = {
  {"py_ode", py_ode, METH_VARARGS,"Ode solver that can be called from python"},
  {NULL, NULL,0,NULL}
};

PyMODINIT_FUNC
initAircraftC2PyODE(void)
{
  (void) Py_InitModule("AircraftC2PyODE", AircraftC2PyODE);
  import_array();

  FILE *fp1;
  FILE *fp2;

  unsigned char status;

  double FSMparams[nFsmParams];

  fp1 = fopen("ACParams.txt","r");
  
  for(int i=0;i<nACParams;i++){
    status = fscanf(fp1,"%lf",ACParams+i);
  }
  
  for(int i=0;i<nGearParams;i++){
    status = fscanf(fp1,"%lf",GearParams+i);
  }

  for(int i=0;i<nAeroParams;i++){
    status = fscanf(fp1,"%lf",AeroParams+i);
  }

  fclose(fp1);

  fp2 = fopen("FSAMParams.txt","r");

  for(int i=0;i<nFsmParams;i++){
    status = fscanf(fp2,"%lf",FSMparams+i);
  }

  fclose(fp2);

  fsam.Vmcg = FSMparams[0];
  fsam.V1   = FSMparams[1];
  fsam.VR   = FSMparams[2];
  fsam.Vlof = FSMparams[3];
  fsam.V2   = FSMparams[4];
  fsam.Vfp  = FSMparams[5];

  fsam.p1   = FSMparams[6];
  fsam.p2   = FSMparams[7];
  fsam.h1   = FSMparams[8];
  fsam.y1   = FSMparams[9];
  fsam.y2   = FSMparams[9];
  fsam.d1   = FSMparams[10];
  fsam.d2   = FSMparams[11];
  
}
