#include "FSAM.c"

void MatrixProduct(double *Mat1, int m1, int n1,
		   double *Mat2, int m2, int n2,
		   double *Prod, unsigned char Trans1,unsigned char Trans2){

  
  if(n1 != m2){
    printf("MatVecProduct: incompatible Matrix and vector for product\n");
    return;
  }
  
  gsl_matrix * A = gsl_matrix_alloc (m1,n1);
  gsl_matrix * x = gsl_matrix_alloc (m2,n2);

  gsl_matrix * B  = gsl_matrix_alloc (m1,n2);
  
  memcpy(A->data,Mat1,sizeof(double)*m1*n1);
  memcpy(x->data,Mat2,sizeof(double)*m2*n2);

  if(Trans1 == 0 && Trans2 == 0)
    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,1.0, A, x,0.0, B);
  else if(Trans1 == 0 && Trans2 == 1)
    gsl_blas_dgemm (CblasNoTrans, CblasTrans,1.0, A, x,0.0, B);
  else if(Trans1 == 1 && Trans2 == 0)
    gsl_blas_dgemm (CblasTrans, CblasNoTrans,1.0, A, x,0.0, B);
  else
    gsl_blas_dgemm (CblasTrans, CblasTrans,1.0, A, x,0.0, B);

  memcpy(Prod,B->data,sizeof(double)*m1*n2);

  gsl_matrix_free(A);
  gsl_matrix_free(x);
  gsl_matrix_free(B);
  
  return;

}

/*  Update function required to compute inputs
    for monte carlo simulations */
void ControlInput(double *params,double t,const double *x,double *U,unsigned char *mode){

  double d2r        = 3.142/180;

  double Vr1        = params[0];
  double kpe1       = params[1];
  double kde1       = params[2];
  double lag        = params[3];
  double pitch_ref1 = params[4]*d2r;
  double Tmax1      = params[5];
  double kpr1       = params[6];
  double kdr1       = params[7];
  double kpy1       = params[8];
  double yaw_ref1   = kpy1 * x[10];

  double Vr2   = 56.032;
  double kpe2  = -3;
  double kde2  = 1;
  double kpr2  = -0.25;
  double kdr2  = 0.1;
  double kpy2  = -1;

  double pitchTS    = 9*d2r;
  double hTS        = 2.1;
  double h          = x[11];

  double pitch_ref0 = 6*d2r;
  double pitch_ref2 = 8*d2r;
  double yaw_ref2   = kpy2 * x[10];

  double Tmax2 = 250000;

  unsigned char mode1, mode2;

  double u[4] = {0.0,0.0,0.0,Tmax1};

  // Get mode here
  GetMode(x,u,&mode1,&mode2);

  mode[0] = mode1;
  mode[1] = mode2;

  if(mode1 == 1){

    // Pilot's Elevator input
    if(x[12] > Vr1)
      U[0]     = kpe1 * (pitch_ref1 - x[7]) + kde1*x[4];
    else
      U[0]     = kpe1 * (0 - x[7]) + kde1*x[4];

    // Aileron input
    U[1] = 0;

    // Rudder input
    U[2] = kpr1 * (yaw_ref1 - x[8]) + kdr1*x[5];

    // Throttle input
    U[3] = Tmax1;
    
  }
  else{

    // Elevator input
    if(x[12] > Vr2)
      if( (pitchTS - x[7]) < 0 && (hTS-h) > 0 )
	U[0]     = kpe2 * (pitch_ref0 - x[7]) + kde2*x[4];
      else
	U[0]     = kpe2 * (pitch_ref2 - x[7]) + kde2*x[4];
    else
      U[0]     = kpe2 * (0 - x[7]) + kde2*x[4];

    // Aileron input
    U[1] = 0;

    // Rudder input
    U[2] = kpr2 * (yaw_ref2 - x[8]) + kdr2*x[5];

    // Throttle input
    U[3] = Tmax2;

  }


  // Impose saturation limits
  if(U[0] >= 30 *d2r)
    U[0] = 30*d2r;
  else if(U[0] <= -30*d2r)
    U[0] = -30*d2r;


  if(U[2] >= 30 *d2r)
    U[2] = 30*d2r;
  else if(U[2] <= -30*d2r)
    U[2] = -30*d2r;


  return;
}




