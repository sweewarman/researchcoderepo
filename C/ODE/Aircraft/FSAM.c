struct FSM{
  unsigned char state1;
  unsigned char state2;

  double Vmcg;
  double V1;
  double VR;
  double Vlof;
  double V2;
  double Vfp;

  double p1;
  double p2;
  double h1;
  double y1;
  double y2;
  double d1;
  double d2;
};
typedef struct FSM fsm_t;

fsm_t fsam;

void GetMode(const double *X,const double *U,
	     unsigned char *mode1,unsigned char *mode2){
  /*
        FSAM state machine for takeoff
        Contains longitudinal and lateral takeoff state machines.
        state1 - current state for longitudinal machine
        state2 - current state for lateral machine
        X      - aircraft state vector
        U      - Control input vector
        
        output 
        state1,state2 - current states
        mode1,mode2   - current mode (1 - pilot, 2 - autopilot)
  */
       
  double r2d = 180/3.142;

  double v = X[12];            // airspeed
  double p = X[7]*r2d;         // pitch
  double d = X[8]*r2d;         // heading
  double x = X[9];             // x-position
  double y = fabs(X[10]);      // y-posiiion
  double h = fabs(X[11]);      // Altitude
  double T = U[3];             // Throttle
  
  // Rest
  if(fsam.state1 == 1){
    *mode1 = 1;
    if (v > 0)
      fsam.state1 = 2;
  }
  // 0 < V < Vmcg
  else if(fsam.state1 == 2){
    *mode1 = 1;
    if (v > fsam.Vmcg)
      fsam.state1 = 3;
    if (T < 100)
      fsam.state1 = 14;
  }
  // Vmcg <= V <= V1
  else if(fsam.state1 == 3){
    *mode1 = 1;
    if (v > fsam.V1)
      fsam.state1 = 4;
    if (T < 100)
      fsam.state1 = 14;
  }
  // V1 <= V < VR
  else if(fsam.state1 == 4){
    *mode1 = 1;
    if (v > fsam.VR)
      fsam.state1 = 5;

    if (p > fsam.p1)
      fsam.state1 = 9;
  }
  // VR <= V < Vlof
  else if(fsam.state1 == 5){
    *mode1 = 1;
            
    if ( (p > fsam.p2) && (h < fsam.h1) )
      fsam.state1 = 10;
    else if (v>fsam.Vlof)
      fsam.state1 = 6;
  }
  // Vlof <= V < V2
  else if(fsam.state1 == 6){
    *mode1 = 1;
            
    if ( (p > fsam.p2) && (h < fsam.h1) )
      fsam.state1 = 6; //fsam.state1 = 11
    else if (v>fsam.V2)
      fsam.state1 = 7;
  }
  // V2 <= V < Vfp
  else if(fsam.state1 == 7){
    *mode1 = 1;
    if ( (p > fsam.p2) && (h < fsam.h1))
      fsam.state1 = 7; //fsam.state1 = 7
    else if (v > fsam.Vfp)
      fsam.state1 = 7;
  }
  else if(fsam.state1 == 9){
    *mode1 = 2;
        
    if (v > fsam.VR)
      fsam.state1 = 5;
  }
  else if(fsam.state1 == 10){
      *mode1 = 2;

     if(v > fsam.Vlof)
       fsam.state1 = 6;
     //if( (p > fsam.p2) && (h < fsam.h1))
     //fsam.state1 = 11;         
    
  }
  else if(fsam.state1 == 11){
    *mode1 = 2;
	      
    if(h > fsam.h1)
      fsam.state1 = 6;
      
    else if((p < fsam.p2) && (h < fsam.h1))
      fsam.state1 = 6;

    else if (v  > fsam.V2)
      fsam.state1 = 12;
  }
  else if(fsam.state1 == 12){
    *mode1 = 2;
    if (h > fsam.h1)
      fsam.state1 = 7;
  }
    
  //=================================================================
  // Lateral state machine
        
  // Rest
  if(fsam.state2 == 1){
    *mode2 = 1;
    if (v > 0)
      fsam.state2 = 2;
  }
  // 0 < V < Vmcg
  else if(fsam.state2 == 2){
    *mode2 = 1;

    if (v > fsam.Vmcg)
      fsam.state2 = 3;

    if (y > fsam.y1)
      fsam.state2 = 8;
  }
  // Vmcg <= V <= V1
  else if(fsam.state2 == 3){
    *mode2 = 1;
            
    if (v > fsam.V1)
      fsam.state2 = 4;

    if (y > fsam.y1)
      fsam.state2 = 9;
  }
  // V1 <= V < VR
  else if(fsam.state2 == 4){
    *mode2 = 1;
            
    if (v > fsam.VR)
      fsam.state2 = 5;

    if (y > fsam.y1)
      fsam.state2 = 10;
  }
  // VR <= V < Vlof
  else if(fsam.state2 == 5){
    *mode2 = 1;

    if (v>fsam.Vlof)
      fsam.state2 = 6;
    
    if (y > fsam.y1)
      fsam.state2 = 11;
  }
  // Vlof <= V < V2
  else if(fsam.state2 == 6){
    *mode2 = 1;

    if (v>fsam.V2)
      fsam.state2 = 7;

    if (y > fsam.y1)
      fsam.state2 = 12;
  }
  //V2 <= V < Vfp
  else if(fsam.state2 == 7){
    *mode2 = 1;
    
    if (v > fsam.Vfp)
      fsam.state2 = 7;

    if (y > fsam.y1)
      fsam.state2 = 13;
  }
  else if(fsam.state2 == 8){
    *mode2 = 2;

    if (v > fsam.Vmcg)
      fsam.state2 = 9;
  }
  else if(fsam.state2 == 9){
    *mode2 = 2;
        
    if (v > fsam.V1)
      fsam.state2 = 10;
  }
  else if(fsam.state2 == 10){
    *mode2 = 2;

    if (v > fsam.VR)
      fsam.state2 = 11;
  }
  else if(fsam.state2 == 11){
    *mode2 = 2;
        
    if (v > fsam.Vlof)
      fsam.state2 = 12;
  }
  else if(fsam.state2 == 12){
    *mode2 = 2;
        
    //if y < fsam.y1:
    //fsam.state2 = 6

    if (v > fsam.V2)
      fsam.state2 = 13;
  }
  else if(fsam.state2 == 13){
    *mode2 = 2;

    //if y < fsam.y1:
    //fsam.state2 = 7
  }

  return;
}
