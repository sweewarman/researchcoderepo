#include "Controller.c"

double ACParams[nACParams];
double GearParams[nGearParams];
double AeroParams[nAeroParams];


void GenCoeff(double alpha, double beta, double pt, double qt, double rt, 
	      double del_e, double del_a,double del_r,double *C){

  double C_Dv[10]  = {1, alpha,    alpha*qt,    alpha*del_e,  
		      pow(alpha,2), pow(alpha,2)*qt, pow(alpha,2)*del_e,
                      pow(alpha,3), pow(alpha,3)*qt, pow(alpha,4)};

  double C_Yv[5]   = {beta, pt, rt, del_a, del_r};

  double C_Lv[8]   = {1, alpha, qt, del_e, alpha*qt, pow(alpha,2), pow(alpha,3), pow(alpha,4)};

  double C_lv[5] = {beta, pt, rt, del_a, del_r};

  double C_mv[10] = {1, alpha, qt, del_e, alpha*qt, pow(alpha,2)*qt, pow(alpha,2)*del_e,
		     pow(alpha,3)*qt, pow(alpha,3)*del_e, pow(alpha,4)};

  double C_nv[7] = {beta, pt, rt, del_a, del_r, pow(beta,2), pow(beta,3)};

  double C_D,C_Y,C_L,C_l,C_m,C_n,C_E,C_X,C_Z;


  double T1[10],T2[5],T3[8],T4[5],T5[10],T6[7];

  // add code to copy aero coefficieints into T1....T6
  memcpy(T1,AeroParams,sizeof(double)*10);
  memcpy(T2,AeroParams+10,sizeof(double)*5);
  memcpy(T3,AeroParams+(10+5),sizeof(double)*8);
  memcpy(T4,AeroParams+(10+5+8),sizeof(double)*5);
  memcpy(T5,AeroParams+(10+5+8+5),sizeof(double)*10);
  memcpy(T6,AeroParams+(10+5+8+5+10),sizeof(double)*7);

  MatrixProduct(C_Dv,1,10,T1,10,1,&C_D,0,0);
  MatrixProduct(C_Yv,1,5,T2,5,1,&C_Y,0,0);
  MatrixProduct(C_Lv,1,8,T3,8,1,&C_L,0,0);
  MatrixProduct(C_lv,1,5,T4,5,1,&C_l,0,0);
  MatrixProduct(C_mv,1,10,T5,10,1,&C_m,0,0);
  MatrixProduct(C_nv,1,7,T6,7,1,&C_n,0,0);
  
  C_E = (C_Y + sin(beta)*C_D)/-cos(beta);
  C_X = -cos(alpha)*cos(beta)*C_D+cos(alpha)*sin(beta)*C_E+sin(alpha)*C_L;
  C_Z = -sin(alpha)*cos(beta)*C_D+sin(alpha)*sin(beta)*C_E-cos(alpha)*C_L;
   
  C[0] = C_D;
  C[1] = C_E;
  C[2] = C_L;
  C[3] = C_l;
  C[4] = C_m;
  C[5] = C_n;
  C[6] = C_X;
  C[7] = C_Y;
  C[8] = C_Z;
  
}

void GearFM(const double *x, double *u, unsigned char LG,double *Gear){

  // Return without computing gear forces if LG == 0
  if(!LG){
    memset(Gear,0,sizeof(double)*6);
    return;
  }

  double U      = x[0];
  double V      = x[1];
  double W      = x[2];
  double pb     = x[3];
  double qb     = x[4];
  double rb     = x[5];
  double roll   = x[6];
  double pitch  = x[7];
  double yaw    = x[8];

  double Ze     = -x[11];

  double DCM[9] = { cos(pitch)*cos(yaw), 
		    sin(roll)*sin(pitch)*cos(yaw) - cos(roll)*sin(yaw),
		    cos(roll)*sin(pitch)*cos(yaw) + sin(roll)*sin(yaw),
		    cos(pitch)*sin(yaw),
		    sin(roll)*sin(pitch)*sin(yaw) + cos(roll)*cos(yaw),
		    cos(roll)*sin(pitch)*sin(yaw) - sin(roll)*cos(yaw),
		    -sin(pitch),   sin(roll)*cos(pitch),   cos(roll)*cos(pitch) }; 
                      

  double Ub_vec[3] = {U,V,W};
  double Ve[3];

  MatrixProduct(DCM,3,3,Ub_vec,3,1,Ve,0,0);

  double Vac    = Ve[2];
  
  double steer  = 0;
  
  double lxL = GearParams[0], lyL = GearParams[1], lzL = GearParams[2];
  double lxR = GearParams[3], lyR = GearParams[4], lzR = GearParams[5];
  double lxN = GearParams[6], lyN = GearParams[7], lzN = GearParams[8]; 

  double k_L = GearParams[9], k_R = GearParams[10], k_N = GearParams[11]; 
  double c_L = GearParams[12] ,c_R = GearParams[13] ,c_N = GearParams[14];
      
  // Oleo locations in the body frame
  double rposL[3]  = { lxL, lyL, 0 };
  double rposR[3]  = { lxR, lyR, 0 };
  double rposN[3]  = { lxN, lyN, 0 };
    
  // Oleo locations in the inertial frame
  double oleoL[3]; 
  double oleoR[3]; 
  double oleoN[3]; 

  MatrixProduct(DCM,3,3,rposL,3,1,oleoL,0,0);
  MatrixProduct(DCM,3,3,rposR,3,1,oleoR,0,0);
  MatrixProduct(DCM,3,3,rposN,3,1,oleoN,0,0);

  // Deflection rate of oleos in inertial frame due to attitude changes (using transport theorem)
  double  w_cross[9]  = {0,-rb, qb,rb,0,-pb,-qb,pb,0};
  double  hcat[9]     = {rposL[0],rposR[0],rposN[0],
			 rposL[1],rposR[1],rposN[1],
			 rposL[2],rposR[2],rposN[2]};

  
  double  oleodotB[9],oleodot[9];
  MatrixProduct(w_cross,3,3,hcat,3,3,oleodotB,0,0);
  MatrixProduct(DCM,3,3,oleodotB,3,3,oleodot,0,0); 

  // Total deflection of oleos - z direction
  double oleoLz   = oleoL[2] + Ze;
  double oleoRz   = oleoR[2] + Ze;
  double oleoNz   = oleoN[2] + Ze;

  // Total deflection rate of oleos 
  double  oleoLdot = oleodot[2*3+0]  + Vac;
  double  oleoRdot = oleodot[2*3+1]  + Vac;
  double  oleoNdot = oleodot[2*3+2]  + Vac;

  // Normal load on the gears
  double Fsn = -k_N * oleoNz;
  double Fdn = -c_N * oleoNdot;

  double Fsl = -k_L * oleoLz;
  double Fdl = -c_L * oleoLdot;

  double Fsr = -k_R * oleoRz;
  double Fdr = -c_R * oleoRdot;

  double FzL = Fsl+Fdl; 
  double FzR = Fsr+Fdr;
  double FzN = Fsn+Fdn;

  

  if(FzL >= 0)
    FzL=0;
    
  if(FzR >= 0)
    FzR=0;
    
  if(FzN >= 0)
    FzN=0;

  // Coefficient of fricition related through the magic formula
  // Simplified by specifying a standard value for friction coeff
  if(u[3] < 1)
    GearParams[15] = 0.2;

  double muL   = GearParams[15];
  double muR   = GearParams[15];
  double muN   = GearParams[15];

  // Longitudinal loads on the wheels
  double FxL   = -muL*fabs(FzL); 
  double FxR   = -muR*fabs(FzR);
  double FxN   = -muN*fabs(FzN);

  double alpL,alpR,alpN;

  //Lateral wheel slip
  if(U > 0){
    alpL = atan(V/U);
    alpR = atan(V/U);
    alpN = -steer + atan(V/U);
  }
  else{
    alpL = 0;
    alpR = 0;
    alpN = 0;
  }


  double FymaxL  = -7.39e-7 * FzL*FzL + 5.11e-1*FzL;
  double FymaxR  = -7.39e-7 * FzR*FzR + 5.11e-1*FzR;
  double FymaxN  = -3.53e-6 * FzN*FzN + 8.33e-1*FzN;

  double  alpoptN = 3.52e-9 *FzN*FzN + 2.80e-5*FzN + 13.8;
  double  alpoptL = 1.34e-10*FzL*FzL + 1.06e-5*FzL + 6.72;
  double  alpoptR = 1.34e-10*FzR*FzR + 1.06e-5*FzR + 6.72;

  // Lateral loads on the wheels
  double FyL   = -(-2*(FymaxL * alpoptL*alpL)/(alpoptL*alpoptL + alpL*alpL));
  double FyR   = -(-2*(FymaxR * alpoptR*alpR)/(alpoptR*alpoptR + alpR*alpR));
  double FyN   = -(-2*(FymaxN * alpoptN*alpN)/(alpoptN*alpoptN + alpN*alpN));
    
  
  // Total reaction forces on the Aircraft exerted by ground in the Earth Frame
  double FX    = FxL + FxR + FxN*cos(steer) + FyN*sin(steer);
  double FY    = FyR + FyL + FyN*cos(steer) + FxN*sin(steer);
  double FZ    = FzL + FzR + FzN;

  // Converting FZ from earth to body frame
  
  double Fz_e[3] = {0,0,FZ};
  double Fz_b[3];
  MatrixProduct(DCM,3,3,Fz_e,3,1,Fz_b,1,0);

  // Adding the all the Force components in the Body Frame
  FX    = FX + Fz_b[0];
  FY    = FY + Fz_b[1];
  FZ    = Fz_b[2];

  double epos[9];
  MatrixProduct(DCM,3,3,hcat,3,3,epos,0,0);
                                                                          
  double lxLe = epos[0*3+0];
  double lxRe = epos[0*3+1];
  double lxNe = epos[0*3+2];
          
  //Total Moment on the Aircraft body frame
  double MX    = lyL*(FzL) + lyR*(FzR) + lzL*(-FyL) + lzL*(-FyR) + lzN*(-FyN)*cos(steer) + lzN*(-FxN)*sin(steer);
  double MY    = lxNe*(-FzN) + lxRe*(-FzR) + lxLe*(-FzL) + lzN*(FxN)*cos(steer) + lzN*(-FyN)*sin(steer) + lzR*FxR + lzL*FxL;
  double MZ    = lyR*(-FxR) + lyL*(-FxL) + lxRe*(FyR) + lxLe*(FyL) + lxNe*(FyN)*cos(steer) + lxNe*(FxN)*sin(steer);
            
  // Force and Moment on the aircraft due  to the undercarriage
  Gear[0] = FX;
  Gear[1] = FY;
  Gear[2] = FZ;
  Gear[3] = MX;
  Gear[4] = MY;
  Gear[5] = MZ;

}


int
func (double t, const double x[], double dx[],
      void *Param)
{

  double U     = x[0];  
  double V     = x[1];  
  double W     = x[2];  
  double P     = x[3];  
  double Q     = x[4];   
  double R     = x[5];  
  
  double phi   = x[6];  
  double theta = x[7];   //  pitch
  double psi   = x[8];   //  yaw
  
  double Xe    = x[9];   //  x position
  double Ye    = x[10];  //  y position
  double h     = x[11];  //  Altitude
  
  double g     = 9.8;     //  Acceleration due to gravity in 
   
  double u[4]  = {0.0,0.0,0.0,0.0};
  unsigned char LG = 0;
 
  unsigned char mode[2];

  LG = 1;

  double WindX = 0;
  double WindY = 0;
  double WindZ = 0;

  ControlInput((double*)Param,t,x,u,mode);

  double del_e = u[0];   //  elevator
  double del_a = u[1];   //  aileron
  double del_r = u[2];   //  rudder
  double del_T = u[3];   //  throttle

  double S     = ACParams[0];
  double M     = ACParams[1];
  double c     = ACParams[2];
  double b     = ACParams[3];
 
  double Ixx   = ACParams[4];
  double Iyy   = ACParams[5];
  double Izz   = ACParams[6];
  double Ixz   = ACParams[7];
   
  // Moment of inertial based coefficients
  double c1   = ((Iyy-Izz)*Izz-Ixz*Ixz)/(Ixx*Izz-Ixz*Ixz);
  double c2   = (Ixx-Iyy+Izz)*Ixz/(Ixx*Izz-Ixz*Ixz);
  double c3   = Izz/(Ixx*Izz-Ixz*Ixz);
  double c4   = Ixz/(Ixx*Izz-Ixz*Ixz);
  double c5   = (Izz-Ixx)/Iyy;
  double c6   = Ixz/Iyy;
  double c7   = 1/Iyy;
  double c8   = ((Ixx-Iyy)*Ixx-Ixz*Ixz)/(Ixx*Izz-Ixz*Ixz);
  double c9   = Ixx/(Ixx*Izz-Ixz*Ixz);
  
  double heng  = 0;
  
  double Tstar = 1 - 0.703*pow(10,-5)*fabs(h) * 3.28084;
  double rho   = 0.002377*pow(Tstar,4.14) * 515.4 ;
  
  // True airspeed
  double U0    = sqrt(U*U + V*V + W*W);
    
  // Start from 1 if U = 0 (To prevent singularities)
  if(U == 0.0){
    U  = 1;
    U0 = 1;
  }
  	
  double alpha =0,beta = 0,q = 0, pt =0, qt =0, rt = 0;
    
  // Angle of attack
  alpha = atan(W/U);
    
  // Side slip angle
  beta  = asin(V/U0);
  
  // Dynamic pressure
  q     = 0.5*rho*U0*U0;
  
  pt    = b/(2*U0)*x[3];
  qt    = c/(2*U0)*x[4];
  rt    = b/(2*U0)*x[5];
  
  double C[9],Gear[6];

  GenCoeff(alpha,beta,pt,qt,rt,del_e,del_a,del_r,C);
    
  GearFM(x,u,LG,Gear);

  double udot      = R*V - Q*W - g*sin(theta)          + q*S*C[6]/M + del_T/M + Gear[0]/M;
  double vdot      = P*W - R*U + g*cos(theta)*sin(phi) + q*S*C[7]/M + Gear[1]/M;
  double wdot      = Q*U - P*V + g*cos(theta)*cos(phi) + q*S*C[8]/M + Gear[2]/M;
  
  double pdot      = (c1*R + c2*P + c4*heng)*Q + q*S*b*(c3*C[3] + c4*C[5]) + c3*Gear[3] + c4*Gear[5];
  double qdot      = (c5*P - c7*heng)*R        - c6*(P*P - R*R) + q*S*c*c7*C[4] + c7*Gear[4];
  double rdot      = (c8*P - c2*R + c9*heng)*Q + q*S*b*(c4*C[3] + c9*C[5]) + c4*Gear[3] + c9*Gear[5];
  
  double phidot    = P + tan(theta)*(Q*sin(phi) + R*cos(phi));
  double thetadot  = Q * cos(phi) - R*sin(phi);
  double psidot    = (Q*sin(phi) + R*cos(phi))/cos(theta);
  
  double xedot     = U*cos(psi)*cos(theta) +				 
    V*(cos(psi)*sin(theta)*sin(phi) - sin(psi)*cos(phi)) +	 
    W*(cos(psi)*sin(theta)*sin(phi) + sin(psi)*sin(phi)) - WindX;
    
  double yedot     = U*sin(psi)*cos(theta) +				 
    V*(sin(psi)*sin(theta)*sin(phi) + cos(psi)*cos(phi)) +	 
    W*(sin(psi)*sin(theta)*cos(phi) - cos(psi)*sin(phi)) - WindY;
    
  double hdot      = U*sin(theta) - V*cos(theta)*sin(phi) - W*cos(theta)*cos(phi) - WindZ;
  
  double U0dot     = (U*udot + V*vdot + W*wdot)/U0;
  
  double alphadot  = (U*wdot - W*udot)/ sqrt(U*U + W*W);
  
  double betadot   = (U0*vdot - V*U0dot)/(U0*U0 * sqrt( 1 - pow((V/U0),2) ) );
  
  dx[0]   = udot;
  dx[1]   = vdot;
  dx[2]   = wdot;
  dx[3]   = pdot;
  dx[4]   = qdot;
  dx[5]   = rdot;
  dx[6]   = phidot;
  dx[7]   = thetadot;
  dx[8]   = psidot;
  dx[9]   = xedot;
  dx[10]  = yedot;
  dx[11]  = hdot;
  dx[12]  = U0dot;
  dx[13]  = 0;
  dx[14]  = 0;
 
  
  return GSL_SUCCESS;
}


