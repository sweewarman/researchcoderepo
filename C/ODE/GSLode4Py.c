#include <stdio.h>
#include <Python.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_odeiv2.h>
#include <math.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

#include "OdeFunc.c"

static PyObject* py_ode(PyObject* Self,PyObject* args){

  
  double t = 0.0, t1 = 0.0, dt = 0.0;
  int dim = 0;
  double *y;
  
  PyArrayObject* SolverParams;
  PyArrayObject* Params;
  PyArrayObject* InitConditions;

  double *solverparams, *params, *initConditions;
  
  PyArg_ParseTuple(args,"O!O!O!",&PyArray_Type,&SolverParams,&PyArray_Type,&Params,&PyArray_Type,&InitConditions);

  solverparams   = (double*) PyArray_DATA(SolverParams);
  params         = (double*) PyArray_DATA(Params);
  initConditions = (double*) PyArray_DATA(InitConditions);
  
  dim   = (int)solverparams[0];
  t1 = solverparams[1];
  dt = solverparams[2];

  y = (double*)malloc(dim*sizeof(double));

  for(int i=0;i<dim;i++)
   y[i] = initConditions[i];

  int N      = (int)t1/dt;
  int stride = dim + 1;

  npy_intp size_output[2] = {N+1,stride}; 
  
  PyObject* out_array     = PyArray_SimpleNew(2,size_output,NPY_DOUBLE);

  double *V = (double*) PyArray_DATA((PyArrayObject*)out_array);

  gsl_odeiv2_system sys   = {func, NULL, dim, params};


  gsl_odeiv2_driver * d = 
    gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd,
				   1e-6, 1e-6, 0.0);


   V[0] = t;
   for(int i=1;i<stride;i++)
     V[i] = initConditions[i-1];

  
  
  for (int i = 1; i <= N; i++)
    {
     
      double ti = i * t1 / N;
      int status = gsl_odeiv2_driver_apply (d, &t, ti, y);
     
      if (status != GSL_SUCCESS)
	{
	  printf ("error, return value=%d\n", status);
	  break;
	}
      

      V[i*stride+0] = t;

      for (int j=1;j<stride;j++)
	V[i*stride+j] = y[j-1];
      
    }

  gsl_odeiv2_driver_free (d);
  
  free(y);

  Py_INCREF(out_array);  
  return (PyObject*) out_array;
  
}

static PyMethodDef GSLode4Py[] = {
  {"py_ode", py_ode, METH_VARARGS},
  {NULL, NULL}
};

void initGSLode4Py()
{
  (void) Py_InitModule("GSLode4Py", GSLode4Py);
  import_array();
}
